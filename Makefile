# Start oF the makeFile DeFining variables

objects := \
	timing.o \
	list.o\
	generalutility.o\
	fconfig.o \
        parameters.o \
	grid.o \
	eigensystem.o \
	potential.o \
	matrix.o \
	banded_matrices.o\
	functions2.o \
	matmult.o\
	crank_nicolson.o\
	sb_crank_nicolson.o\
	lanczos.o\
	lanczos_sym_banded.o\
	split_operator.o \
	chebychev.o \
  electron_spectra.o\
  main.o 


F90comp:=gfortran
NOLINK = -c
#LDFLAGS = -L/c:/lib/ -lblas -llapack

FCFLAGS  = -g -fbacktrace

######################################
# SET COMPILER FLAGS DEPENDING ON WHICH ONE IS BEING USED
######################################
ifeq  ($(F90comp),gfortran)
LDFLAGS = -L/usr/lib/ -lblas -llapack
else
ifeq  ($(F90comp),ifort)
LDFLAGS = -mkl#sequential
else
echo "please provide the lapack link of libraries"
endif
endif

## MakeFile
run: $(objects)
	$(F90comp) $(OMPFLAGS) $(FCFLAGS) -o 1dpot.x $(objects) $(LDFLAGS)
%: %.o
	$(F90comp) $(FCFLAGS) -o $@ $^ $(LDFLAGS)


%.o: %.f90
	$(F90comp) -o $@ $(FCFLAGS) $(OMPFLAGS) -c $<
%.o: %.F90
	$(F90comp) -o $@ $(FCFLAGS) $(OMPFLAGS) -c $<
%.o: %.f
	$(F90comp) -o $@ $(FCFLAGS) $(OMPFLAGS) -c $<


# Cleaning everything
clean:
	rm -fR *.x
	rm -f $(objects)
	rm -f $(objects:.o=.mod)
