!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
!
!> MODULE: lanczos_sym_banded
!
!> This module is used to do lanczos iteration on a symetric banded
!> matrix.
!---------------------------------------------------------------------


module lanczos_sym_banded
  use parameters
  use banded_matrices
  use timing
  use matmult
  implicit none
  private
  public sb_lanczos_exp, time_dep_lanczos_banded_sym


  !-----------------------------------------------------------------
  !>    type(tridiagonal_mat)               :: tdmat_alpha
  !>    this tridiagonal matrix is used to find the q_kth lanczos
  !>    vector associated to alpha_k diagonal element of the
  !>    lanczos matrix
  !-----------------------------------------------------------------
  type(banded_sym_mat)                :: bsmat_alpha
  integer                             :: last_vec
  real(8)                             :: max_overlap

contains


  !*************************************************************************
  !   Lanczos driver, lancz_itnum and lanc_threshold are parameters read
  !   from input file (look at parameters.f90) The option GS is the
  !   Gram_Schmidt operation on the lanczos vectors (q here.)
  !*************************************************************************
  subroutine sb_lanczos_exp(pulse_shape,bandnum, m_eig)
    use list
    use functions2
    use grid, only : x
    !number of eigenvectors that are projected onto
    integer                             :: m_eig
    !number of bands of the matrix
    integer                             :: bandnum
    integer                             :: i,j,k
    !length of time set
    integer                             :: tlength
    !length of probability set
    integer                             :: plength
    !iteration for which data is recorded
    integer                             :: rec_it
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !time, incremented from 0 by dt upto t_intv
    real(8)                             :: t
    !lanczo specific dt (dt_prime) which can decrease below dt
    real(8)                             :: dt_p, dt_saved
    !electric field that is evolving with time
    real(8)                             :: E_t
    !set containing the time increment with dt_p
    real(8), allocatable                :: tdata(:)
    !set containing the probability projections
    real(8), allocatable                :: pdata(:,:)
    !probability of projection of the propagated wave onto
    !the ground (0) and neig number of excited states
    !, temporary holder, the final agregate data is in pdata
    real(8), allocatable                :: prob(:), eigenvalues(:)
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !initial tridiagonal matrix of the system
    type(banded_sym_mat)                :: bsmat
    !time-evolved tridiagonal matrix of the system
    type(banded_sym_mat)                :: bsmat_t
    !determines the properties of the pulse
    procedure(pulse_at_t_func), pointer :: pulse
    !linked list that records time, the start of the list
    type(cell),pointer                  :: inital_time
    !linked list that records time, the rest of the list
    type(cell),pointer                  :: time
    !linked list that records probability array, the start of the list
    type(cell_array),pointer            :: i_p
    !linked list that records probability array, the rest of the list
    type(cell_array),pointer            :: p
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    !>the ground state eigenvector of tdmat, that is evolved with time
    complex(8), allocatable             :: psi_0(:)
    !>the m_found number of wavefunctions that are saved
    complex(8), allocatable             :: vec0 (:,:)
    !>projections of propagated wave on the m_found states
    complex(8), allocatable             :: proj (:)
    !i*dt of the exp (i*dt*D)
    complex(8)                          :: factor
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    character(len=*)                    :: pulse_shape
    character(len=64)                   :: fmt
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    print*, '("***Lanczos with maximum iteration of",x,I3,x"***")',lancz_itnum
    print*, 'lanczos tolerance:',lanc_threshold

    open(unit=33,file=datafilename)

    open(unit=34, file="iteration_vec.out")
    !soft_core_eps is an optional option if left out it will
    !be automatically 1.d0
    call bsmat%make_banded_matrix_on_grid(x,h,bandnum,soft_core_eps)
    call bsmat_t%make_banded_matrix_on_grid(x,h,bandnum)
    call bsmat_alpha%make_banded_matrix_on_grid(x,h,bandnum)

    !!factor is double idt because idt already has been halved...
    factor = -2.d0*idt

    !to just check all eigenvalues; not necessary
    !============================================================
    !call bsmat%eigenvalues_only(eigenvalues,printkey=.true.);stop
    !============================================================

    !here we find psi and psi_0
    call bsmat%eigensystem(m_eig, psi_0, vec0)

    !Start of the time loop
    call init_fill_list(inital_time, time, 0.d0)
    allocate(prob(1:m_eig), proj(1:m_eig))
    prob = [1.d0,(0.d0,i=1,m_eig-1)]
    call init_fill_array_list(i_p, p, prob)
    pulse => select_pulse_type(pulse_shape, 'length')
    dt_p = dt ; t = 0.d0; dt_saved = dt_p; j=0; rec_it=1

    print *,  '***Begin Time Loop***'

    call begin_timing
    do while (t <= t_intv)
      !              update pulse
      E_t = pulse(t + 0.5d0 * dt_p)

      ! Length-Gauge
      bsmat_t%diagonal(:)  = bsmat%diagonal(:)  - x(:) * E_t

      call time_dep_lanczos_banded_sym(bsmat_t,bsmat_t%mat_size,lancz_itnum,&
                                       dt_p,factor, psi_0,lanc_threshold)

      !              **************diagnostics*****************************************
      !                if (dt_saved > dt_p )then
      !                    print*, "dt=", dt_p, "@ t=", t,"fac=",factor
      rec_it = int(dt/dt_p)
      !                end if
      !              ******************************************************************
      !              print*, dt_p
      j = j + 1

      if (j == rec_it) then


        do i= 1, m_eig
          proj(i)  =  sum(psi_0(:)*vec0(:,i))
          prob(i)  =  real(proj(i)*conjg(proj(i)))
        end do

        !              warning this part is storing the probability arrays in memory
        !              the linked list approach is taken here

        call fill_list(time, t)
        call fill_array_list(p,prob)

        j = 0

      end if

      t = t + dt_p
      !            !****diagnostics***************
      !               dt_saved = min(dt_p,dt_saved)
      ! write(34,*) t,last_vec, max_overlap
      !            !******************************
      !               dt_p = dt; factor= -ii * dt

    end do ! end of the time loop

    call stop_timing()
    print *,  '***End Time Loop***'

    print*,'last (smallest) time step used:',dt_p
    print*,'last factor:',factor

    call print_timing

    call end_list(time)
    call end_array_list(p)

    call read_list(inital_time, time, tdata, tlength)
    call read_array_list(i_p, p, pdata, plength)

    !first memebr of the list is an artificial zero to start the list

    write(fmt,'("(f10.2,",I0,"es15.3)")') m_eig

    do i = 2, plength
      !do j = 1, neig+1
      write(33,fmt)tdata(i),pdata(:,i)
      !end do
    end do



  end subroutine sb_lanczos_exp


  !**************************************************************************
  !>    Lanczos iteration scheme for a symetric banded matrix
  !>    This version tries to reduce the dt in the exponential if convergence
  !>    is not reached at the end of the given number of iterations.
  !**************************************************************************
  subroutine time_dep_lanczos_banded_sym(bsmat_t,mat_size,itnum,dt_p,factor,v,threshold)
    use generalUtility , only : zschmab
    integer                             :: i,j,k,m
    integer, intent(in)                 :: itnum
    integer, parameter                  :: sch_const = 1
    integer                             :: mat_size
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    real(8), parameter                  :: sch_tol = 1.d-10
    real(8),intent(in)                  :: threshold
    real(8)                             :: convfac
    real(8)                             :: convfac_old
    real(8)                             :: dt_p
    real(8)                             :: alpha(itnum)
    real(8)                             :: beta (itnum)
    real(8)                             :: eigvec_k(itnum,itnum)
    real(8)                             :: eigval_k(itnum)
    real(8)                             :: eigvec_old(itnum,itnum)
    real(8)                             :: eigval_old(itnum)
    real(8)                             :: saved_diag(mat_size)
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    type(banded_sym_mat)                :: bsmat_t
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    complex(8),dimension (bsmat_t%mat_size,itnum)  :: q
    complex(8),dimension (   itnum)                :: qv
    complex(8),dimension (   itnum)                :: sqv
    complex(8),dimension (   itnum)                :: sqv_old
    complex(8),dimension (   itnum)                :: esqv
    complex(8),dimension (   itnum)                :: sesqv
    complex(8),dimension (   itnum)                :: sesqv_old
    complex(8),dimension (mat_size)                :: qsesqv
    complex(8),dimension (mat_size)                :: rh_vec
    complex(8),dimension (:)                       :: v
    complex(8)                                     :: factor
    complex(8)                                     :: newFactor
    complex(8)                                     :: zdotc
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logical                                        :: convg

    !******************
    !   diagnostics
    !******************
    !max_overlap = 0.d0
    !last_vec = itnum
    !******************

    alpha      = 0.d0
    beta       = 0.d0
    eigval_k   = 0.d0
    eigval_old = 0.d0
    eigvec_k   = 0.d0
    eigvec_old = 0.d0
    qv        = z_zero
    q         = z_zero
    sqv       = z_zero
    sqv_old   = z_zero
    esqv      = z_zero
    sesqv     = z_zero
    sesqv_old = z_zero
    convg     = .false.

    saved_diag = bsmat_t%diagonal
    !
    !! First iteration*******************************************

    q(:,1) = v/normz(v)

    !alpha(1) = q(:,1)* H_0 * q(:,1)

    alpha(1) = find_alpha(bsmat_t,q(:,1),mat_size)
    !

    !Performing projection on incoming vector**********************
    sqv_old  (1) = zdotc(mat_size,q(:,1),1,v,1)
    sesqv_old(1) = exp(factor*alpha(1))* sqv_old(1)

    !
    ! Finding the q of second iteration*****************************
    !

    bsmat_t%diagonal = saved_diag - alpha(1)

    call mat_vec_mul(bsmat_t,q(:,1),rh_vec)

    beta(1) = real(sqrt(zdotc(mat_size,rh_vec(:),1,rh_vec(:),1)))

    q(:,2)=rh_vec(:)/beta(1)

    !     ! print*,"just gram-schmidt the 1st to 2nd (they could be too close to each other)"
    call zschmab(q(1,1),q(1,2),sch_tol,mat_size,sch_const,1,m)
    !
    !! All other lanczos iterations***********************************
    !

    do k= 2, itnum


      bsmat_t%diagonal = saved_diag
      ! Find alpha(k):
      alpha(k) = find_alpha(bsmat_t,q(:,k),mat_size)

      !Finding the eigen values/vectors of the Lanczos
      !tridiagonal matrix
      call lanczos_diagonalizer(alpha(1:k),beta(1:k-1)&
        ,eigvec_k(1:k,1:k),eigval_k(1:k),k)

      ! Finding the projection of Lanczos tridiagonal
      ! matrix on the wave
      call proj_exp_q(q(:,1:k),v,eigvec_k(1:k,1:k),eigval_k(1:k), &
        mat_size,k,factor,sqv(1:k),sesqv(1:k))

      convfac = real(sqrt(zdotc(itnum,sesqv-sesqv_old,1,sesqv-sesqv_old,1)))

      if(convfac<=threshold)then
        convg = .true.
        call mat_vec_mul(q,sesqv,qsesqv,mat_size,itnum)
        v = qsesqv
        exit
      elseif(k<itnum)then
        sesqv_old            = sesqv
        sqv_old              = sqv
        eigvec_old           = eigvec_k
        eigval_old           = eigval_k
        bsmat_t%diagonal     = saved_diag - alpha(k)
        convfac_old          = convfac

        call mat_vec_mul(bsmat_t,q(:,k),rh_vec)

        rh_vec(:)   = rh_vec(:) - beta(k-1)* q(:,k-1)

        beta(k)  = real(sqrt(zdotc(mat_size,rh_vec(:),1,rh_vec(:),1)))

        q(:,k+1) = rh_vec(:)/beta(k)

      end if

    end do ! lanczos loop

    newFactor = factor/2
    ! !       reseting k to the last unconverged iteration
    i = 1 ; j =2
    k = itnum

    ! !        we only need to try the last two iterations of lanczos with the new time.
    do while (.not. convg)

      esqv = z_zero

      esqv(1:k-1) = exp(newFactor*eigval_old(1:k-1))*sqv_old(1:k-1)

      call mat_vec_mul(eigvec_old,esqv(1:k-1),sesqv_old,k-1,k-1)

      esqv = z_zero

      esqv(1:k) = exp(newFactor*eigval_k(1:k))*sqv(1:k)

      call mat_vec_mul(eigvec_k,esqv(1:k),sesqv,k,k)

      convfac = real(sqrt(zdotc(itnum,sesqv-sesqv_old,1,sesqv-sesqv_old,1)))
      !                print*, convfac

      if(convfac <= threshold) then
        ! ! projection to the original basis
        call mat_vec_mul(q,sesqv,qsesqv,mat_size,itnum)
        v = qsesqv
        !print*,"divided by:",j
        dt_p = dt_p/j
        factor = - ii * dt_p
        convg = .true.

      else

        if(i==20)stop 'reducing time-step size is not helping'
        newFactor = newFactor/2

        i = i +1
        j = 2 **i

      end if
      !
    end do
  end subroutine

  !************************************************************************
  !> Normalizing a complex vec
  !************************************************************************
  function normz(vec) result(norm)
    complex(8)                            :: zdotc
    complex(8),intent(in), dimension(:)   :: vec
    real   (8)                            :: norm

    norm = real(sqrt(zdotc(size(vec),vec,1,vec,1)))

  end function normz

  !*************************************************************************
  !>   Alpha as normaly described in Lanczos algorithms as the diagonal element
  !>   of Lanczos tridiagonal.
  !*************************************************************************
  function find_alpha(mat, vec, ndim) result(alpha)
    integer, intent(in)                     :: ndim
    type(banded_sym_mat)                    :: mat
    complex(8), dimension(ndim), intent(in) :: vec
    complex(8), dimension(ndim)             :: rh_vec
    complex(8)                              :: zdotc
    real(8)                                 :: alpha

    call mat_vec_mul(mat,vec,rh_vec)

    !        alpha(k) = q(:,k)* H_0 * q(:,k)
    alpha = real(zdotc(ndim,vec,1,rh_vec,1))


  end function find_alpha
  !************************************************************************
  !>  sesqv is the final vector in the lanczos basis; dimensioned k, the
  !>  iteration number of the lanczos.
  !>  This routine calclates sesqv and the the sqv(k) vector
  !>  Here s is the (k,k) dimensioned modal matrix of lanczos
  !>       q is the (ndim,k) matrix of lanczos vectors
  !**************************************************************************
  subroutine proj_exp_q(q,v,eigvec,eigval,ndim,k,factor,sqv,sesqv)

    integer   , intent(in)                  :: ndim
    integer   , intent(in)                  :: k
    complex(8), intent(in)                  :: factor
    complex(8), intent(in) ,dimension(:,:)  :: q
    complex(8), intent(in) ,dimension(ndim) :: v
    complex(8)             ,dimension(k)    :: qv
    complex(8), intent(out),dimension(k)    :: sqv
    complex(8)             ,dimension(k)    :: esqv
    complex(8), intent(out),dimension(k)    :: sesqv
    real   (8), intent(in),dimension(:,:)   :: eigvec
    real   (8), intent(in),dimension(:)     :: eigval

    !!             q^T_(kxn) * v(nx1) = qv (k,1)

    call tmat_vec_mul(q,v,qv,ndim,k)

    !!             s^T_(kxk) * qv_(kx1) = sqv_(kx1)
    call tmat_vec_mul(eigvec,qv,sqv,k,k)

    !!            exp(i dt eigenval)* I_(kxk) * sqv_(kx1)
    esqv(1:k) = exp(factor*eigval(1:k))*sqv(1:k)

    !!            S_(kxk) * esqv(kx1) = sesqv_(kx1)
    call mat_vec_mul(eigvec, esqv, sesqv, k,k)

  end subroutine proj_exp_q

  !************************************************************************
  !> Diagonalizer for lanczos tridiagonal
  !************************************************************************
  subroutine lanczos_diagonalizer(alpha,beta,eigvec,eigval,ndim)
    use eigensystem
    type(all_vec)                               :: mat_lanczos
    real(8),dimension(ndim,ndim),intent(out)    :: eigvec
    real(8),dimension(ndim)     ,intent(out)    :: eigval
    real(8),dimension(ndim)     ,intent( in)    :: alpha
    real(8),dimension(ndim-1)   ,intent( in)    :: beta
    integer                                     :: ndim

    call mat_lanczos%mat_initialize(alpha,beta)
    ! !      print*,mat_all%mat_size
    call mat_lanczos%eigensystem_solve

    eigvec = mat_lanczos%eigenvec
    eigval = mat_lanczos%eigenval

  end subroutine lanczos_diagonalizer

end module lanczos_sym_banded
