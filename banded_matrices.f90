module banded_matrices
  use potential
  use parameters, only: potential_type
  implicit none
  private
  public mat_vec_mul, banded_sym_mat

  type banded_sym_mat

    integer              :: mat_size
    integer              :: bsz
    real(8), allocatable :: diagonal(:)
    real(8), allocatable :: offdiagonal(:,:)
    logical              :: init = .false.

  contains

    procedure :: initialize  !(mat_size, band_size, diagonal(mat_size), offdiagonal(mat_size-1))
    procedure :: eigensystem !(num_eigenpairs,cmplx_ground_state(mat_size),cmplx_states(mat_size, num_eigenpairs))
    procedure :: eigenvalues_only           !(eigenvalues)
    procedure :: make_banded_matrix_on_grid !(x(mat_size),dx,band_size,soft_core_epsilon)

  end type




  interface mat_vec_mul!(mat,vec, answer)

    procedure :: mat_band_sym_zvec_mul

  end interface


contains

  subroutine initialize(self, m_size, b_size, diag, offd)
    !******************************************************************
    ! Initializes and allocates the components of the
    ! banded_symmetric_matrix (self).  
    !  m_size : symmetric matrix size 
    !  b_size : number of bands in matrix
    !  diag   : the diagonal vector of the matrix dimension(m_size)
    !  offd   : the off diagonal matrix has dimensions (m_size, b_size)
    !*******************************************************************           
    class(banded_sym_mat) :: self
    integer, intent(in)   :: m_size, b_size
    real(8), intent(in)   :: diag(:), offd(:,:)
    integer               :: i, j


    self%mat_size = m_size
    self%bsz      = b_size
    allocate(self%diagonal(m_size))
    allocate(self%offdiagonal(m_size-1, b_size))

    do j = 1, b_size
      do i = 1, m_size - 1
        self%offdiagonal(i, j) = offd(i, j)
      end do
    end do

    do i = 1, m_size
      self%diagonal(i) = diag(i)
    end do

    self%init = .true.

  end subroutine initialize


  subroutine mat_band_sym_zvec_mul(mat,vec,ans)
    !***************************************************************
    !Multiplies symmetric_banded matrix into a complex vector 
    !  mat : matrix of symmetrix_banded_type (NxN)
    !  vec : complex vector the matrix is multiplied into (N)
    !  ans : the outcome complex vector of multiplication (N)
    !**************************************************************
    type(banded_sym_mat) :: mat
    integer :: i, j
    integer :: b, m_size
    complex(8) :: vec(:)
    complex(8) :: ans(:)
    complex(8) :: z_zero = (0.d0, 0.d0)
    if(mat%init) then

      b      = mat%bsz
      m_size = mat%mat_size


      ans(1:m_size) = mat%diagonal(1:m_size) * vec(1:m_size)


      do i = 1, b
        ans(1:m_size) = ans(1:m_size) + [mat%offdiagonal(1:m_size-i,i) * vec(i+1:m_size),(z_zero, j=1,i)]
        ans(1:m_size) = ans(1:m_size) + [(z_zero, j=1,i), mat%offdiagonal(1:m_size-i,i) * vec(1:m_size-i) ]
      end do

    else
      stop 'type banded_sym_mat is not initilized!'
    end if

  end subroutine mat_band_sym_zvec_mul


  subroutine eigensystem( self, iu , psi, psi0)
    !*********************************************************************
    !Eigensystem solver for banded_symmetric matrix (self, NxN).
    ! A wrapper for dsbevx subroutine of Lapack that computes the eigen-
    ! values/vectors of the banded_symmetric matrix up to the iu-th values.
    !   iu : uppermost eigenvalue index to be calculated
    !   psi: complex vector of the first (ground state) eigenvector 
    !        sized (N)
    !   psi0: complex matrix holding the ground up to the iu-th excited 
    !         state psi0(N,iu)
    !*********************************************************************
    use timing
    class(banded_sym_mat) :: self
    character(len=1)      :: jobz, crange, uplo
    integer               :: kd
    integer               :: ldmat,ldq
    integer               :: i,j,k
    integer               :: ndim      !order of matrix
    integer               :: il, iu    !index of lowest and highest eigenvalues

    integer               :: info        !0 if alright, <0 i-th argument was wrong, >0 internal error
    integer               :: istat, m
    !
    integer, allocatable  :: ifail(:)
    integer, allocatable  :: iwork(:)    !support of internal routines
    real(8), allocatable  :: work(:)     !support of internal routines
    real(8), allocatable  :: mat(:,:), qmat(:,:)
    real(8)               :: vl, vu      !not referenced for 'I'
    real(8)               :: abstol      !absolute error tolerance for the eigenvalues
    real(8)               :: dlamch      !error is determined by this
    real(8), allocatable  :: w(:)        !selected eigenvalues in ascending order
    real(8), allocatable  :: z(:,:)      !eigenvalues
    complex(8), allocatable :: psi(:)
    complex(8), allocatable, optional :: psi0(:,:)
    !will hold ground eigenket transformed into
    !a complex quantity

    !

    if(self%init) then

      jobz   = 'V' !eigenvectors as well as eigenvalues
      crange = 'I' !Il-th trough Iu-th eigenvalues
      uplo   = 'U' !upper triangle of mat is stored

      abstol = 0.d0!2*DLAMCH('S')
      !
      !          !allocations
      il = 1
      ndim = self%mat_size
      kd   = self%bsz
      ldmat= kd + 1
      ldq  = ndim

      !
      allocate(psi(ndim), psi0(ndim,iu))
      allocate(mat(ldmat,ndim), qmat(ldq,ndim))
      allocate(w(ndim))
      allocate(z(ndim,ndim))
      allocate(ifail(ndim))
      allocate(work(7*ndim), stat=istat)
      if(istat /= 0 ) stop 'too large a dimension for work'
      allocate(iwork(5*ndim), stat=istat)
      if(istat /= 0 ) stop 'too large a dimension for work'
      !
      mat = 0.d0
      do i = 1, ndim
        mat(kd+1,i) = self%diagonal(i)
      end do

      k = 0
      do i = ldmat - 1 , 1, -1
        k = k + 1
        mat(i, :) = [(0.d0,j = 1, k), self%offdiagonal(:ndim-k,k)]
      end do

      !           do j = 1, ldmat
      !             print*, mat(j,:)
      !           end do

      print*, 'finding eigenvalues and eigenvectors of matrix'

      m =  iu
      call begin_timing
      call dsbevx(jobz, crange, uplo, ndim, kd, mat, ldmat, qmat, &
        ldq,vl, vu, il, iu, abstol, m, w, z, ndim, work, iwork, ifail, info)

      if(info /= 0)then
        print*, 'info=', info
        stop 'error in eigensystem of banded_matrices'
      end if

      call stop_timing
      call print_timing

      !print*, 'eigvals:',w(1:iu)

      !print*, 'eigvec1:', z(:,1)

      psi(:) =cmplx(1.0d0,0.d0) * z(:,1)
      if(present(psi0))then
        psi0(:,il:iu)=cmplx(1.0d0,0.d0) * z(:,il:iu)
      end if

    else

      stop 'type banded_sym_mat is not initilized!'

    end if
    print*, psi((ndim-1)/2)
  end subroutine eigensystem

  subroutine eigenvalues_only(self, w, printkey)
    !*******************************************************************************************************
    ! Finds all the eigenvalues of the banded_symmetric_matrix and stores them in w
    ! Optionally prints the eigenvalues if the logical key printkey is present and
    ! .true.
    !  A wrapper for dsbevx subroutine of Lapack.
    !******************************************************************************************************
    use timing
    class(banded_sym_mat) :: self
    character(len=1)      :: jobz, crange, uplo
    integer               :: kd
    integer               :: ldmat,ldq
    integer               :: i,j,k
    integer               :: ndim      !order of matrix
    integer               :: il, iu    !index of lowest and highest eigenvalues

    integer               :: info        !0 if alright, <0 i-th argument was wrong, >0 internal error
    integer               :: istat, m
    !
    integer, allocatable  :: ifail(:)
    integer, allocatable  :: iwork(:)    !support of internal routines
    real(8), allocatable  :: work(:)     !support of internal routines
    real(8), allocatable  :: mat(:,:), qmat(:,:)
    real(8)               :: vl, vu      !not referenced for 'I'
    real(8)               :: abstol      !absolute error tolerance for the eigenvalues
    real(8)               :: dlamch      !error is determined by this
    real(8), allocatable  :: w(:)        !selected eigenvalues in ascending order
    real(8), allocatable  :: z(:,:)      !eigenvalues

    logical,optional      :: printkey
    !

    if(self%init) then

      jobz   = 'N' !eigenvectors as well as eigenvalues
      crange = 'A' !Il-th trough Iu-th eigenvalues
      uplo   = 'U' !upper triangle of mat is stored

      abstol = 0.d0!2*DLAMCH('S')
      !
      !          !allocations
      il = 1
      ndim = self%mat_size
      kd   = self%bsz
      ldmat= kd + 1
      ldq  = ndim

      !

      allocate(mat(ldmat,ndim), qmat(ldq,ndim))
      allocate(w(ndim))
      allocate(z(ndim,ndim))
      allocate(ifail(ndim))
      allocate(work(7*ndim), stat=istat)
      if(istat /= 0 ) stop 'too large a dimension for work'
      allocate(iwork(5*ndim), stat=istat)
      if(istat /= 0 ) stop 'too large a dimension for work'
      !
      mat = 0.d0
      do i = 1, ndim
        mat(kd+1,i) = self%diagonal(i)
      end do

      k = 0
      do i = ldmat - 1 , 1, -1
        k = k + 1
        mat(i, :) = [(0.d0,j = 1, k), self%offdiagonal(:ndim-k,k)]
      end do

      !           do j = 1, ldmat
      !             print*, mat(j,:)
      !           end do

      print*, 'finding eigenvalues of matrix'

      m =  iu
      call begin_timing
      call dsbevx(jobz, crange, uplo, ndim, kd, mat, ldmat, qmat, &
        ldq,vl, vu, il, iu, abstol, m, w, z, ndim, work, iwork, ifail, info)

      if(info /= 0)then
        print*, 'info=', info
        stop 'error in eigenvalues of banded_matrices'
      end if

      call stop_timing
      call print_timing

      !if eigenvalues were required to be printed then print them
      if(present(printkey))then
        if(printkey)then
          print*, 'eigvals:',w(:)
        end if
      end if


    else

      stop 'type banded_sym_mat is not initilized!'

    end if

  end subroutine eigenvalues_only

  subroutine make_banded_matrix_on_grid(self,x_grid,dx,bandnum, epsilion_in)
    !***********************************************************************************************   
    ! Constructs the matrix of the Hamiltonian d^2/dx^2 + potential on
    ! as a baned_symmetric_matrix (self)
    !   xgrid      : on input, the already filled grid points vector
    !   dx         : the size of the uniform grid increments
    !   bandnum    : determines the size of finite-difference scheme to be used
    !                (i.e. the size of the bands in matrix)
    !   epsilon_in : optional value of the epsilon in the potential, if not provided
    !                is assumed to be 1.
    !***********************************************************************************************
    class(banded_sym_mat)  :: self
    real(8),intent(in)     :: x_grid(:)
    real(8)                :: dx, epsilion
    real(8), optional      :: epsilion_in
    integer                :: bandnum
    integer                :: ndim
    real(8),allocatable    :: diag(:), offdiag(:,:)
    procedure(potential_func), pointer :: pot


    pot => select_potential_type(potential_type)

    ndim = size(x_grid)

    if (present(epsilion_in)) then
      epsilion = epsilion_in
    else
      epsilion = 1.d0
    endif

    allocate(diag(ndim), offdiag(ndim-1,bandnum))

    select case (bandnum)

    case(1)

      diag(:) = (1.d0 / dx**2) +pot(x_grid(:))
      offdiag(1:ndim-1,1) = -0.5d0/dx**2


    case(2)

      diag(:) = (2.5d0 /2.d0 /dx**2) + pot(x_grid(:))
      offdiag(1:ndim-1,1) = -2d0/3.d0/dx**2
      offdiag(1:ndim-2,2) = 1d0/24.d0/dx**2

    case(3)

      diag(:) = (49.d0 / 36.d0/dx**2) &
        + pot(x_grid(:))
      offdiag(1:ndim-1,1) = -1.5d0/2.d0/dx**2
      offdiag(1:ndim-2,2) = 3.d0/40.d0/dx**2
      offdiag(1:ndim-3,3) = -1.d0/180.d0/dx**2

    case(4)

      diag(:) = (205.d0 / 144.d0/dx**2) &
        + pot(x_grid(:))
      offdiag(1:ndim-1,1) = -4.d0/5.d0/dx**2
      offdiag(1:ndim-2,2) = 1.d0/10.d0/dx**2
      offdiag(1:ndim-3,3) = -4.d0/315.d0/dx**2
      offdiag(1:ndim-4,4) = 1.d0/1120.d0/dx**2


    case default

      stop 'only band matrices between 1 and 4 are coded'

    end select

    call self%initialize(ndim, bandnum, diag, offdiag)


  end subroutine make_banded_matrix_on_grid

end module banded_matrices
