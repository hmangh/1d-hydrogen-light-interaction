module split_operator
  use parameters
  use timing
  use grid
  use functions2
  !use functions
  use generalUtility, only :   matrixExponantial2x2,MatSolver2x2, ebcc, ebtcc
  use matrix, only : eigenval, eigenvec, psi, psi0, fill_matrix
  use eigensystem
  implicit none
  private
  public      split_propagator,even_oddsplit_propagator, full_exp_propagator, &
              split_propagator_4

  complex(8), allocatable, dimension(:, :, :) :: Expmatodd, Expmateven
  character(len=20)                           :: fmt

contains
  !****************************************************************************************
  ! Given diagonal elements of a matrix assumed to be tridiagonal and symmetric,
  ! this routine splits the diagonals into two sets of 'even' and 'odd' bys simply
  ! assuming starting point zero and spliting every element of diagonal, except the first
  ! and last elements between even and odd diagonals.
  !****************************************************************************************
  subroutine even_odd(d, ed, od)
    real(8), intent(in) :: d(:)
    real(8), intent(out):: ed(:), od(:)
    integer :: n

    n = size(d)

    !print*, n, size(ed), size(od)

    if (size(ed) /= n-1 .or. size(od) /= n-1) stop 'even or odd diagonal size is incorrect'

    !given d elements the even elements are half the elements of d except for the first
    !element that is untouched and the last element that is excluded

    ed(1) = d(1)

    ed(2:n-1) = 0.5 * d(2:n-1)


    !given d elements the odd elements are half the elements of d except for the last
    !element that is untouched and the first element that is excluded

    od(n-1) = d(n)

    od(1:n-2) = 0.5 * d(2:n-1)

  end subroutine even_odd
  !****************************************************************************************

  subroutine H_0split_operator_initialize(diag, offdiag)
    !*******************************************************************************************
    ! given diagonal and off-diagonal elements of a symmetric tridiagonal matrix H_0, the
    ! Hamiltonian is split between 'odd' and 'even' diagonal sparce matrices. The exponentiation
    ! is then strightforward and is done by exponentiation of 2x2 elements. Here it is assumed
    ! all off-diagonal matrix elements are symmetric, but they don't have to be the same from
    ! each 2x2 matrix to another.
    ! on entry                   real(8):: diag : diagonal elements of H_0 matrix
    !                                   offdiag : offdiagonal elements of H_0, assumed symmetric
    !                                   dt      : time step
    !on exit,filling global variables:
    !                        complex(16):: Expmateven : exponential matrix of the even split
    !                        complex(16):: Expmatodd  : exponential matrix of the odd split
    !*******************************************************************************************

    integer                            :: ndim
    integer                            :: i, k
    real(8), intent(in)                :: diag(:), offdiag(:)
    real(8), dimension(2)              :: eigval
    real(8), dimension(2,2)            :: eigvec
    real(8), allocatable, dimension(:) :: evenD, oddD
    complex(8), dimension(1:2, 1:2)   :: Expmat
    complex(8) :: factor

    print*,  '*** Begin Setup ***'

    call begin_timing
    factor = - idt  !-cmplx(0.d0,1.d0)*0.5d0*dt

    ndim = size (diag)
   ! print*, ndim

    if (size(offdiag) /= ndim-1) stop 'wrong dimension of offdiagonals in split_operator_initialize!'

    allocate (evenD(ndim-1),oddD(ndim-1))
    allocate (Expmateven(2,2,(ndim-1)/2),Expmatodd(2,2,(ndim-1)/2))

    !populate evenD and oddD
    call even_odd(diag, evenD, oddD)

    k=0
    do i = 1, ndim-1, 2

      k=k+1

      call MatSolver2x2([oddD(i),oddD(i+1)], [offdiag(i), offdiag(i)] , eigval, eigvec)

      call matrixExponantial2x2(eigval, eigvec, Expmat, factor )

      !save the odd exponential matrix
      Expmatodd(:,:,k)=Expmat(:,:)


      call MatSolver2x2([evenD(i),evenD(i+1)], [offdiag(i), offdiag(i)] , eigval, eigvec)

      call matrixExponantial2x2(eigval, eigvec, Expmat, 2.0 * factor )

      !save the even exponential matrix
      Expmateven(:,:,k)=Expmat(:,:)

    end do
    call stop_timing
    call print_timing

  end subroutine H_0split_operator_initialize

  !*******************************************************************************************************

  subroutine H_0split_propagator(vec)
    !****************************************************************************************
    ! This subroutine for vector matrix multiplication, propagates the input matrix via
    ! split operator of the H_0 Hamiltonian. A set of 2x2 matrices filling the Expodd
    ! and Expeven are multiplied in vec.
    !****************************************************************************************
    complex(8) :: vec(:)
    integer     :: ndim
    integer     :: i, j, k, l
    complex(8), dimension (2)  :: ans

    ndim = size(vec)

    !odd matrix vector multiplication
    k=0
    do i = 1, ndim-1, 2

      k=k+1
      !  call mat_vec_multiply2x2(Expmatodd(:,:,k),[vec(i+1),vec(i+2)])!, ans)

      !initialize

      ans = (0.d0, 0.d0)

      !ans(1) = mat(1,1) * vec(1) + mat(1,2) * vec(2)
      !ans(2) = mat(2,1) * vec(1) + mat(2,2) * vec(2)

      do j = 1, 2
        do l =1, 2
          ans(j) = Expmatodd(j,l,k) * vec(i+l) + ans (j)
        end do
      end do


      vec(i+1)= ans(1)
      vec(i+2)= ans(2)


      !end do

      !even matrix vector multiplication
      !k=0
      !do i = 1, ndim-1, 2
      !  k=k+1
      !  call mat_vec_multiply2x2(Expmateven(:,:,k),[vec(i),vec(i+1)])!, ans)
      ans = (0.d0, 0.d0)

      !ans(1) = mat(1,1) * vec(1) + mat(1,2) * vec(2)
      !ans(2) = mat(2,1) * vec(1) + mat(2,2) * vec(2)

      do j = 1, 2
        do l =1, 2
          ans(j) = Expmateven(j,l,k) * vec(i+l-1) + ans (j)
        end do
      end do
      vec(i)= ans(1)
      vec(i+1)= ans(2)
      !end do

      !odd matrix vector multiplication
      !k=0
      !do i = 1, ndim-1, 2
      !  k=k+1
      !  call mat_vec_multiply2x2(Expmatodd(:,:,k),[vec(i+1),vec(i+2)])!, ans)
      ans = (0.d0, 0.d0)

      !ans(1) = mat(1,1) * vec(1) + mat(1,2) * vec(2)
      !ans(2) = mat(2,1) * vec(1) + mat(2,2) * vec(2)

      do j = 1, 2
        do l =1, 2
          ans(j) = Expmatodd(j,l,k) * vec(i+l) + ans (j)
        end do
      end do
      vec(i+1)= ans(1)
      vec(i+2)= ans(2)

    end do

  end subroutine H_0split_propagator


  !*****************************************************************************************

  subroutine diagonal_potential_exponential(v)
    !****************************************************************************************
    ! length-gauge time dependent exponential matrix.
    ! Assuming that the time dependent part of a Hamiltonian is all diagonal V(t),
    ! this routine forms the exponential matrix Exp[-i*V(t)*dt/2] and fills the
    ! global variable ExpV_t
    !****************************************************************************************
    real(8)     :: v(m_size)
    complex(8) :: factor
    complex(8),dimension(m_size) :: ExpV_t
    factor = - idt  !-cmplx(0.d0,1.d0)*0.5d0*dt

    ExpV_t(:) = exp(factor* v(:))

  end subroutine

  !**************************************************************

  subroutine V_tsplit_propagator(t,vec)
    !**************************************************************
    !  Calculates Exp[i V(t) dt/2]* vec
    !**************************************************************
    complex(8)                :: vec(:)
    complex(8)                :: factor
    real(8), dimension(m_size) :: V_t
    real(8)                    :: t
    real(8)                    :: E_t
    complex(8),dimension(m_size) :: ExpV_t
    ! call field_maker(t, V_t)
    ! call diagonal_potential_exponential(V_t)

    E_t = E_0*sin(omega*(t+0.5d0*dt))*sin(pi*(t+0.5d0*dt)/t_on)*sin(pi*(t+0.5d0*dt)/t_on)
    !
    V_t(:) = - x(:) * E_t
    !
    !  factor = - idt  !-cmplx(0.d0,1.d0)*0.5d0*dt
    !
    ExpV_t(:) = exp(-idt * V_t(:))

    vec(:)=ExpV_t(:)*vec(:)

  end subroutine

  !**************************************************************

  subroutine even_oddsplit_propagator(diag,offdiag)!,vec,vec0)
    !********************************************************************
    ! The routine takes in the diagonal and off-diagonal elements of
    ! a tridiagonal Hamiltonian matrix, H, and a vector to be multiplies into
    ! the exponentiation of the matrix; MatrixExp[H*i*dt].vec.
    ! vec initially contains an eigen vector of the H(t=0).
    ! vec0 contains some of the eigen vectors of the matrix at some
    ! initial time, i.e., t=0. The projection of time evolved vec on to
    ! vec0 eigen vectors is then written to file.
    !*********************************************************************
    real(8), intent(in) :: diag(:), offdiag(:)
    real(8)             :: V_t(m_size)
    real(8)             :: t
    real(8)             :: E_t
    integer             :: ndim
    integer             :: ierr
    integer             :: i,j, k, m ,n, first, last
    complex(8)          :: vec(1:m_size), vec0(1:m_size,0:neig)
    complex(8)          :: proj(0:neig)
    complex(8),dimension(m_size) :: ExpV_t
    real(8),allocatable,dimension(:):: prob
    print*,  '*** Split operator, even and odd H_0 exponentiation ***'

    open(unit=11,file=datafilename)
    write(fmt,'("(f10.3,",I0,"es15.3)")')neig

    call H_0split_operator_initialize(diag, offdiag)

    allocate(prob(1:(t_tot+1)*(neig)))

    last = 0

    print *,  '***Begin Time Loop***'
    print *,  'time after which the pulse is off:', int(t_on)
    !      tv=0.d0; th=0.d0
    call begin_timing

    do j = 0, t_tot
      t = dt * j
      !          call cpu_time (tv_i)
      !          call V_tsplit_propagator(time,vec)
      ! DEF SMOOTH PULSE
      E_t = E_0*sin(omega*(t+0.5d0*dt))*&
        sin(pi*(t+0.5d0*dt)/t_on)*sin(pi*(t+0.5d0*dt)/t_on)
      !
      V_t(:) = - x(:) * E_t
      !
      !!    - idt = -cmplx(0.d0,1.d0)*0.5d0*dt
      !
      ExpV_t(:) = exp(-idt * V_t(:))

      psi(:)=ExpV_t(:)*psi(:)

      !             call cpu_time (tv_f)
      !             tv=tv+tv_f-tv_i
      !             call cpu_time (th_i)

      call H_0split_propagator(psi)
      !             call cpu_time (th_f)
      !             th=th+th_f-th_i
      !          call V_tsplit_propagator(time,vec)
      psi(:)=ExpV_t(:)*psi(:)
      !          do k = 1, m_size
      !            write(1,*)  real(vec(k) * conjg(vec(k)))
      !          end do

      do i= 1, neig
        proj(i)=sum(psi(:)*psi0(:,i))
        !             prob(i)=real(proj(i)*conjg(proj(i)))
      end do
      !           write(11,fmt)t,prob(:)

      first = last + 1
      last  = first -1 + neig
      prob(first:last)= real(proj(:)*conjg(proj(:)))


    end do !end of time loop
    call stop_timing
    print *,  '***End Time Loop***'

    call print_timing

    k = 0
    do j = 0, t_tot
      i = k + 1
      k = (j+1) * (neig )
      write(11, fmt) j*dt, prob(i:k)
    end do


  end subroutine even_oddsplit_propagator

  !**********************************************************************

  subroutine split_propagator(pulse_shape)
    !********************************************************************
    ! The routine takes in the eigenvalues and eigenvectors of
    ! a tridiagonal Hamiltonian matrix, H_0, and a vector to be multiplies into
    ! the exponentiation of the matrix; MatrixExp[H_0*i*dt].vec.
    ! vec initially contains an eigen vector of the H_0(t=0).
    ! A field is turned on that also perturbs the vec (Exp(V(t)*i*dt)).
    ! vec0 contains some of the eigen vectors of the matrix at t=0.
    ! The projection of time evolved vec on to
    ! vec0 eigen vectors is then written to file.
    !*********************************************************************
    character(len=*)     :: pulse_shape
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    procedure(pulse_at_t_func), pointer :: pulse
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    real(8)             :: t
    real(8)             :: E_t
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    integer             :: ndim
    integer             :: ierr
    integer             :: io,i,j, k, n, first, last
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    complex(8), allocatable :: expmat(:, :), texp(:, :)
    !complex(8), dimension(m_size,m_size) :: expmat, texp
    complex(8)              :: proj(0:neig)
    complex(8)              :: factor
    complex(8)              :: ans(1:m_size)
    real(8),allocatable,dimension(:):: prob
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logical                 :: pulse_switched_on, file_exists

    print*, pulse_shape
    pulse => select_pulse_type(pulse_shape, 'length')
    open(unit=11,file=datafilename)
    write(fmt,'("(f10.2,",I0,"es15.3)")')neig

    !!   some checks

    print*,'matrix size=',size(eigenvec,2),'x',size(eigenvec,1)

    if ( size(eigenvec,2)/=m_size ) stop 'Full matrix diagonalization is needed'

    factor = -2.d0 * idt;
    print*,'factor', factor
    allocate(expmat(1:m_size, 1:m_size),texp(1:m_size, 1:m_size), stat = ierr )
    if ( ierr /= 0 ) then
      write(*,*) 'Oops, expmat or texp allocation failed!'
      stop
    endif
    !
    !

    allocate(prob(1:(t_tot+1)*(neig)))

    print *,'calc_matrixExp:', calc_matrixExp


    if (calc_matrixExp) then
      open(unit=25, file='exponentialMatrix.dat')
      !!     call matrixExponantial(eigenval, eigenvec, expmat, factor)
      expmat = z_zero
      print*,'starting exponentiation'
      call begin_timing
      do j= 1, m_size
        do k= 1, m_size
          texp(k,j) = exp(factor* eigenval(k)) * eigenvec(j,k)
        end do
      end do

      call ebcc(expmat, eigenvec, texp, m_size, m_size, m_size)


      do j = 1, m_size
        do i = 1, m_size
          write(25,*)expmat(i,j)
        end do
      end do

      call stop_timing
      call print_timing

    else
      inquire(file='exponentialMatrix.dat', exist=file_exists)
      if(file_exists) then
       open(unit=25, file='exponentialMatrix.dat', status='old')
       print *, ''
       do j = 1, m_size
         do i = 1, m_size
           read(25,*, iostat=io) expmat(i,j)
           print*, io
           if (io > 0 .or. io <0  ) then
             stop ("Old file does not match new input, save the matrix.dat file if needed and put T for recalc_matrix in input")
           endif
         end do
       end do
       !
      else
         print*, "file with saved exponential matrix does not exist"
         print*, "put T for recalc_exp in input"
         stop
      end if
    endif
    ! !
    ! !
    print *,  '***Begin Time Loop***'
    print *,  'time after which the pulse is off:', int(t_on)
    ! ! !      tv=0.d0; th=0.d0
    pulse_switched_on = .true.
    last = 0

    call begin_timing
    do k = 0, t_tot
        t = dt * k
        E_t = pulse(t+0.5d0*dt)
        ! ! !!     propagating the wave function
        psi(:)= exp( idt * x(:) * E_t) * psi(:)

        call zgemv ('n', m_size, m_size, z_one, expmat&
          , m_size, psi, 1, z_zero, ans, 1)

        psi(:)= exp( idt * x(:) * E_t) * ans(:)

        ! ! !!         projecting onto H_0 eigen vectors
        do i= 1, neig
          proj(i)=sum(psi(:)*psi0(:,i))
        end do

        first = last  + 1
        last  = first - 1 + neig
        prob(first:last)= real(proj(:)*conjg(proj(:)))

      end do
      call stop_timing
      print *,  '***End Time Loop***'
      call print_timing
      k = 0
      do j = 0, t_tot
        !     do i= 0,neig
        i = k + 1
        k = (j+1) * (neig)
        write(24, fmt) j*dt, prob(i:k)
        !    end do
      end do

      close(25)

  end subroutine split_propagator


  !**********************************************************************
  subroutine split_propagator_4(pulse_shape)
    !********************************************************************
    ! The routine takes in the eigenvalues and eigenvectors of
    ! a tridiagonal Hamiltonian matrix, H_0, and a vector to be multiplies into
    ! the exponentiation of the matrix; MatrixExp[H_0*i*dt].vec.
    ! vec initially contains an eigen vector of the H_0(t=0).
    ! A field is turned on that also perturbs the vec (Exp(V(t)*i*dt)).
    ! vec0 contains some of the eigen vectors of the matrix at t=0.
    ! The projection of time evolved vec on to
    ! vec0 eigen vectors is then written to file.
    !*********************************************************************
    character(len=*)        :: pulse_shape
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    procedure(pulse_at_t_func), pointer :: pulse
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    real(8)                 :: t
    real(8)                 :: E_t
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    integer                 :: ndim
    integer                 :: ierr, io
    integer                 :: i,j, k, n, first, last
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    complex(8), allocatable :: expmat(:, :), texp(:, :)
    complex(8), allocatable :: expmat_m(:, :), texp_m(:, :)
    complex(8)              :: vec(1:m_size), vec0(1:m_size,1:neig)
    complex(8)              :: proj(1:neig)
    complex(8)              :: factor
    complex(8)              :: ans(1:m_size)
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    real(8),allocatable,dimension(:)  :: prob
    real(8), dimension(5)             :: t_var
    real(8), parameter                :: s = 1.d0/(4.d0 - 4.d0**(1.d0/3.d0))
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logical                 :: pulse_switched_on, file_exists

    print*,  '*** Split operator full exponentiation 4th order***'
    print*, 's: ', s
    t_var = [s * 0.5d0 , 1.d0-1.5d0 * s, 0.5d0, 1.d0 - s, s * 1.5d0]

    do i = 1, 5
      print*, "t_var(",i,"):",t_var(i)
    end do
    print *, 'results written to ', datafilename

    print*,  pulse_shape
    pulse => select_pulse_type(pulse_shape, 'length')

    open(unit=11,file=datafilename)
    write(fmt,'("(f10.3,",I0,"es15.3)")')neig



    !!   some checks

    print*,'matrix size=',size(eigenvec,2),'x',size(eigenvec,1)

    if ( size(eigenvec,2)/=m_size ) stop 'Full matrix diagonalization is needed'

    !
    !
    ! !!   finding Exp(-i*dt* H_0)
    factor = -2.d0 * idt * s;
    print*,'factor', factor
    allocate(expmat(1:m_size, 1:m_size),texp(1:m_size, 1:m_size), stat = ierr )
    if ( ierr /= 0 ) then
      write(*,*) 'Oops, expmat or texp allocation failed!'
      stop
    endif
    allocate(expmat_m(1:m_size, 1:m_size),texp_m(1:m_size, 1:m_size), stat = ierr )
    if ( ierr /= 0 ) then
      write(*,*) 'Oops, expmat middle or texp middle allocation failed!'
      stop
    endif
    !
    !

    allocate(prob(1:(t_tot+1)*(neig)))

    print *,'calc_matrixExp:', calc_matrixExp


    if (.not. calc_matrixExp) then
      !*********************************************************
      inquire(file='exponentialMatrix4.dat', exist=file_exists)
      if(file_exists) then
       open(unit=25, file='exponentialMatrix4.dat', status='old')

       print*,'Reading Matexp'
       call begin_timing

       do j = 1, m_size
         do i = 1, m_size
           read(25,*, iostat=io) expmat(i,j), expmat_m(i,j)
           print*, io
           if (io > 0 .or. io <0  ) then
             stop ("Old file does not match new input, save the matrix.dat file if needed and put T for recalc_matrix in input")
           endif
         end do
       end do
       call stop_timing
       call print_timing

       !
      else
         print*, "file with saved exponential matrix does not exist"
         print*, "put T for recalc_exp in input"
         stop
      end if

    else
      open(unit=25, file='exponentialMatrix4.dat')
      expmat = z_zero; expmat_m= z_zero
      !*********************************************************
      print*,'starting exponentiation'
      call begin_timing

      do j= 1, m_size
        do k= 1, m_size
          texp(k,j) = exp(factor* eigenval(k)) * eigenvec(j,k)
          texp_m(k,j) = exp(-2.d0 * idt * (1-4.d0*s)* eigenval(k)) * eigenvec(j,k)
        end do
      end do
      call stop_timing
      call print_timing
      !********************************************************
      call ebcc(expmat, eigenvec, texp, m_size, m_size, m_size)
      call ebcc(expmat_m, eigenvec, texp_m, m_size, m_size, m_size)
      !********************************************************
      print*,'writing Matexp to file'
      call begin_timing

      do j = 1, m_size
        do i = 1, m_size
          write(25,*)expmat(i,j), expmat_m(i,j)
        end do
      end do
      call stop_timing
      call print_timing
      !**********************************************************
    end if
    ! !
    ! !
    print *,  '***Begin Time Loop***'
    print *,  'time after which the pulse is off:', int(t_on)

    last = 0

      call begin_timing
      do k = 0, t_tot
         t = dt * k

        do i = 1, 2
          E_t = pulse(t+t_var(i)*dt)

          ! ! !!     propagating the wave function
          psi(:)= exp( s * idt * x(:) * E_t) * psi(:)

          call zgemv ('n', m_size, m_size, z_one, expmat&
                     , m_size, psi , 1, z_zero, ans, 1)

          ! No Point Copying it
          !vec(:) = ans(:)
          !
          psi(:)= exp( s * idt * x(:) * E_t) * ans(:)

        end do

        E_t = pulse(t+t_var(3)*dt)
        ! ! !!     propagating the wave function
        psi(:)= exp( (1-4.d0*s) * idt * x(:) * E_t) * psi(:)

        call zgemv ('n', m_size, m_size, z_one, expmat_m &
                   , m_size, psi , 1, z_zero, ans, 1)

        ! No Point Copying it
        !vec(:) = ans(:)
        !
        psi(:)= exp( (1.d0-4.d0*s) * idt * x(:) * E_t) * ans(:)


        do i = 4, 5
          E_t = pulse(t+t_var(i)*dt)

          ! ! !!     propagating the wave function
          psi(:)= exp( s * idt * x(:) * E_t) * vec(:)

          call zgemv ('n', m_size, m_size, z_one, expmat&
            , m_size, psi , 1, z_zero, ans, 1)

          ! No Point Copying it
          !vec(:) = ans(:)
          !
          psi(:)= exp( s * idt * x(:) * E_t) * ans(:)

        end do

        ! ! !!         projecting onto H_0 eigen vectors
        do i= 1, neig
          proj(i)=sum(psi(:)*psi0(:,i))
        end do

        first = last + 1
        last  = first - 1 + neig
        prob(first:last)= real(proj(:)*conjg(proj(:)))

      end do
      call stop_timing
      print *,  '***End Time Loop***'
      call print_timing

      k = 0
      do j = 0, t_tot
         i = k + 1
         k = (j+1) * (neig)
         write(11, fmt) j*dt, prob(i:k)
     end do

    close(11)


  end subroutine split_propagator_4

  !*********************************************************************
  subroutine full_exp_propagator(pulse_shape)
    !**********************************************************************
    ! The routine fully exponentiates the time-dependant Hmailtonian (H(t))
    ! and propagates the wave according to Exp(i H(t)*dt)
    !***********************************************************************
    character(len=*)                    :: pulse_shape
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    procedure(pulse_at_t_func), pointer :: pulse
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    character(len=20)                   :: fmt
    !+++++++++++++++++++++++++++++++++++++++++++++++++++
    type(all_vec)                       :: mat_all
    type(tridiagonal_mat)               :: mat_tridiag
    !+++++++++++++++++++++++++++++++++++++++++++++++++++
    real(8)                             :: t
    real(8)                             :: diag(m_size)
    real(8)                             :: offdiag(m_size-1)
    real(8)                             :: E_t
    real(8),allocatable,dimension(:)    :: prob
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    integer                             :: i,j,k,n
    integer                             :: ierr
    integer                             :: first, last
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    complex(8)                          :: vec(1:m_size)
    complex(8)                          :: vec0(1:m_size,0:neig)
    complex(8)                          :: proj(0:neig)
    complex(8)                          :: overlap(1:m_size)
    complex(8)                          :: factor
    complex(8)                          :: ans(m_size)
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logical                             :: pulse_switched_on
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    print*,  '*** Full Exponential Propagation ***'
    print*,  pulse_shape
    pulse => select_pulse_type(pulse_shape, 'length')

    factor = -2.d0 * idt
    print*,'factor:',factor

    !!   some checks
    print*,'matrix size=',size(eigenvec,2),'x',size(eigenvec,1)
    if( size(eigenvec,2)/=m_size ) stop 'Full matrix diagonalization is needed'

    vec0 = psi0
    vec = psi

    ! !!     fill diagonal offdiag
    call fill_matrix(mat_tridiag)
    diag = mat_tridiag%diagonal
    offdiag = mat_tridiag%offdiagonal

    call mat_all%mat_initialize(diag,offdiag)

    print *, 'results written to fexp_l_prob.dat'
    print*, 'could be graphed with matlab through cnprob.m'
    open(unit=24,file='fexp_l_prob.dat')
    write(fmt,'("(f10.3,",I0,"es15.3)")')neig+1

    allocate(prob(1:(t_tot+1)*(neig+1)))


      print *,  '***Begin Time Loop***'
      print *,  'time after which the pulse is off:', int(t_on)
      !pulse_switched_on = .true.
      last = 0
      call begin_timing
      do k = 0, t_tot
        t = dt * k
        E_t = pulse(t+0.5d0*dt)
        mat_all%diagonal(:)= diag(:) - x(:) * E_t
        mat_all%offdiagonal(:) = offdiag(:)
        !   !       call full-eigensystem solver
        ! ! !       Find all eigenvalues and eigenvectors
        call mat_all%eigensystem_solve
        !           overlap= eigvec^T * vec
        call ebtcc(overlap, mat_all%eigenvec, vec, m_size, m_size, 1)
        overlap(:)=exp(factor*mat_all%eigenval(:))*overlap(:)
        call ebcc(vec, mat_all%eigenvec, overlap, m_size, m_size, 1)
        do i= 0, neig
          proj(i)=sum(vec(:)*vec0(:,i))
        end do
        first = last + 1
        last  = (k + 1)*(neig+1)
        prob(first:last)= real(proj(:)*conjg(proj(:)))
      end do
      call stop_timing()


    print *,  '***End Time Loop***'
    call print_timing()

    k = 0
    do j = 0, t_tot
      !       do i= 0,neig
      i = k + 1
      k = (j+1) * (neig + 1)
      write(24, fmt) j*dt, prob(i:k)
      !       end do
    end do

    close(24)

  end subroutine full_exp_propagator
  !*****************************************************************

end module split_operator
