module chebychev
  use parameters
  use eigensystem, only: tridiagonal_mat
  use matrix
  use matmult
  use timing
  use grid, only: x, m_size
  use functions2
  implicit none

  integer, parameter :: MAX_CHEBY_COEFF = 1000

contains

  subroutine expChebyCoeefs(spectral_radius, e_min, limit, dt, n, a) 
    intent(in) :: spectral_radius, e_min, dt, limit
    real(8) :: spectral_radius, e_min, dt, a(n), alpha, limit
    integer :: i, n
    intent(inout) :: a, n

    alpha = 0.5d0 * spectral_radius * dt

    print*, alpha, limit

    a(1) = bessel_jn(0, alpha)
    do i = 2, MAX_CHEBY_COEFF
      a(i) = 2d0 * bessel_jn(i - 1, alpha)
      if (abs(a(i)) < limit) then 
        n = i
        print*, "num_cheby_coeff = ", n
        return
      end if 
    end do


  end subroutine 


  !------------------------------------------------------------------
  ! DESCRIPTION:
  !> Steps psi forward by dt using precalculated chebychev coefficents
  !
  !> \param mypsi vector to be propagated, will be updated with the results
  !> \param h hamiltonion
  !> \param dt time step forwards
  !> \param coeff the derived chebychev coeffients, used again with each step
  !------------------------------------------------------------------
  subroutine cheby_step(mypsi, h, dt, coeff, num_coeff, e_min, delta)
    use matrix
    use grid, only: m_size
    use matmult

    real(8) :: delta, dt, d, e_min, beta, coeff(MAX_CHEBY_COEFF)
    complex(8) :: mypsi(m_size), v(m_size, 3), w(m_size, 2), &
      temp_vec(m_size)
    type(tridiagonal_mat) :: h
    integer :: i, num_coeff

    d = 0.5d0 * delta
    beta = d + e_min

    ! First Propagation
    v(:,1) = mypsi(:)
    w(:,1) = coeff(1) * v(:,1)

    ! Propagation 2
    call mat_vec_mul(h, v(:,1), temp_vec)
    v(:,2) = cmplx(0.d0, 1.d0) * (temp_vec(:) - beta * v(:,1))
    v(:,2) = v(:,2) / d
    w(:,2) = w(:,1) + coeff(2) * v(:,2)

    do i = 3, num_coeff
      ! only needs one w ago, but must be moved back from the beginning
      w(:,1) = w(:,2)

      call mat_vec_mul(h, v(:,2), temp_vec)
      v(:,3) = 2.d0 * cmplx(0.d0, 1.d0) * (temp_vec(:) - beta * v(:,2))
      v(:,3) = v(:,3)/d + v(:,1)
      w(:,2) = w(:,1) + coeff(i) * v(:,3)

      ! copy back
      v(:,1) = v(:,2)
      v(:,2) = v(:,3)
    end do

    mypsi(:) = exp(cmplx(0d0, 1d0) * beta * dt) * w(:,2)
  end subroutine cheby_step


  subroutine prop_chebychev(pulse_name)
    character(len=*) :: pulse_name
    complex(8) :: mypsi(m_size), proj(neig)
    real(8) :: prob(neig * (t_tot+1)), coeff(MAX_CHEBY_COEFF), t_array(t_tot)
    real(8) :: t, e_min, e_t, spectral_delta
    real(8), parameter :: limit = 1d-8
    procedure(pulse_at_t_func), pointer :: pulse
    type(tridiagonal_mat) :: current, base
    integer :: prob_idx, i, j, num_cheby

    write(*,*) pulse_name

    pulse => select_pulse_type(pulse_name, 'length')

    call eig_H0(base)
    call fill_matrix(current)

    prob_idx = 1
    t = 0
    mypsi = psi
    e_min = minval(eigenval)
    spectral_delta = maxval(eigenval) - e_min

    print*, "Min energy = ", e_min, "spectral delta = ",spectral_delta

    write(*,*) size(prob)
    call expChebyCoeefs(spectral_delta, e_min, limit, dt, num_cheby, coeff)

    call begin_timing
    do i = 1, t_tot
      e_t = pulse(t)
      ! print*, e_t, allocated(current%diagonal), allocated(base%diagonal), prob_idx
      current%diagonal(:) = base%diagonal(:) - e_t * x(:)

      call cheby_step(mypsi, current, dt, coeff, num_cheby, e_min, spectral_delta)

      ! Project and Save Probability
      do j = 1, neig
        proj(j) = sum(mypsi(:) * psi0(:,j))
      end do
      prob(prob_idx:prob_idx+neig-1) = real(proj(:) * conjg(proj(:)))
      prob_idx = prob_idx + neig
      t_array(i) = t

      ! Time steps forward
      t = i * dt
    end do
    call stop_timing
    call print_timing


    open(unit=78, file=datafilename)
    prob_idx = 1
    do i = 1, t_tot
      write(78,1002) t_array(i), (prob(j), j = prob_idx, prob_idx + neig - 1)
      prob_idx = prob_idx + neig 
    end do
    close(78)
    1002 FORMAT (205G20.9)

  end subroutine prop_chebychev

end module chebychev
