# What is this repository for?

This FORTRAN 90 code treats the interaction of a time dependent, intense, short-pulse, electromagnetic field with a 1-dimensional model of the hydrogen atom.  The model potential is a 
Coulomb potential which is cutoff near the origin to remove the singularity.  This model was first described and used by : J. Javanainen, J. H. Eberly, and Qichang Su (JES), 
“Numerical simulations of multiphoton ionization and above-threshold electron spectra,” 
Phys. Rev. A, vol. 38, no. 7, pp. 3430--3446, 1988. In this code we have explored a number of different propagation methods, other than the Crank-Nicolson method discussed by JES. 
The code may be used for multi-photon excitation as well as ionization. The code enables the user to explore
how above threshold ionization (ATI) occurs when an atom can absorb a large number of photons.

# Usage

Command line arguments are accepted.

./1dpot.x [config file]

Where config file is a configuration or input file required to define the input variables. If a config file is not provided, then the file default.conf is used. The default.config file contains a description of the input variables.  In addtion, they are also described here.

See default.conf for an example config file.

# How do I get set up?

The code may be compiled using either the Intel fortran compiler or gfortran.  The code requires the
BLAS and LAPACK libraries for linear algebra.  The Makefile links to these libraries using,

>### intel fortran:
>LDFLAGS = -mkl (optionally -mkl=sequential; depending on the system either one could run faster)

>### gfortran:
>LDFLAGS = -L\<name of directory with liblapack.a and libblas.a libraries> -llapack -lblas
for gfortran.

## Deployment instructions:

* make

* ./1dpot.x <inputfile>


# Input Parameters
This is a description of input parameters that are required as input. Additional input files and examples are included in the zip folder Input.zip. The user should look at default.config to see how the data is read. The config file is structured to have a keyword: followed by the value of that keyword.  Here are examples of the keywords and their definitions.

>## box-size:
> The atom is confined to a 1_dimensional box that extends from -box-size to box-size symmetrically about x=zero in atomic units. 

>## spatial_step_size:
>The distance between successive grid points. It is assumed that thuis does not vary with distance.

>## band_num_sym_mat:
>For **lanczos (sb_lanczos) and crank-nicolson (sb_cn_l)** in the length gauge, it is possible to use higher than a 3-point finite difference scheme.
These subroutines can call LAPACK routines that can handle a general, symmetric-banded matrix rather than one tailored just for a tridiagonal symmetric matrix. The default is 1.
Up to a 9-point finite difference having 4 bands is possible.  Other methods use only the 3-point scheme.  
>#### Here is a list of number of bands (band_num_sym_mat) with respective finite difference scheme:
* 3-point finite difference or tridiagonal matrices : 1 (majority of methods)
* 5-point finite difference or quintuple matrices   : 2
* 7-point finite difference or septuple matrices    : 3
* 9-point finite difference or nonotuple matrices   : 4

>Different potentials 
>## potential_type:
> potential type incorporated in the box
> current options are soft_coulomb and gaussian
> if type is soft_coulomb,
>## soft_core_eps:
> The cutoff potential parameter for soft_coulomb. Only valid if potential_type is soft_coulomb

>## gaussian_pot_amplitude
>## gaussian_pot_exponent
> variables for the gaussian potential of type V(x)=Amplitude * Exp(-exponent * x^2). Only valid if potential_type is gaussian.


>## total_time: 
> The total time of the simulation in atomic units.

>## time_step_size: 
> The size of the time step in atomic units


>## time_field_on_percent: 
> Percentage of time the laser field is on (by default 1.0) This quantity in general is always 1. In a few cases the simulation could be run for a few cycles after the laser field is turned off. In principle most of the routines run only during the duration of pulse on.



>## pulse_amp: 
> Amplitude of the laser pulse (default 0.1)

>## pulse_freq: 
> Frequency of the laser pulse (default 0.148)

>## pulse_phase:
> Phase of the laser pulse (default 0.0)

>## eigen_desired: 
> Number of eigenvectors of the H_0 that are required for projection onto the propagation wave. This value depends on how many eigenvectors of the initial Hamiltonian (H_0 ) are needed. 
 If you are using the split_propagation methods or full_exp_propagator which depend on all eigenvectors, this must be equal to
the size of the matrix or 2 * (box_size / spatial_step_size) + 1. In the case of electron spectrum subroutines a suggestion is always made to increase this parameter until enough eigenvectors are included for the spectrum calculations.



>## excited_eigen_plotted:
> Number of eigenvectors used to make the plots. In all cases excited_eigen_plotted <= eigen_desired. This number is used to save the H_0 eigenvectors that are used for propagation projection. In some cases, for example, the electron spectrum subroutines the number of eigenvectors is automatically suggested. 

># The next 2 parameters are used to enable caching of results.

>## recalculate_matrix:
> Recalculate the matrix can be True or False.
 Whether the eigenpairs of the H0 matrix can be reused depends on a number of things. If the spatial grid changes due to a change in box size or investigations of convergence, the eigenpairs must be recalculated.  Otherwise they may be reused.

>## calculate_matrix_exponentional:
Again very badly written
If we should recalculate the matrix exponentional, relevant in the case of split operators or full exponentiation 
( F or T )


>## pulse_type:
>two options:
* square_pulse 
* smooth_pulse



>## method_to_use:  
 >Method emploted to solve the TDSE, keywords are given in main.f90:
>### 3-point finite difference implemented: 
* cn_l and cn_v : crank-nicolson in length or velocity gauge 
* lanczos: Lanczos method with automatically variying time (always lowering the dt) 
* split_propagator: splitting H_0 and V(x)
* full_exp_propagator: no splitting, diagonalization and exponentiation at every time step
* cheby: Chebychev propagator
* lanczos_split_exp: lanczos + splitting the H_0 and V(x)
>### electron spectrum specific subroutines:
* spectrum_cnl
* spectrum_cnv
* spectrum_lncz

>### higher than 3-point finite difference methods:
* sb_cn_l and spectrum_sb_cnl: crank-nicolson for general excited states and electron spectrum investigations
* sb_lanczos and  spectrum_glncz: lanczos for general excited states and electron spectrum investigations


>## lancz_iterations:
 >Number of iterations for lanczos, typically 10 to 20

>## lancz_threshold: 
> Threshold number for lanczos convergence accuracy, typically 1.d-4 or 1.d-5 are enough 
