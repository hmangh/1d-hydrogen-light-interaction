! Symmetric banded crank-nicholson
module sb_crank_nicolson
  use parameters, only : idt, dt, t_tot, m_found, datafilename
  use banded_matrices
  use functions2
  use timing
  implicit none
  private
  public sb_cn_l, zmat_band_sym_zvec_mul

  complex(8), allocatable :: mat_ab(:,:)
  complex(8), allocatable :: ld_saved(:), rd_saved(:)

  integer   , allocatable :: ipiv(:)

contains

  !****************************************************************************  
  ! symetric banded matrix crank-nicolson in length gauge
  !***************************************************************************
  subroutine sb_cn_l(pulse_shape, mat_size, gridx, grid_dx, bandnum, soft_core_eps )

    type(banded_sym_mat)          :: mat

    real(8), intent(in)           :: gridx(:), grid_dx
    real(8), optional             :: soft_core_eps
    real(8)                       :: E_t, t
    real(8)                       :: prob(1:m_found)

    integer, intent(in)           :: mat_size, bandnum
    integer                       :: i,j,k

    complex(8), allocatable       :: rhs_mat_diagonal(:)
    complex(8), allocatable       :: rhs_mat_offdiagonal(:,:)
    complex(8), allocatable       :: lhs_mat_diagonal(:)
    complex(8), allocatable       :: lhs_mat_offdiagonal(:,:)
    complex(8), allocatable       :: E_pot(:)

    complex(8), allocatable       :: psi(:), ans(:)
    complex(8), allocatable       :: psi0(:,:)
    complex(8)                    :: proj(1:m_found)

    character(len=*)              :: pulse_shape

    procedure(pulse_at_t_func), pointer :: pulse


    open(unit=5,file=datafilename)
    print*, 'genaral banded symmetric matrix with cranck-nicholson in length gauge'
    print*, pulse_shape

    pulse => select_pulse_type(pulse_shape, 'length')

    if(.not. present(soft_core_eps))then
      soft_core_eps = 1.d0
    end if

    call allocate_commons(mat_size, bandnum)

    allocate(E_pot(mat_size),ans(mat_size))

    call mat%make_banded_matrix_on_grid(gridx,grid_dx,bandnum,soft_core_eps)

    !find the first eigenvector
    call mat%eigensystem(m_found,psi, psi0)

    print*, psi((mat_size-1)/2)!; stop

    call make_right_hand_side_mat_l(mat, rhs_mat_diagonal, rhs_mat_offdiagonal)

    call make_left_hand_side_mat_l (mat, lhs_mat_diagonal, lhs_mat_offdiagonal)


    ld_saved = lhs_mat_diagonal
    rd_saved = rhs_mat_diagonal

    print *,  '***Begin Time Loop***'
    call begin_timing
    t = 0.d0
    do i = 1, t_tot

      t = t + dt!; print*, 'time: ', t
      E_t = pulse(t + 0.5d0 * dt) !; print*, 'pulse: ', E_t
      E_pot(:) = idt * E_t * gridx(:) !;  print*, 'pulse pot_at: ', E_pot(mat_size)
      !update diagonals
      rhs_mat_diagonal = rd_saved + E_pot
      lhs_mat_diagonal = ld_saved - E_pot


      call zmat_band_sym_zvec_mul(rhs_mat_diagonal, rhs_mat_offdiagonal, psi,ans)

      call find_lhs_vec(lhs_mat_diagonal, lhs_mat_offdiagonal,ans)

      psi = ans


      do k= 1, m_found

        proj(k)=sum(psi(:)*psi0(:,k))
        prob(k)=real(proj(k)*conjg(proj(k)))

      end do

      write(5,*) t, prob(:)

    end do

    call stop_timing
    call print_timing
    close(5)

  end subroutine sb_cn_l


  subroutine allocate_commons(mat_size, bandnum)

    integer, intent(in)   :: mat_size, bandnum

    allocate(ld_saved(mat_size), rd_saved(mat_size))

    allocate(mat_ab(bandnum + 1, mat_size))
    allocate(ipiv(mat_size))

  end subroutine



  subroutine make_right_hand_side_mat_l(mat, rhs_mat_diagonal, rhs_mat_offdiagonal)
    type(banded_sym_mat) :: mat

    complex(8), allocatable   :: rhs_mat_diagonal(:)
    complex(8), allocatable   :: rhs_mat_offdiagonal(:,:)

    allocate(rhs_mat_diagonal(mat%mat_size))
    allocate(rhs_mat_offdiagonal(mat%mat_size-1,mat%bsz))

    rhs_mat_diagonal(:) = 1.d0 - idt * mat%diagonal(:)
    rhs_mat_offdiagonal(:,:) = - idt * mat%offdiagonal(:,:)


  end subroutine


  subroutine make_left_hand_side_mat_l(mat, lhs_mat_diagonal, lhs_mat_offdiagonal)
    type(banded_sym_mat)          :: mat

    complex(8), allocatable       :: lhs_mat_diagonal(:)
    complex(8), allocatable       :: lhs_mat_offdiagonal(:,:)

    allocate(lhs_mat_diagonal(mat%mat_size))
    allocate(lhs_mat_offdiagonal(mat%mat_size-1,mat%bsz))

    lhs_mat_diagonal(:) = 1.d0 + idt * mat%diagonal(:)
    lhs_mat_offdiagonal(:,:) = + idt * mat%offdiagonal(:,:)


  end subroutine

  !!*******************************************************************
  !! symetric complex matrix vector multiplier
  !!********************************************************************
  subroutine zmat_band_sym_zvec_mul(zmatd,zmato,vec, ans)
    integer    :: i, j
    integer    :: b, m_size
    complex(8) :: zmatd(:), zmato(:,:)
    complex(8) :: vec(:)
    complex(8) :: ans(:)
    complex(8) :: z_zero = (0.d0, 0.d0)

    b      = size(zmato, 2)
    m_size = size(zmatd)

    ans(1:m_size) = zmatd(1:m_size) * vec(1:m_size)

    do i = 1, b
      ans(1:m_size) = ans(1:m_size) + &
        [zmato(1:m_size-i,i) * vec(i+1:m_size),(z_zero, j=1,i)]
      ans(1:m_size) = ans(1:m_size) + &
        [(z_zero, j=1,i),zmato(1:m_size-i,i) * vec(1:m_size-i)]
    end do

  end subroutine zmat_band_sym_zvec_mul

  !!***********************************************************************
  !! Using lapack's zgbsv for linear-solver Ax=B
  !! zgbsv is a general banded matrix linear solver
  !!************************************************************************
  subroutine find_lhs_vec(zmatd, zmato, ans)

    integer    :: b, m_size, b3p1, i,j, k ,info

    complex(8) :: zmatd(:), zmato(:,:)
    complex(8) :: ans(:)
    complex(8), allocatable :: ab(:,:)
    complex(8) :: z_zero = (0.d0, 0.d0)

    m_size = size(zmatd)
    b = size(zmato,2)
    b3p1 = 3 * b + 1

    allocate(ab(b3p1,m_size))
    ab(2 * b + 1, :) = zmatd(:)

    k = 0
    do j = 2 * b + 2, b3p1
      k  =  k + 1
      ab(j,:) = [zmato(1:m_size-k,k),(z_zero,i=1,k)]
    end do

    k = 0
    do j =  2 * b, b + 1, -1
      k  =  k + 1
      ab(j, :) = [(z_zero, i=1,k),zmato(1:m_size-k,k)]
    end do

    call zgbsv (m_size, b, b, 1, ab, b3p1, ipiv, ans, m_size, info)

  end subroutine
  !****************************************************************
  !!The cholesky solver is good for real symmetric banded_matrices
  !!not complex Hermitian
  !****************************************************************
  subroutine cholesky_solve(zmatd, zmato, ans)

    integer    :: b, m_size, bp1, i,j,k ,info

    complex(8) :: zmatd(:), zmato(:,:), ans(:)
    character  :: uplo

    !complex(8), allocatable :: mat_ab(:,:)
    complex(8), parameter   :: z_zero = cmplx(0.d0, 0.d0)

    uplo = 'U'

    m_size = size(zmatd)
    b      = size(zmato,2)
    bp1    = b + 1


    mat_ab(bp1,:) = zmatd(:)

    k = 0
    do i = b , 1, -1
      k = k + 1
      mat_ab(i, :) = [(z_zero, j = 1, k), zmato(:m_size-k,k)]
    end do

    call zpbsv( uplo, m_size, b, 1, mat_ab, bp1, ans, m_size, info )

  end subroutine cholesky_solve

end module sb_crank_nicolson
