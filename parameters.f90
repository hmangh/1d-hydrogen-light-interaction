!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
!
!> MODULE: parameters
!
!> This module is used to load the parameters for the simulation, and
!> provide them to other modules through globals.
!---------------------------------------------------------------------


module parameters
  use fconfig

  implicit none

  !>***********************************************************************
  !> time variables
  !>***********************************************************************

  !> time 'box' size real(8) :: t_intv
  real(8)            :: t_intv

  !> size of time step real(8) :: dt
  real(8)            :: dt
  !> Total time to propagate the simulation
  integer            :: t_tot
  !> Number of steps pulse is on
  real(8)            :: t_on
  !> Percent of time the pulse is on
  real(8)            :: prcntg_on
  !> sqrt(-1) * dt
  complex(8)         :: idt ! sqrt(-1)*dt


  !>***********************************************************************
  !> spatial variables
  !>***********************************************************************
  !> Size of spatial box
  real(8)            :: l
  !> Size of steps in spatial box
  real(8)            :: h
  !>*********************************************************************
  !> matrix variables
  !>*********************************************************************
  !> soft core potential
  real(8)            :: soft_core_eps

  real(8)            :: gauss_pot_amplitude
  real(8)            :: gauss_pot_exponent
  !> Number of eigenstates whose projections on Psi is to be plotted
  integer            :: neig
  !> Number of eigenvalues and vectors needed to be found
  integer            :: m_found

  !> Recalculate matrix diagonalization
  logical            :: calc_matrix !recalculate matrix diagonalization?

  !> calculate Sum_i | i > exp(idt*D_i)<i|?
  logical            :: calc_matrixExp


  !***********************************************************************
  !lanczos variables
  !***********************************************************************

  !> Number of iterations of lanzos
  integer            :: lancz_itnum
  !> Tolerance of lanczos
  real(8)             :: lanc_threshold

  !************************************************************************
  !symetric banded matrices variable
  !************************************************************************
  !> Number of bands of a symmetric banded matrix
  integer              :: band_num_sym_mat
  !> Number of times ATI is probed
  integer              :: ati_laser_cycle_num
  !************************************************************************
  ! pulse variables
  !************************************************************************

  !> Pulse Aplitude
  real(8)             :: E_0   !amplitude of pulse

  !> Frequency of pulse
  real(8)             :: omega !frequency of pulse

  !> Phase of pulse
  real(8)             :: phase !pulse phase

 !> laser cycles for which ati spectrum is probed
  real(8), allocatable :: ati_laser_cycle_vec(:)
  !> Type of pulse to simulate. Either square_pulse or smooth_pulse
  character(:), allocatable  :: pulse_name! options are square_pulse or smooth_pulse

  !> The method to use for the simulation.
  !> Must be a string value tested against in main.
  character(:), allocatable  :: method_name

  character(:), allocatable  :: potential_type

  !*************************************************************************
  ! other complex variables and parameters
  !*************************************************************************

  !> 0 + sqrt(-1)
  complex*16, parameter       :: ii     = cmplx(0.d0,1.d0)

  !> ZERO
  complex*16, parameter       :: z_zero = cmplx(0.d0,0.d0)

  !> 1 + 0i
  complex*16, parameter       :: z_one  = cmplx(1.d0,0.d0)
  !*********************************************************************

  !> PI
  real*8, parameter           :: pi = 4*atan(1.0d0)
  !*********************************************************************

  !> Name of data file to write results out to
  character(len=512)          :: datafilename
contains

  !-----------------------------------------------------------------------
  ! DESCRIPTION:
  !> Reads in the parameters from file. The file is either the default or
  !> passed in on the command line. If the file passed on the command line
  !> does not exist then the program terminates.
  !
  !> The loaded parameters are placed into the global variables of this
  !> module.
  !-----------------------------------------------------------------------
  subroutine parameter_reader
    type(config)        :: conf
    character(len=255)  :: conf_file_name, default_conf_file = 'default.conf'
    integer             :: ierr

    call GET_COMMAND_ARGUMENT(1, conf_file_name, status=ierr)

    if (ierr .gt. 0) then
      conf_file_name = default_conf_file
      print*, "Using default configuration file"
    end if

    call conf%read_file(conf_file_name)
    print*, "Using config file = ", trim(conf_file_name)


    !     read size of box on each side
    call conf%value_from_key("box_size", l)

    !     read spatial step size
    call conf%value_from_key("spatial_step_size", h)
    !     number of steps needed to fill the box from 0 to l (look in grid.f90)
    !      n = int(l/h)

    !>    total size of the matrix (number of steps to fill the box from -l to l
    !>     and NOT include point 0 in the middle) look in grid module
    !>      m_size = 2*n
    !>      ...| * | * | * | ... where * is the grid point and | corresponds to
    !>      the normal division points
    
    call conf%value_from_key('potential_type', potential_type)
    select case(potential_type)
        case('soft_coulomb')
    
    !>     soft core potential factor
           call conf%value_from_key('soft_core_eps',soft_core_eps)
        
        case('gaussian')
    !>     gaussian potential
           call  conf%value_from_key('gauss_pot_amplitude',gauss_pot_amplitude)   
           call  conf%value_from_key('gauss_pot_exponent',gauss_pot_exponent)
    case default
         stop 'potential_type is not a valid key'
    end select

    !>     total time in au that it takes for the propagation of the wave
    call conf%value_from_key('total_time', t_intv)

    !     time step size
    call conf%value_from_key('time_step_size', dt)

    !     total time steps needed to be taken
    t_tot = int(t_intv/dt)
    !     percentage of the time that the laser pulse is on

    call conf%value_from_key('time_field_on_percent', prcntg_on)

    !     time (in a.u.) for which the pulse is on (T in Eberly et. al.)
    t_on  = int(t_tot * dt * prcntg_on)
    !     amplitude of the laser puls
    call conf%value_from_key('pulse_amp', E_0)

    !     frequency of the laser pulse
    call conf%value_from_key('pulse_freq', omega)

    !     phase of the laser pulse
    call conf%value_from_key('pulse_phase', phase)

    !     number of eigenvectors and possibly number of eigen_values that we
    !     are interested in
    call conf%value_from_key('eigen_desired', m_found)

    !     number of eigenvectors + 1 that are saved for the projection purposes.
    !     The first eigenvector is automatically saved, so the actual
    !     number of saved eigenvectors is neig + 1
    call conf%value_from_key('excited_eigen_plotted', neig)

    !     logical switch to bypass matrix operations if they match given input
    !     parameters; if True, matrix data is read from file.
    call conf%value_from_key('recalculate_matrix', calc_matrix)
    !     logical switch to bypass matrix operations if they match given input
    !     parameters; if True, matrix data is read from file.
    call conf%value_from_key('calculate_matrix_exponentional', calc_matrixExp)

    !     shape of pulse, currently keywords smooth_pulse and square_pulse work
    call conf%value_from_key('pulse_type', pulse_name)

    !     method
    call conf%value_from_key('method_to_use', method_name)

    !     number of lanczos iterations to be performed
    call conf%value_from_key('lancz_iterations', lancz_itnum)
    call conf%value_from_key('lancz_threshold', lanc_threshold)
    !     numbers of bands of a symetric banded matrix
    call conf%value_from_key('band_num_sym_mat', band_num_sym_mat)

   select case(trim(method_name))
  case('spectrum_glncz','spectrum_lncz','spectrum_cnl', &
      'spectrum_sb_cnl','spectrum_cnv')
    !     numbers of cycle probes of ATI (electron spectrum only)
    call conf%value_from_key('ati_laser_cycle_num', ati_laser_cycle_num)
    call conf%value_from_key('ati_laser_cycle_vec', ati_laser_cycle_num,ati_laser_cycle_vec)

    print*, 'number of laser cycles to probe the electron spectrum: ', ati_laser_cycle_num
    print*, 'laser cycles for which the ATI spectrum is probed: ', ati_laser_cycle_vec

  case default

  end select

    call make_datafile_name

    !Print relavant data
    print *, ''
    print* , potential_type
    print *, 'Spatial Box ranges from (a.u.)  ', -l, ' to ', l
    print *, 'Step size 		= ', h
    print *, 'Matrix size               = ', int(2 * real(l) / h) + 1
    print *, 'Propagation time in a.u. ', t_intv
    print *, 'Time step size 		= ', dt
    print *, 'Time pulse is on (a.u.) = ', t_on
    print *, 'Amplitude of pulse	= ', E_0
    print *, 'Frequency 		= ', omega
    print *, 'Phase                 = ', phase

    print *, 'type of pulse:', pulse_name
    print *, 'method key:', method_name
    print *, 'Saving results to: ', trim(datafilename)
    print *, ''
    !     initialize some parameters
    idt = 0.5d0*ii*dt


  end subroutine

  !--------------------------------------------------------------------
  ! DESCRIPTION:
  !> makes the datafile name from the parameters passed into the
  !> program
  !>
  !> Data file name is formatted such that:
  !>
  !> method_name pulse_name box_size spatial_step dt total_time
  !>
  !> Values that are small are expected to be small are multiplied by
  !> a 1000. All values are formatted as ints
  !--------------------------------------------------------------------
  subroutine make_datafile_name()
    character(len=64)     :: info
    character(len=32)     :: lanczos
    character (len=32)    :: sb, sbcnl

    write(info, 1001) h, l, dt, t_intv


    datafilename = trim(method_name) // '_' // trim(pulse_name) // &
      '_' // trim(info)

    if (index(trim(method_name), "lanczos") > 0) then
      write(lanczos, 1002) lancz_itnum, lanc_threshold

      datafilename = trim(datafilename) // trim(lanczos)
    end if

    if (index(trim(method_name), "spectrum_glncz") > 0 .or. &
        index(trim(method_name), "sb_lanczos") > 0 )    then
      write(sb, 1003)  band_num_sym_mat, lancz_itnum, lanc_threshold

      datafilename = trim(datafilename) // trim(sb)
    end if

    if (index(trim(method_name), "spectrum_sb_cnl") > 0 )  then
      write(sbcnl, 1004)  band_num_sym_mat

      datafilename = trim(datafilename) // trim(sbcnl)
    end if


    datafilename = trim(datafilename) // '.dat'

1001 format (G0.2, '_', G0.4, '_', G0.4, '_', G0.4)
1002 format ('_', I0.3, '_', G0.4)
1003 format ('_', I1,'_', I0.3, '_', G0.4)
1004 format ('_b',I1)
  end subroutine


end module
