module generalUtility
  implicit none

contains
  !****************************************************************************************      
  ! finds eigenvalues and normalized eigenvectors of a two by two matrix
  ! on entry real(8) diag   : diagonal elements of matrix from top to bottom
  !                  offdiag: offdiagonal elements oof matrix from top to bottom
  ! on exit          eigenval and eigenvec: eigenvalues of matrix in ascending order
  !                                         and the associated normalized eigenvectors
  !****************************************************************************************
  subroutine MatSolver2x2(diag, offdiag, eigenval, eigenvec)
    real(8), dimension(2), intent(in)    :: diag, offdiag
    real(8), dimension(2), intent(out)   :: eigenval
    real(8), dimension(2,2), intent(out) :: eigenvec
    real(8), dimension(2)                :: lambda
    real(8) :: apb, ab, cd, norm(1)
    integer :: i

    apb = diag(1) + diag(2)
    ab  = diag(1) * diag(2)
    cd  = offdiag(1) * offdiag(2)

    lambda(1) = 0.5 * ( apb + sqrt(apb**2 - 4.0 * (ab-cd)))
    lambda(2) = 0.5 * ( apb - sqrt(apb**2 - 4.0 * (ab-cd)))

    if (lambda(1) == lambda(2)) then
      eigenval = lambda
    else   
      eigenval(1) = min(lambda(1),lambda(2))
      eigenval(2) = max(lambda(1),lambda(2))
    end if

    if (offdiag(1) /= 0.d0) then
      eigenvec(1,1) = eigenval(1)-diag(2)
      eigenvec(2,1) = offdiag(1)
      eigenvec(1,2) = eigenval(2)-diag(2)
      eigenvec(2,2) = offdiag(1)
    else 
      if(offdiag(2) /= 0.d0)then
        eigenvec(1,1) = offdiag(2)
        eigenvec(2,1) = eigenval(1)-diag(1)
        eigenvec(1,2) = offdiag(2)
        eigenvec(2,2) = eigenval(2)-diag(1)
      else
        eigenvec(1,1) = 1.d0
        eigenvec(2,1) = 0.d0
        eigenvec(1,2) = 0.d0
        eigenvec(2,2) = 1.d0
      end if
    end if

    do i =1, 2
      norm = norm2(eigenvec(:,i),1)
      eigenvec(:,i) = eigenvec(:,i)/ norm(1)
    end do

  end subroutine
  !****************************************************************************************	  
  !Given an array eigval(1:2) and matrix eigvec(1:2, 1:2), the exponential matrix of
  !a two by two is calculated
  !****************************************************************************************
  subroutine matrixExponantial2x2(eigval, eigvec, Expmat, factor )

    integer :: i, j, k 
    real(8), dimension(2), intent(in)::eigval
    real(8), dimension(1:2, 1:2), intent(in) :: eigvec
    real(8), dimension(1:2, 1:2) :: Teigvec
    complex(8), intent(in) :: factor
    complex(8), dimension(1:2, 1:2), intent(out):: Expmat
    complex(8), dimension(1:2, 1:2):: holder
    complex(8), dimension(2) :: dexp

    !initialize

    Expmat = (0.d0, 0.d0)

    !First form the exponentials of eigenvalues, the eigenvalues and vectors are assumed to 
    !enter the subroutine already associated in the right order with respect to each other

    dexp(:) = Exp(factor * eigval(:))

    !Form the transpose of eigenvec matrix

    do i = 1, 2
      do j = 1, 2
        Teigvec(i,j) = eigvec(j,i)
      end do
    end do

    !Form the diagonalExp times the eigvec matrix
    do i = 1, 2
      holder(:,i) = dexp(:) * Teigvec(:,i)
    end do


    do i = 1, 2  
      do j = 1, 2
        do k = 1, 2
          Expmat(j,i) = Expmat(j,i) + eigvec(j,k) * holder(k,i) 
        end do
      end do
    end do

  end subroutine matrixExponantial2x2

  !****************************************************************************************

  !****************************************************************************************
  !Given an array eigenval(ndim) and matrix eigvec(ndim, ndim), the exponential matrix of
  !a ndim by ndim is calculated
  !****************************************************************************************
  subroutine matrixExponantial(eigval, eigvec, Expmat, factor )

    integer :: i, j, k 
    integer :: ndim

    real(8), dimension(:)   , intent(in) :: eigval
    real(8), dimension(:, :), intent(in) :: eigvec

    complex(8), intent(in)      :: factor
    complex(8), dimension(:, :) :: Expmat

    ndim = size(eigval)

    !make sure dimensions are right

    if(ndim/=size(eigvec,1).or.ndim/=size(eigvec,2))&
      stop 'wrong dimensions for exponentiation!'

    !initialize

    Expmat = (0.d0, 0.d0)

    !Form the transpose of eigvec matrix

    do k= 1, ndim
      do j = 1, ndim
        do i = 1, ndim
          Expmat(i,j) = Expmat(i,j) + eigvec(i,k)* exp(factor* eigval(k))*eigvec(j,k)
        end do
      end do
    end do

  end subroutine matrixExponantial

  !****************************************************************************************

  subroutine mat_vec_multiply2x2(mat,vec)!,ans)
    !****************************************************************************************
    ! complex 2x2 matrix and vector multiplication. The result is passed into ans 
    !****************************************************************************************

    !Given a two by two matrix and a 2x1 vector the 2x1 result of multiplication is produced
    complex(8), dimension(2,2) :: mat
    complex(8), dimension (2)  :: vec, ans
    integer:: i, j

    !initialize

    ans = (0.d0, 0.d0)

    !ans(1) = mat(1,1) * vec(1) + mat(1,2) * vec(2)
    !ans(2) = mat(2,1) * vec(1) + mat(2,2) * vec(2)

    do j = 1, 2
      do i =1, 2
        ans(j) = mat(j,i) * vec(i) + ans (j) 
      end do
    end do  

    vec(:) = ans(:)

  end subroutine mat_vec_multiply2x2

  !*****************************************************************************************
  ! matrix multiplies a = b * c
  ! takes   : real(8) b(ni,nk)
  !           complex (8)   c(nk,nj)
  ! returns : complex (8)   a(ni,nj)
  !*****************************************************************************************      
  subroutine ebtcc(a,b,c,ni,nk,nj)
    integer :: i,j, k
    integer :: ni, nj, nk
    real(8),dimension(nk,ni) :: b
    complex(8),dimension(ni,nj) :: a
    complex(8),dimension(nk,nj) :: c

    a = cmplx(0.d0,0.d0)
    do k=1,nj
      do j=1,nk
        do i=1,ni
          a(i,k)=a(i,k)+b(j,i)*c(j,k)
        end do
      end do
    end do

  end subroutine ebtcc
  !*****************************************************************************************
  ! *deck ebtccx
  ! c***begin prologue     ebtccx
  ! c***date written       880423   (yymmdd)
  ! c***revision date      yymmdd   (yymmdd)
  ! c***keywords           complex matrix multiply, transpose
  ! c***author             schneider, barry (lanl)
  ! c***source             mylib 
  ! c***                                         t
  ! c***purpose            matrix multiply  a = b * c
  ! c***description        vectorized matrix multiply
  ! c***                   for complex a and c
  ! c***                  
  ! c***references         none
  ! c
  ! c***end prologue       ebtccx
  subroutine ebtccx(a,na,b,nb,c,nc,ni,nk,nj)
    integer :: i, j, k
    integer :: ni, nj, nk, na, nb, nc  
    real(8) :: b(nb,ni)
    complex (8) :: a(na,nj), c(nc,nj)

    a = cmplx(0.d0,0.d0)
    do k=1,nj
      do j=1,nk
        do i=1,ni
          a(i,k)=a(i,k)+b(j,i)*c(j,k)
        end do
      end do
    end do
  end subroutine ebtccx
  !*****************************************************************************************
  subroutine ebtcx(a,na,b,nb,c,nc,ni,nk,nj)
    integer :: i, j, k
    integer :: ni, nj, nk, na, nb, nc  
    real(8) :: b(nb,ni)
    real(8) :: a(na,nj)
    real(8) :: c(nc,nj)

    a = 0.d0
    do k=1,nj
      do j=1,nk
        do i=1,ni
          a(i,k)=a(i,k)+b(j,i)*c(j,k)
        end do
      end do
    end do
  end subroutine ebtcx	  
  !*****************************************************************************************
  subroutine ebcc(a,b,c,ni,nk,nj)
    integer :: i,j, k
    integer :: ni, nj, nk
    real(8),dimension(ni,nk) :: b
    complex(8),dimension(ni,nj) :: a
    complex(8),dimension(nk,nj) :: c

    a = cmplx(0.d0,0.d0)
    do k=1,nj
      do j=1,nk
        do i=1,ni
          a(i,k)=a(i,k)+b(i,j)*c(j,k)
        end do
      end do
    end do

  end subroutine ebcc
  !******************************************************************************************
  ! *deck ebccx
  ! c***begin prologue     ebccx
  ! c***date written       880423   (yymmdd)
  ! c***revision date      yymmdd   (yymmdd)
  ! c***keywords           complex matrix multiply
  ! c***author             schneider, barry (lanl)
  ! c***source             mylib
  ! c***purpose            matrix multiply  a = b * c
  ! c***description        vectorized matrix multiply 
  ! c***                   for complex a and c
  ! c***                  
  ! c***references         none
  ! c
  ! c***routines called    czero(mylib)
  ! c***end prologue       ebccx
  subroutine ebccx(a,na,b,nb,c,nc,ni,nk,nj)
    integer :: i, j, k
    integer :: ni, nj, nk, na, nb, nc  
    real (8):: b(nb,nk)
    complex (8) :: a(na,nj), c(nc,nj)

    a = cmplx(0.d0,0.d0)
    do  k=1,nj
      do  j=1,nk
        do  i=1,ni
          a(i,k)=a(i,k)+b(i,j)*c(j,k)
        end do
      end do
    end do

  end subroutine ebccx
  !******************************************************************************************
  subroutine ebcx(a,na,b,nb,c,nc,ni,nk,nj)
    integer :: i,j, k
    integer :: ni, nj, nk, na, nb, nc   
    real (8):: a(na,nj)
    real (8):: b(nb,ni)
    real (8):: c(nc,nj)
    real (8), parameter :: zero=0.d0
    real (8), parameter :: one =1.d0
    !      dimension a(na,nj), b(nb,ni), c(nc,nj)

    call dgemm('n','n',ni,nj,nk,one,b,nb,c,nc,zero,a,na)


  end subroutine ebcx
  !******************************************************************************************
  !******************************************************************************************
  subroutine cebcx(a,na,b,nb,c,nc,ni,nk,nj)
    integer :: i, j, k
    integer :: ni, nj, nk, na, nb, nc  
    complex (8) :: b(nb,nk)
    complex (8) :: a(na,nj)
    real(8)	  :: c(nc,nj)

    a = cmplx(0.d0,0.d0)
    do  k=1,nj
      do  j=1,nk
        do  i=1,ni
          a(i,k)=a(i,k)+b(i,j)*c(j,k)
        end do
      end do
    end do

  end subroutine cebcx
  !******************************************************************************************       
  !*deck @(#)ebc.f 5.1  11/6/94
  ! c***begin prologue     ebc
  ! c***date written       850601  (yymmdd)
  ! c***revision date      yymmdd  (yymmdd)
  ! c***keywords           matrix, multiply,
  ! c***author             saxe, paul (lanl)
  ! c***source             @(#)ebc.f        5.1   11/6/94
  ! c***purpose            vectorized matrix operation:  a=b*c .
  ! c***description
  ! c                      call ebc(a,b,c,ni,nk,nj)
  ! c                        a       output matrix, (ni,nj).
  ! c                        b       input matrix, (ni,nk).
  ! c                        c       input matrix, (nk,nj).
  ! c
  ! c***references
  ! c***routines called    sgmm(clams)
  ! c***end prologue       ebc
  subroutine ebc(a,b,c,ni,nk,nj)
    integer :: ni, nj, nk   
    real (8):: a(ni,nj),b(ni,nk),c(nk,nj)
    real (8), parameter :: zero=0.d0
    real (8), parameter :: one =1.d0

    call dgemm('n','n',ni,nj,nk,one,b,ni,c,nk,zero,a,ni)

  end subroutine ebc     
  !******************************************************************************************
  ! matrix (tridiagonal and symmetric) vector multiply 
  ! takes: real(8) : d =>diagonal of the matrix (ndim)
  !        real(8) : e =>offdiagonal of the matrix (ndim-1)
  !        real(8) : v =>vector (ndim x 1)
  !        integer : ndim => size of the (diagonal of the) matrix
  ! returns: real(8) : ans => ndim x 1 vector
  !******************************************************************************************
  subroutine sym_tridiag_mat_vec_multp(d,e,v,ans, ndim)

    integer :: i, ndim
    real(8) :: d(ndim), e(ndim-1), v(ndim)
    real(8) :: ans(ndim)

    ans(1) =  d(1) * v(1)  +  e (1) * v(2)         
    do i = 2, ndim-1
      ans(i) =  d(i) * v(i) + e(i-1) * v(i-1) +  e (i) * v(i+1)
    end do
    ans(ndim) =  d(ndim) * v(ndim) + e(ndim-1) * v(ndim-1)

  end subroutine sym_tridiag_mat_vec_multp
  !******************************************************************************************
  ! matrix (tridiagonal and symmetric) vector multiply 
  ! takes: real(8) : d =>diagonal of the matrix (ndim)
  !        real(8) : e =>offdiagonal of the matrix (ndim-1)
  !     complex(8) : v =>vector (ndim x 1)
  !        integer : ndim => size of the (diagonal of the) matrix
  ! returns: complex(8) : ans => ndim x 1 vector
  !******************************************************************************************
  subroutine sym_tridiag_mat_cvec_multp(d,e,v,ans, ndim)

    integer    :: i, ndim
    real(8)    :: d(ndim), e(ndim-1)
    complex(8) :: v(ndim)
    complex(8) :: ans(ndim)

    ans(1) =  d(1) * v(1)  +  e (1) * v(2)         
    do i = 2, ndim-1
      ans(i) =  d(i) * v(i) + e(i-1) * v(i-1) +  e (i) * v(i+1)
    end do
    ans(ndim) =  d(ndim) * v(ndim) + e(ndim-1) * v(ndim-1)

  end subroutine sym_tridiag_mat_cvec_multp
  !******************************************************************************************
  subroutine schmab(va,vb,thresh,n,na,nb,nout,prnt)
    ! c***begin prologue     schmab
    ! c***date written       960801  (yymmdd)
    ! c***revision date      170820  (yymmdd)
    ! c
    ! c***keywords           gram-schmidt, orthogonalization
    ! c***author             barry schneider(nsf), revised heman gharibnejad (nist)
    ! c***source
    ! c***purpose            gram-schmidt orthogonalization.
    ! c***description        a set of non-orthonormal vectors, vb, are orthogonalized 
    ! c                      to another set of vectors, va, using a gram-schmidt process 
    ! c                      that checks for linear dependencies. the set of vectors,
    ! c                      va, are already assumed to be internally orthonormal. 
    ! c
    ! c                          va(n,*) = input vectors
    ! c                          vb(n,*) = input as non-orthogonal vectors and output
    ! c                                    as orthonormal set.
    ! c                          thresh  = acceptance tolerance on overlap
    ! c                          n       = dimension of vector space
    ! c                          na      = number of va vectors
    ! c                          nb      = number of initial vb vectors
    ! c                          nout    = number of final vb vectors
    ! c***references
    ! c***routines called    saxpy(clams), sdot(clams), sscal(clams)
    ! c
    ! c***end prologue       gschmt
    integer :: i, j, k
    integer :: n, na, nb
    integer :: nout

    real(8), dimension(n,na) :: va
    real(8), dimension(n,nb) :: vb
    real(8) :: ddot, norm, thresh, ovrlap
    logical, optional :: prnt
    ! print*,"inside"
    ! print*,"va"
    !      do i=1,na       
    !       print*, i, va(:,i)
    ! print*,'******************'
    !       end do
    !        print*, 'vb:'
    !        print*,vb
    nout=0
    do  i=1,nb
      do  j=1,na
        ovrlap=sum(va(:,j)*vb(:,i))!ddot(n,va(:,j),1,vb(:,i),1)
        !             if(na==15 .and. j==4)then
        !                do k=1,n
        !                  write(*,*)'va(i,4):',k, va(k,j)
        !                  write(*,*)'vb(i,15):',k,i, vb(k,i)
        !                  write(*,*) 'mult:',va(k,j)*vb(k,i)
        !                end do  
        !                stop
        !             endif   
        !            print*,'overlap:',j,ovrlap
        call daxpy(n,-ovrlap,va(:,j),1,vb(:,i),1)
        !             print*,'vb after saxpy:'
        !             print*, vb(:,i)
        !            print*,'****************************************************'
      end do

      norm=sqrt(sum(vb(:,i)*vb(:,i)))
      !sqrt(ddot(n,vb(:,i),1,vb(:,i),1))
      !          print*, 'norm',norm
      if(norm.gt.thresh) then
        !call dscal(n,1.0d+00/norm,vb(:,i),1)
        vb(:,i)=vb(:,i)/norm
        nout=nout+1
        !call copy(vb(1,i),vb(1,nout),n)
        vb(:,nout) = vb(:,i)
      endif
    end do
    if(present(prnt).and.prnt) then
      write(*,1) na, nb
      write(*,2) nout
    endif             
    if(nout.eq.0) then
      stop 'no vectors from schmab'
    endif
    !
    !
    !       print*,'vb being passed on'
    !       print*,vb
    !       print*,"*******************"
    return
    1    format(/,1x,'schmidt orthogonalization of two sets of vectors', &
      /,1x,                                                     &
      'set a already assumed to be orthonormal',/,1x,     &
      'number of vectors in set a = ',i4,/,1x,            &
      'number of vectors in set b = ',i4)                 
    2    format(/,1x,'number of set b vectors after orthogonalization '  &
      'to set a vectors = ',i4)
    end
    !*************************************************************************
    !******************************************************************************************
    subroutine zschmab(va,vb,thresh,n,na,nb,nout)!,prnt)
      ! c***begin prologue     schmab
      ! c***date written       960801  (yymmdd)
      ! c***revision date      170820  (yymmdd)
      ! c
      ! c***keywords           gram-schmidt, orthogonalization
      ! c***author             barry schneider(nsf), revised heman gharibnejad (nist)
      ! c***source
      ! c***purpose            gram-schmidt orthogonalization.
      ! c***description        a set of non-orthonormal vectors, vb, are orthogonalized 
      ! c                      to another set of vectors, va, using a gram-schmidt process 
      ! c                      that checks for linear dependencies. the set of vectors,
      ! c                      va, are already assumed to be internally orthonormal. 
      ! c
      ! c                          va(n,*) = input vectors
      ! c                          vb(n,*) = input as non-orthogonal vectors and output
      ! c                                    as orthonormal set.
      ! c                          thresh  = acceptance tolerance on overlap
      ! c                          n       = dimension of vector space
      ! c                          na      = number of va vectors
      ! c                          nb      = number of initial vb vectors
      ! c                          nout    = number of final vb vectors
      ! c***references
      ! c***routines called    saxpy(clams), sdot(clams), sscal(clams)
      ! c
      ! c***end prologue       gschmt
      integer :: i, j, k
      integer :: n, na, nb
      integer :: nout

      complex(8), dimension(n,na) :: va
      complex(8), dimension(n,nb) :: vb
      complex(8) :: zdotc, ovrlap
      real(8)    :: norm, thresh
      !logical, optional :: prnt
      ! print*,"inside"
      ! print*,"va"
      !      do i=1,na       
      !       print*, i, va(:,i)
      ! print*,'******************'
      !       end do
      !        print*, 'vb:'
      !        print*,vb
      nout=0
      do  i=1,nb
        do  j=1,na
          ovrlap= dot_product(va(:,j),vb(:,i))!!zdotc(n,va(:,j),1,vb(:,i),1)!sum(va(:,j)*vb(:,i))!ddot(n,va(:,j),1,vb(:,i),1)
          !             if(na==15 .and. j==4)then
          !                do k=1,n
          !                  write(*,*)'va(i,4):',k, va(k,j)
          !                  write(*,*)'vb(i,15):',k,i, vb(k,i)
          !                  write(*,*) 'mult:',va(k,j)*vb(k,i)
          !                end do  
          !                stop
          !             endif   
          !            print*,'overlap:',j,ovrlap
          call zaxpy(n,-ovrlap,va(:,j),1,vb(:,i),1)
          !             print*,'vb after saxpy:'
          !             print*, vb(:,i)
          !            print*,'****************************************************'
        end do

        norm=real(sqrt(dot_product(vb(:,i),vb(:,i))))!zdotc(n,vb(:,i),1,vb(:,i),1)))
        !sqrt(ddot(n,vb(:,i),1,vb(:,i),1))
        !          print*, 'norm',norm
        if(norm.gt.thresh) then
          !call dscal(n,1.0d+00/norm,vb(:,i),1)
          vb(:,i)=vb(:,i)/norm
          nout=nout+1
          !call copy(vb(1,i),vb(1,nout),n)
          vb(:,nout) = vb(:,i)
        endif
      end do
      !print*, present(prnt).and. prnt
!      if(present(prnt).and.prnt) then
!        write(*,1) na, nb
!        write(*,2) nout
!      endif             
!      if(nout.eq.0) then
!        stop 'no vectors from schmab'
!      endif
!      !
!      !
!           print*,'vb being passed on'
!           print*,vb
!           print*,"*******************"
!      return
!      1    format(/,1x,'schmidt orthogonalization of two sets of vectors', &
!        /,1x,                                                     &
!        'set a already assumed to be orthonormal',/,1x,     &
!        'number of vectors in set a = ',i4,/,1x,            &
!        'number of vectors in set b = ',i4)                 
!      2    format(/,1x,'number of set b vectors after orthogonalization '  &
!        'to set a vectors = ',i4)
      end
      !*************************************************************************
      ! Returns factorials up to the integer argument as an array of 
      ! double precision numbers.
      !*************************************************************************
      function factorial_array(max_number) result(array)
        !+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        integer, intent(in)            :: max_number
        integer                        :: i
        real(8), dimension(max_number) :: array
        !+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if(max_number == 0) array(1)=1.d0
        if(max_number < 0 ) stop 'argument of factorial must be positive'

        array(1) = 1.d0
        if(max_number > 1) then
          do i = 2,max_number
            array(i)=array(i-1)*real(i)
          end do
        end if

      end function factorial_array
      !**************************************************************************
    end module generalUtility
