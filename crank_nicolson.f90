module crank_nicolson
  use parameters
  use matrix
  use grid
  use functions2
  use potential
  use timing
  use eigensystem
  implicit none

  private
  !
  public  CN_velocity, CN_length, CN_length_split

  !Electron_Spectra_CN_length!, Electron_Spectra_CN_velocity
  !**********************************************************************
  ! inumarators
  integer, private           :: i, j, k, m, inc
  integer                    :: info !to check the linear solver
  !************************************************************************
  ! time variable
  !************************************************************************
  real*8            :: t
  !************************************************************************
  ! pulse variables
  !************************************************************************
  real*8            :: A_t   !vector field potential
  real*8            :: E_t   !Electric field
  !*************************************************************************
  ! Crank-Nicolson variables
  !**********************************************************************
  complex*16, allocatable     :: dcn_r(:),ecn_r_up(:),ecn_r_dwn(:)
  complex*16, allocatable     :: dcn_l(:),ecn_l_up(:),ecn_l_dwn(:)

  complex*16, allocatable     :: dcn_r_saved(:),ecn_r_up_saved(:),ecn_r_dwn_saved(:)
  complex*16, allocatable     :: dcn_l_saved(:),ecn_l_up_saved(:),ecn_l_dwn_saved(:)

  ! left-hand-side Crank_Nicolson matrix
  !complex*16, allocatable     :: lh_mat(:,:)
  complex*16, allocatable     :: rh_vec(:)! right-hand-side vector of Crank_Nicolson
  complex*16, allocatable     :: proj(:) ! holding projection of one state on another
  ! cpu time variables
  !*********************************************************************
  real*8                      :: t_i, t_f
  !*********************************************************************
  real(8), allocatable, dimension(:) :: pes,peseven,pesodd,w
  integer                            :: indxmin, indxmax
  integer                            :: ndim

  character(len=20)                  :: fmt

contains
  !************************************************************************
  subroutine allocate_CN_variables

    integer    :: ierr

    if(.not. allocated(proj))then
      allocate(proj(1:neig),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, proj allocation in Carnk-Nicolson failed!'
      endif
    endif

    if(.not. allocated(rh_vec))then
      allocate(rh_vec(l_indx:h_indx), stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, rh_vec allocation in Carnk-Nicolson failed!'
      endif
    endif

    if(.not. allocated(dcn_r))then
      allocate(dcn_r(l_indx:h_indx),dcn_l(l_indx:h_indx),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, dcn_r allocation failed!'
      endif
    endif

    if(.not. allocated(ecn_r_up))then
      allocate(ecn_r_up (l_indx:h_indx-1),ecn_l_up (l_indx:h_indx-1),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, ecn_r_up allocation failed!'
      endif
    endif

    if(.not. allocated(ecn_r_dwn))then
      allocate(ecn_r_dwn(l_indx+1:h_indx),ecn_l_dwn(l_indx+1:h_indx),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, ecn_r_dwn allocation failed!'
      endif
    endif


  end subroutine allocate_CN_variables

  !********************************************************************
  subroutine CN_velocity(pulse_shape)

    !Crank_Nicolson in the velocity gauge
    character(len=*)            :: pulse_shape
    procedure(pulse_at_t_func), pointer :: pulse
    procedure(potential_func) , pointer :: potential
    complex*16                  :: zdotc
    integer                     :: kk(1), pulse_switch
    real(8), allocatable        :: prob(:)

    call allocate_CN_variables
    allocate(prob(neig))
    !open(unit=12, file='A_t.dat')
    !open(unit=1, file='vgauge_probability.dat')
    !      open(unit=4,file='probability_v2l.dat') !Probabilities
    open(unit=3,file=datafilename)   !Time
    write(fmt,'("(f10.2,",I0,"es15.3)")')neig
    print*, pulse_shape
    psi(:)= psi0(:,l_indx)
    print*, 'Velocity gauge'
    print *,  '***Begin Time Loop***'
    print *,  'time after which the pulse is off:', int(t_on)

    pulse => select_pulse_type(pulse_shape, 'velocity')
    potential => select_potential_type(potential_type)

    call begin_timing
    pulse_switch = 1
    do j = 0, t_tot
      t  = j * dt
      ! write(5,*) t
      if (t >= t_on .and. pulse_switch == 1) then
        A_t = 0.d0
        print*, 'pulse is turned off at:', t
        ! kk =  maxloc (real(psi(:)*conjg(psi(:))))
        ! i = kk(1)
        pulse_switch = 0
      end if


      if(pulse_switch == 1) then
        A_t = pulse(t + 0.5d0*dt)
      end if


      !write(12,*)(t+0.5d0*dt),A_t
      ! Filling out the Carnk-Nicolson matrices
      !update of diagonals & off-diagonals unfortunately the
      !zgtsv solver destroys the diagonals and off-diagonals
      !diagonals
      
      dcn_r(l_indx:h_indx)=1.d0 - idt*( 1.d0/h**2 + potential(x(l_indx:h_indx)))
      dcn_l(l_indx:h_indx)=1.d0 + idt*( 1.d0/h**2 + potential(x(l_indx:h_indx)))
  
      !off-diagonals
      do i=l_indx,h_indx-1
        ecn_r_up (i)=-idt*(-0.5d0/h**2 + 0.5d0 * ii * A_t / h)
        ecn_l_up (i)= idt*(-0.5d0/h**2 + 0.5d0 * ii * A_t / h)
      end do
      do i=l_indx+1,h_indx
        ecn_r_dwn(i)=-idt*(-0.5d0/h**2 - 0.5d0 * ii * A_t / h)
        ecn_l_dwn(i)= idt*(-0.5d0/h**2 - 0.5d0 * ii * A_t / h)
      end do
      !applying Crank-Nicolson:
      ! building right-hand-side vector or B vector here
      rh_vec(l_indx) =  dcn_r(h_indx) * psi(l_indx)  +  ecn_r_up (l_indx) * psi(l_indx+1)
      do i = l_indx+1, h_indx-1
        rh_vec(i) =  dcn_r(i) * psi(i) + ecn_r_dwn(i) * psi(i-1) +  ecn_r_up (i) * psi(i+1)
      end do
      rh_vec(h_indx) =  dcn_r(h_indx) * psi(h_indx) + ecn_r_dwn(h_indx) * psi(h_indx-1)

      ! solve lh_mat * psi(new) = rh_mat * psi(old) = rh_vec
      ! by zgtsv(N, NRHS, DL, D, DU, B, LDB, INFO) that solves complex A X = B system
      ! N is the dimension of A, NRHS is the number of right hand sides, i.e., the number of columns
      ! On entry, DL must contain the (n-1) subdiagonal elements of A.
      ! On entry, D must contain the diagonal elements of A
      ! On entry, DU must contain the (n-1) superdiagonal elements of A.
      ! On entry, the N-by-NRHS right hand side matrix B. On exit, if INFO = 0, the N-by-NRHS solution matrix X.
      ! LDB is the leading dimension of the array B.  LDB >= max(1,N) of the matrix B.
      call zgtsv(m_size, 1, ecn_l_dwn, dcn_l, ecn_l_up, rh_vec , m_size, info)
      if (info /= 0) then
        print*,'info=',info
        stop
      endif

      ! zgtsv puts the answer in rh_vec (vector B)

      psi = rh_vec


      ! do i= l_indx, h_indx
      ! write(1,*) real(psi(i) * conjg(psi(i)))
      ! end do

      do i= 1, neig
        !Multiply by scale factor before projection:
        proj(i)=sum(exp(-ii*x(:)*A_t)*psi(:)*psi0(:,l_indx))
        !proj(i)=sum(psi(:)*psi0(:,l_indx))
        prob(i)=real(proj(i)*conjg(proj(i)))
        !          write(4,*) real(proj(i)*conjg(proj(i)))
      end do

      write(3,fmt)t,prob(:)

      !!Check normalization
      !write(*,*)j,real(sum(psi(:) * conjg(psi(:))))
      !write(*,*) j, zdotc(2*n+1, psi, 1, psi, 1)
      !Convert back from Complex to Real when writing
      !write (4,*) (sum( real((psi*conjg(psi)) * psi0) ))**2


    end do
    call stop_timing
    print *,  '***End Time Loop***'
    call print_timing

    !close(1)
    close(3)!;close(4)

  end subroutine
  !********************************************************************************
  subroutine CN_length(pulse_shape)

    !---------------------------------------------------------
    character(len=*) :: pulse_shape
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    procedure(pulse_at_t_func), pointer :: pulse
    procedure(potential_func) , pointer :: potential
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    integer  :: kk(1), pulse_switch
    integer  :: ierr, first, last
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    complex(8) :: projection
    complex(8) :: zdotc
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    real(8)                         :: halfdt
    real(8)                         :: sfceps2
    real(8)                         :: del2fac
    real(8),allocatable,dimension(:):: prob
    !---------------------------------------------------------

    print *,  '***Crank-Nicolson Length_Gauge***'

    call allocate_CN_variables

    allocate(prob(1:(t_tot+1)*(neig)))
    allocate(dcn_r_saved, source=dcn_r)
    allocate(dcn_l_saved, source=dcn_l)
    allocate(ecn_r_up_saved, source=ecn_r_up)
    allocate(ecn_l_up_saved, source= ecn_l_up)
    allocate(ecn_r_dwn_saved, source=ecn_r_dwn)
    allocate(ecn_l_dwn_saved, source=ecn_l_dwn)
    sfceps2  = soft_core_eps **2
    del2fac = 1.d0/h**2
    potential => select_potential_type(potential_type)
    !diagonals
    dcn_r(l_indx:h_indx)=1.d0 - idt*( 1.d0/h**2 + potential(x(l_indx:h_indx)))
    dcn_l(l_indx:h_indx)=1.d0 + idt*( 1.d0/h**2 + potential(x(l_indx:h_indx)))
    !off-diagonals
    do i=l_indx,h_indx-1
      ecn_r_up_saved(i)=-idt*(-0.5d0*del2fac )
      ecn_l_up_saved (i)= idt*(-0.5d0*del2fac )
    end do
    do i=l_indx+1,h_indx
      ecn_r_dwn_saved(i)=-idt*(-0.5d0*del2fac )
      ecn_l_dwn_saved(i)= idt*(-0.5d0*del2fac )
    end do


    open(unit=24,file=datafilename)
    write(fmt,'("(f10.3,",I0,"es15.3)")')neig
    !time step loop
    print*, pulse_shape
    pulse => select_pulse_type(pulse_shape, 'length')

    halfdt = 0.5d0 * dt
    print *,  '***Begin Time Loop***'
    print *,  'time after which the pulse is off:', int(t_on)
    ! time the time loop
    last = 0
    call begin_timing
    do j = 0, t_tot
      t  = j * dt
      E_t = pulse( t + halfdt )
      dcn_r = dcn_r_saved + idt * E_t * x
      dcn_l = dcn_l_saved - idt * E_t * x
      ecn_r_up  = ecn_r_up_saved
      ecn_l_up  = ecn_l_up_saved
      ecn_l_dwn = ecn_l_dwn_saved
      ecn_r_dwn = ecn_r_dwn_saved
      rh_vec(l_indx) =  dcn_r(l_indx) * psi(l_indx)  +  ecn_r_up (l_indx) * psi(l_indx+1)
      do i = l_indx+1, h_indx-1
        rh_vec(i) =  dcn_r(i) * psi(i) + ecn_r_dwn(i) * psi(i-1) +  ecn_r_up (i) * psi(i+1)
      end do
      rh_vec(h_indx) =  dcn_r(h_indx) * psi(h_indx) + ecn_r_dwn(h_indx) * psi(h_indx-1)
      call zgtsv(m_size, 1, ecn_l_dwn, dcn_l, ecn_l_up, rh_vec , m_size, info)
      psi = rh_vec
      do i= 1, neig
        proj(i)=sum(psi(:)*psi0(:,i))
      end do
      first = last  + 1
      last  = first - 1 + neig
      prob(first:last)= real(proj(:)*conjg(proj(:)))
    end do
    call stop_timing
    call print_timing
    k = 0
    do j = 0, t_tot
      !     do i= 0,neig
      i = k + 1
      k = (j+1) * (neig)
      write(24, fmt) j*dt, prob(i:k)
      !    end do
    end do

    close(24)


  end subroutine
  !*****************************************************************************

  subroutine CN_length_split(pulse_shape)

    !---------------------------------------------------------
    character(len=*) :: pulse_shape
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    procedure(pulse_at_t_func), pointer :: pulse
    procedure(potential_func) , pointer :: potential
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    integer  :: kk(1), pulse_switch
    integer  :: ierr, first, last
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    complex(8) :: projection
    complex(8) :: zdotc
    complex(8) :: v(m_size)
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    real(8)                         :: halfdt
    !real(8)                         :: sfceps2
    real(8)                         :: del2fac

    real(8),allocatable,dimension(:):: prob
    !---------------------------------------------------------

    print *,  '***Split-Crank-Nicolson Length_Gauge***'

    call allocate_CN_variables

    allocate(prob(1:(t_tot+1)*(neig)))
    allocate(dcn_r_saved, source=dcn_r)
    allocate(dcn_l_saved, source=dcn_l)
    allocate(ecn_r_up_saved, source=ecn_r_up)
    allocate(ecn_l_up_saved, source= ecn_l_up)
    allocate(ecn_r_dwn_saved, source=ecn_r_dwn)
    allocate(ecn_l_dwn_saved, source=ecn_l_dwn)
    !sfceps2  = soft_core_eps **2
    del2fac = 1/h**2
    potential => select_potential_type(potential_type)
    !diagonals
    
    dcn_r_saved(l_indx:h_indx)=1.d0 - idt*( 1.d0*del2fac + potential(x(l_indx:h_indx)))
    dcn_l_saved(l_indx:h_indx)=1.d0 + idt*( 1.d0*del2fac + potential(x(l_indx:h_indx)))
    
    !off-diagonals
    do i=l_indx,h_indx-1
      ecn_r_up_saved(i)=-idt*(-0.5d0*del2fac )
      ecn_l_up_saved (i)= idt*(-0.5d0*del2fac )
    end do
    do i=l_indx+1,h_indx
      ecn_r_dwn_saved(i)=-idt*(-0.5d0*del2fac )
      ecn_l_dwn_saved(i)= idt*(-0.5d0*del2fac )
    end do
    dcn_r     = dcn_r_saved
    dcn_l     = dcn_l_saved
    ecn_r_up  = ecn_r_up_saved
    ecn_l_up  = ecn_l_up_saved
    ecn_l_dwn = ecn_l_dwn_saved
    ecn_r_dwn = ecn_r_dwn_saved
    open(unit=24,file=datafilename)
    write(fmt,'("(f10.3,",I0,"es15.3)")')neig
    !time step loop
    print*, pulse_shape
    pulse => select_pulse_type(pulse_shape, 'length')

    halfdt = 0.5d0 * dt
    print *,  '***Begin Time Loop***'
    print *,  'time after which the pulse is off:', int(t_on)
    ! time the time loop
    last = 0
    call begin_timing
    do j = 0, t_tot
      t   = j * dt
      E_t = pulse( t + halfdt )
      v   =  exp( - idt *  x * E_t )

      !left split
      psi(:) = v(:) * psi(:)

      !tridiagonal multiplier
      rh_vec(l_indx) =  dcn_r(l_indx) * psi(l_indx)  +  ecn_r_up (l_indx) * psi(l_indx+1)
      do i = l_indx+1, h_indx-1
        rh_vec(i) =  dcn_r(i) * psi(i) + ecn_r_dwn(i) * psi(i-1) +  ecn_r_up (i) * psi(i+1)
      end do
      rh_vec(h_indx) =  dcn_r(h_indx) * psi(h_indx) + ecn_r_dwn(h_indx) * psi(h_indx-1)
      !linear solver
      call zgtsv(m_size, 1, ecn_l_dwn, dcn_l, ecn_l_up, rh_vec , m_size, info)
      ! overwritten bits are copied back
      ecn_l_dwn = ecn_l_dwn_saved
      dcn_l     = dcn_l_saved
      ecn_l_up  = ecn_l_up_saved
      psi = rh_vec

      !right split
      psi(:) = v(:) * psi(:)

      !projections are saved
      do i= 1, neig
        proj(i)=sum(psi(:)*psi0(:,i))
      end do

      first = last  + 1
      last  = first - 1 + neig
      prob(first:last)= real(proj(:)*conjg(proj(:)))
    end do

    call stop_timing
    call print_timing

    k = 0
    do j = 0, t_tot
      !     do i= 0,neig
      i = k + 1
      k = (j+1) * (neig)
      write(24, fmt) j*dt, prob(i:k)
      !    end do
    end do

    close(24)


  end subroutine





end module
