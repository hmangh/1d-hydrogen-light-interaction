module electron_spectra
  use parameters
  use matrix
  use grid
  use functions2
  use potential
  use eigensystem
  use generalUtility
  use matmult
  use list
  use timing
  implicit none

  private
  !
  public  Electron_Spectra_CN_length,   &
    Electron_Spectra_CN_velocity, &
    Electron_Spectra_sb_cn_l,     &
    Electron_Spectra_LNZ_length,  &
    Electron_Spectra_GLNZ_length
  !**********************************************************************
  ! inumarators
  ! integer, private         ::
  integer                    :: info !to check the linear solver
  !************************************************************************
  ! time variable
  !************************************************************************
  real*8            :: t
  !************************************************************************
  ! pulse variables
  !************************************************************************
  real*8            :: A_t   !vector field potential
  real*8            :: E_t   !Electric field
  !*************************************************************************
  ! Crank-Nicolson variables
  !**********************************************************************
  complex*16, allocatable     :: dcn_r(:),ecn_r_up(:),ecn_r_dwn(:)
  complex*16, allocatable     :: dcn_l(:),ecn_l_up(:),ecn_l_dwn(:)

  complex*16, allocatable     :: dcn_r_saved(:),ecn_r_up_saved(:),ecn_r_dwn_saved(:)
  complex*16, allocatable     :: dcn_l_saved(:),ecn_l_up_saved(:),ecn_l_dwn_saved(:)

  ! left-hand-side Crank_Nicolson matrix
  !complex*16, allocatable     :: lh_mat(:,:)
  complex*16, allocatable     :: rh_vec(:)! right-hand-side vector of Crank_Nicolson
  complex*16, allocatable     :: proj(:) ! holding projection of one state on another
  !*********************************************************************
  real(8), allocatable, dimension(:) :: pes,peseven,pesodd,w
  integer                            :: indxmin, indxmax
  integer                            :: ndim

  character(len=20)                  :: fmt
  !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  !    this tridiagonal matrix is used to find the q_kth lanczos
  !    vector associated to alpha_k diagonal element of the
  !    lanczos matrix
  type(tridiagonal_mat)                          :: tdmat_alpha
  complex(8),dimension(:), allocatable           :: qsesqv
  complex(8),dimension(:,:), allocatable           :: q
  !************************************************************************

contains

  !*****************************************************************************
  subroutine convert_cycles_to_times(t_array,t_int)

    integer :: i
    integer , allocatable:: t_int(:)
    integer(8) :: mk
    real(8), allocatable :: t_array(:)
    !******************** Electron spectra **************************
    !        Times when spectra is graphed

    allocate(t_int(ati_laser_cycle_num), t_array(ati_laser_cycle_num))


    !        generally we probe times equivalent to 4.25, 8.25,
    !        16.25, 32.25
    !        of laser cycles
    ! converting laser cycles to time
    t_array = ati_laser_cycle_vec * 2.d0 * pi / omega
    !integer location of the time in its array, assuming the dt does not change.
    do i = 1, ati_laser_cycle_num
      t_int(i)= int(anint(t_array(i)/dt))
      mk = anint(t_array(i)*100.d0)
      t_array(i) = mk / 100.d0
    end do

    print*,'times for probing the Electron Spectra:'
    print*,t_array

  end subroutine

  !////////////////////////////////////////////////////////////////////////////
  subroutine Electron_Spectra_CN_length(pulse_shape)

    type(tridiagonal_mat)       :: hamiltonian
    type(some_vec)              :: mat

    integer                     :: i, j, m, inc, k
    integer                     :: tslength
    integer                     :: counter
    integer                     :: int_t_steps, previous_steps
    integer, allocatable        :: t_int(:)


    real(8)                     :: E_t
    real(8), allocatable        :: time_series(:)

    character(len=*)            :: pulse_shape

    complex(8),allocatable      :: eigenvectors(:,:)
    complex(8)                  :: zdotc
    complex(8)                  :: projection


    procedure(pulse_at_t_func),pointer :: pulse

    !***************************************************************************

    print*, "**** ati spectrum with CN in length gauge ****"
    print*, "pulse shape: ", pulse_shape


    ! allocae and fill various variable pertinent to the method
    call allocate_CN_variables

    call convert_cycles_to_times(time_series,t_int)
    !check the time intervals given fit in the time of simulation
    !disregard times that are larger than simulation time
    call check_time_series(time_series,tslength)

    call fill_matrix(hamiltonian)

    call calculate_eigenvalues(hamiltonian,mat)

    call find_spectrum_dimensions(mat,ndim)


    allocate(w(ndim))
    indxmax = ndim - 2
    allocate(pes(indxmax),peseven(indxmax),pesodd(indxmax))

    !finding quadruple-avereged eigenvalues weighted by omega
    call find_normalized_quadruple_energy(w,mat%eigenval)

    ! find all eigenvectors (ndim of them)
    print*, 'finding corresponding eigenvectors...'
    call find_some_eigenvectors(mat,ndim,eigenvectors)


    open(unit=30,file=datafilename)
    write(30, *) indxmax , tslength
    !
    previous_steps = 0
    counter = 1
    t       = 0.d0
    pulse => select_pulse_type(pulse_shape, 'length')
    !
    print *,  '***Begin Time Loop***'
    print *,  'time after which the pulse is off:', t_on
    ! time the time loop
    call begin_timing

    do counter = 1, tslength

      int_t_steps = t_int(counter) - previous_steps
      print*, counter, "int_t_steps:", int_t_steps
      do
        do i = 1, int_t_steps
          !     update pulse
          E_t = pulse(t+0.5d0*dt)
          !******propagation*******
          call cn_driver_l (E_t)
          !************************
          t   = t + dt
        end do

        int_t_steps = int(abs(time_series(counter)-t)/dt)
        print*, "steps to go:", int_t_steps, "@t:", t, "with dt:", dt
        if(int_t_steps<=1)exit

      end do

      print*,'counter',counter,'time:',time_series(counter),'t:',t


      call project_ati(psi,eigenvectors, mat, ndim)

      do i =1, indxmax
        write(30, *) w(i), pes(i)
      end do

      previous_steps = t_int(counter)

    end do
    call stop_timing
    call print_timing

    print *,  '***End Time Loop***'

  end subroutine Electron_Spectra_CN_length

  !////////////////////////////////////////////////////////////////////////////
  subroutine electron_spectra_sb_cn_l(pulse_shape, bandnum )
    use banded_matrices
    use sb_crank_nicolson, only:
    type(banded_sym_mat)          :: mat

    real(8)                       :: E_t, t, test
    real(8), allocatable          :: prob(:)
    real(8), allocatable          :: time_series(:)
    real(8), allocatable          :: eigenval(:)

    integer, intent(in)           :: bandnum
    integer, allocatable          :: t_int(:)
    integer                       :: i,j,k, counter, previous_steps
    integer                       :: tslength,int_t_steps

    complex(8), allocatable       :: rhs_mat_diagonal(:),rd_saved(:)
    complex(8), allocatable       :: rhs_mat_offdiagonal(:,:)
    complex(8), allocatable       :: lhs_mat_diagonal(:),ld_saved(:)
    complex(8), allocatable       :: lhs_mat_offdiagonal(:,:)
    complex(8), allocatable       :: E_pot(:)
    complex(8), allocatable       :: psi(:), ans(:)
    complex(8), allocatable       :: psi0(:,:)
    complex(8)                    :: projection
    complex(8)                    :: zdotc

    character(len=*)              :: pulse_shape

    procedure(pulse_at_t_func), pointer :: pulse

    open(unit=33,file=datafilename)
    print*, 'ati for genaral banded symmetric matrix with cranck-nicolson'// &
      'in length gauge'
    print*, pulse_shape

    pulse => select_pulse_type(pulse_shape, 'length')

    ! allocae and fill various variable pertinent to the method
    allocate(E_pot(m_size),ans(m_size))
    allocate(ld_saved(m_size), rd_saved(m_size))

    call convert_cycles_to_times(time_series,t_int)
    call check_time_series(time_series,tslength)

    call mat%make_banded_matrix_on_grid(x,h,bandnum,soft_core_eps)
    call mat%eigenvalues_only(eigenval)


    ! *******************************************************************
    indxmin = 1
    test = 0.d0

    do k = 2, m_size - 2
      test  = 0.25*(sum(eigenval(k-1:k+2))) / omega
      if(test >= 6.d0) exit
    end do

    indxmax = k + 2

    ndim = indxmax - 1
    print*, "ndim of spectrum:"   , ndim

    print*, "indxmin:", indxmin, eigenval(indxmin)
    print*, "indxmax:", indxmax, eigenval(indxmax)

    ! *******finding quadruple-avereged eigenvalues weighted by omega ***
    allocate(w(ndim))
    do k = 2, indxmax
      w(k-1)  = 0.25*(sum(eigenval(k-1:k+2))) / omega
    end do

    write(33, *) ndim -2, tslength

    allocate(pes(ndim-2),peseven(ndim-2),pesodd(ndim-2),prob(ndim-2))
    allocate(rhs_mat_diagonal(mat%mat_size))
    allocate(rhs_mat_offdiagonal(mat%mat_size-1,mat%bsz))
    allocate(lhs_mat_diagonal(mat%mat_size))
    allocate(lhs_mat_offdiagonal(mat%mat_size-1,mat%bsz))
    !***********************************************************************
    ! find all eigenvectors between
    !indxmin - 2 (=1) and indxmax - 1 (ndim of them)
    print*, 'finding corresponding eigenvectors...'
    call mat%eigensystem(ndim, psi, psi0)
    print*, 'done'

    print*, psi((m_size-1)/2)!; stop


    rhs_mat_diagonal(:) = 1.d0 - idt * mat%diagonal(:)
    rhs_mat_offdiagonal(:,:) = - idt * mat%offdiagonal(:,:)
    !
    lhs_mat_diagonal(:) = 1.d0 + idt * mat%diagonal(:)
    lhs_mat_offdiagonal(:,:) = + idt * mat%offdiagonal(:,:)


    ld_saved = lhs_mat_diagonal
    rd_saved = rhs_mat_diagonal
    previous_steps = 0
    print *,  '***Begin Time Loop***'
    call begin_timing
    t = 0.d0
    do counter = 1, tslength

      int_t_steps = t_int(counter) - previous_steps
      do
        do i = 1, int_t_steps
          !     update pulse
          E_t = pulse(t + 0.5d0 * dt) !; print*, 'pulse: ', E_t
          E_pot(:) = idt * E_t * x(:) !;  print*, 'pulse pot_at: ', E_pot(mat_size)
          !update diagonals
          rhs_mat_diagonal = rd_saved + E_pot
          lhs_mat_diagonal = ld_saved - E_pot

          call zmat_band_sym_zvec_mul(rhs_mat_diagonal, rhs_mat_offdiagonal, psi,ans)

          call find_lhs_vec(lhs_mat_diagonal, lhs_mat_offdiagonal,ans)

          psi = ans
          !
          t = t + dt!; print*, 'time: ', t
        end do

        int_t_steps = int(abs(time_series(counter)-t)/dt)
        !print*, "steps to go:", int_t_steps, "@t:", t, "with dt:", dt
        if(int_t_steps<1)exit

      end do

      print*,'counter',counter,'time:',time_series(counter),'t:',t

      previous_steps = t_int(counter)

      pes  = 0.d0
      do k = 2, ndim - 1
        i = k - 1
        projection = zdotc(m_size,psi0(:,k),1,psi,1)
        peseven(i) = real(projection*conjg(projection))
        peseven(i) = peseven(i) / (eigenval(k+1)-eigenval(k-1))
        !
        projection = zdotc(m_size,psi0(:,k+1),1,psi,1)
        pesodd(i) =  real (projection*conjg(projection))
        !
        pesodd(i) = pesodd(i)/(eigenval(k+2)-eigenval(k))
        !
        pes(i)    = peseven(i) + pesodd(i)

      end do

      do i =1, ndim - 2
        write(33, *) w(i), pes(i)
      end do

    end do



    call stop_timing
    call print_timing
    close(33)

  end subroutine electron_spectra_sb_cn_l

  !//////////////////////////////////////////////////////////////////////////////

  !!*******************************************************************
  !! symetric complex matrix vector multiplier
  !!********************************************************************
  subroutine zmat_band_sym_zvec_mul(zmatd,zmato,vec, ans)
    integer    :: i, j
    integer    :: b, m_size
    complex(8) :: zmatd(:), zmato(:,:)
    complex(8) :: vec(:)
    complex(8) :: ans(:)
    complex(8) :: z_zero = (0.d0, 0.d0)

    b      = size(zmato, 2)
    m_size = size(zmatd)

    ans(1:m_size) = zmatd(1:m_size) * vec(1:m_size)

    do i = 1, b
      ans(1:m_size) = ans(1:m_size) + &
        [zmato(1:m_size-i,i) * vec(i+1:m_size),(z_zero, j=1,i)]
      ans(1:m_size) = ans(1:m_size) + &
        [(z_zero, j=1,i),zmato(1:m_size-i,i) * vec(1:m_size-i)]
    end do

  end subroutine zmat_band_sym_zvec_mul
  !//////////////////////////////////////////////////////////////////////////

  subroutine cn_driver_l(E_t)
    
    procedure(potential_func), pointer :: potential
    integer                :: i
    real(8), intent(in)    :: E_t

    ! Filling out the Carnk-Nicolson matrices
    !update of diagonals & off-diagonals unfortunately the
    !zgtsv solver destroys the diagonals and off-diagonals
    !diagonals

    potential => select_potential_type(potential_type)
    
    dcn_r(:)=1.d0 - idt*( 1.d0/h**2 + potential(x(:)) - E_t * x(i))
    dcn_l(:)=1.d0 + idt*( 1.d0/h**2 + potential(x(:)) - E_t * x(i))
    
    !off-diagonals
    do i=l_indx,h_indx-1
      ecn_r_up (i)=-idt*(-0.5d0/h**2)
      ecn_l_up (i)= idt*(-0.5d0/h**2)
    end do
    do i=l_indx+1, h_indx
      ecn_r_dwn(i)=-idt*(-0.5d0/h**2)
      ecn_l_dwn(i)= idt*(-0.5d0/h**2)
    end do
    !applying Crank-Nicolson:
    ! building right-hand-side vector or B vector here
    rh_vec(l_indx) =  dcn_r(l_indx) * psi(l_indx)  +  &
      ecn_r_up (l_indx) * psi(l_indx+1)
    do i = l_indx+1, h_indx-1
      rh_vec(i) =  dcn_r(i) * psi(i) + ecn_r_dwn(i) * psi(i-1) + &
        ecn_r_up (i) * psi(i+1)
    end do
    rh_vec(h_indx) =  dcn_r(h_indx) * psi(h_indx) + &
      ecn_r_dwn(h_indx) * psi(h_indx-1)

    ! solve lh_mat * psi(new) = rh_mat * psi(old) = rh_vec
    ! by zgtsv(N, NRHS, DL, D, DU, B, LDB, INFO) that solves
    ! complex A X = B system N is the dimension of A, NRHS is the number
    ! of right hand sides, i.e., the number of columns
    ! On entry, DL must contain the (n-1) subdiagonal elements of A.
    ! On entry, D must contain the diagonal elements of A
    ! On entry, DU must contain the (n-1) superdiagonal elements of A.
    ! On entry, the N-by-NRHS right hand side matrix B. On exit, if INFO = 0,
    ! the N-by-NRHS solution matrix X.
    ! LDB is the leading dimension of the array B.
    !  LDB >= max(1,N) of the matrix B.
    call zgtsv(m_size, 1, ecn_l_dwn, dcn_l, ecn_l_up, rh_vec , m_size, info)
    if (info /= 0) then
      print*,'info=',info
      stop
    endif

    ! zgtsv puts the answer in rh_vec (vector B)

    psi = rh_vec

  end subroutine


  !!***********************************************************************
  !! Using lapack's zgbsv for linear-solver Ax=B
  !! zgbsv is a general banded matrix linear solver
  !!************************************************************************
  subroutine find_lhs_vec(zmatd, zmato, ans)

    integer    :: b, m_size, b3p1, i,j, k ,info
    integer, allocatable :: ipiv(:)
    complex(8) :: zmatd(:), zmato(:,:)
    complex(8) :: ans(:)
    complex(8), allocatable :: ab(:,:)
    complex(8) :: z_zero = (0.d0, 0.d0)

    m_size = size(zmatd)
    b = size(zmato,2)
    b3p1 = 3 * b + 1

    allocate(ab(b3p1,m_size), ipiv(m_size))
    ab(2 * b + 1, :) = zmatd(:)

    k = 0
    do j = 2 * b + 2, b3p1
      k  =  k + 1
      ab(j,:) = [zmato(1:m_size-k,k),(z_zero,i=1,k)]
    end do

    k = 0
    do j =  2 * b, b + 1, -1
      k  =  k + 1
      ab(j, :) = [(z_zero, i=1,k),zmato(1:m_size-k,k)]
    end do

    call zgbsv (m_size, b, b, 1, ab, b3p1, ipiv, ans, m_size, info)

  end subroutine

  !/////////////////////////////////////////////////////////////////////////////

  subroutine project_ati(psi_t,eigenvectors,mat,ndim )

    complex(8),dimension(:,:)    :: eigenvectors
    complex(8)                   :: zdotc
    complex(8)                   :: projection
    complex(8)                   :: psi_t(:)
    type(some_vec)               :: mat

    integer                      :: i, k, ndim, indxmin , indxmax

    do k = 2, ndim - 1

      i = k - 1

      projection = zdotc(m_size,eigenvectors(:,k),1,psi_t,1)
      peseven(i) = real(projection*conjg(projection))

      peseven(i) = peseven(i) / (mat%eigenval(k+1)-mat%eigenval(k-1))


      projection = zdotc(m_size,eigenvectors(:,k+1),1,psi_t,1)
      pesodd(i) =  real(projection*conjg(projection))

      pesodd(i) = pesodd(i)/(mat%eigenval(k+2)-mat%eigenval(k))

      pes(i) = peseven(i) + pesodd(i)

    end do

  end subroutine project_ati

  !//////////////////////////////////////////////////////////////////////////////


  subroutine Electron_Spectra_CN_velocity(pulse_shape)

    type(tridiagonal_mat)       :: hamiltonian
    type(some_vec)              :: mat

    character(len=*)            :: pulse_shape
    character(len=1)            :: gauge

    complex(8)                  :: zdotc
    complex(8)                  :: projection
    complex(8),allocatable      :: eigenvectors(:,:)
    complex(8)                  :: psi_conv(l_indx:h_indx)

    integer                     :: i, j, k, m, inc
    integer                     :: kk(1)
    integer                     :: pulse_switch
    integer                     :: tslength
    integer                     :: counter
    integer                     :: int_t_steps, previous_steps
    integer, allocatable        :: t_int(:)

    real(8), allocatable        :: time_series(:)
    real(8)                     :: n_energy
    real(8)                     :: ddot
    real(8)                     :: A_t

    procedure(pulse_at_t_func),pointer :: pulse

    !***************************************************************************

    print*, "**** ati spectrum with CN in velocty gauge ****"
    print*, "pulse shape: ", pulse_shape
    !    ***************************************************************
    !     allocae and fill various variable pertinent to the method
    call allocate_CN_variables

    call convert_cycles_to_times(time_series,t_int)
    !check the time intervals given fit in the time of simulation
    !disregard times that are larger than simulation time
    call check_time_series(time_series,tslength)
    !
    call fill_matrix(hamiltonian)

    call calculate_eigenvalues(hamiltonian,mat)

    call find_spectrum_dimensions(mat,ndim)


    allocate(w(ndim))
    indxmax = ndim - 2
    allocate(pes(indxmax),peseven(indxmax),pesodd(indxmax))

    !finding quadruple-avereged eigenvalues weighted by omega
    call find_normalized_quadruple_energy(w,mat%eigenval)

    ! find all eigenvectors (ndim of them)
    print*, 'finding corresponding eigenvectors...'
    call find_some_eigenvectors(mat,ndim,eigenvectors)


    open(unit=30,file=datafilename)
    write(30, *) indxmax , tslength
    !
    previous_steps = 0
    counter = 1
    t       = 0.d0
    pulse => select_pulse_type(pulse_shape, 'velocity')

    pulse_switch = 1

    counter = 1
    !       do j = 0, t_tot
    !         t  = j * dt
    t = 0.d0; j = 1

    print *,  '***Begin Time Loop***'
    print *,  'time after which the pulse is off:', t_on
    ! time the time loop
    call begin_timing
    do counter = 1, tslength

      int_t_steps = t_int(counter) - previous_steps
      print*, counter, "int_t_steps:", int_t_steps
      do
        do i = 1, int_t_steps
          !     update pulse
          A_t = pulse(t+0.5d0*dt)
          !******propagation*******
          call cn_driver_v (A_t)
          !************************
          t   = t + dt
        end do

        int_t_steps = int(abs(time_series(counter)-t)/dt)
        print*, "steps to go:", int_t_steps, "@t:", t, "with dt:", dt
        if(int_t_steps<=1)exit

      end do

      print*,'counter',counter,'time:',time_series(counter),'t:',t

      psi_conv = psi * exp(-ii*A_t* x)
      call project_ati(psi_conv,eigenvectors, mat, ndim)

      do i =1, indxmax
        write(30, *) w(i), pes(i)
      end do

      previous_steps = t_int(counter)

    end do
    call stop_timing
    call print_timing

    print *,  '***End Time Loop***'


  end subroutine Electron_Spectra_CN_velocity

  !//////////////////////////////////////////////////////////////////////////////

  subroutine allocate_CN_variables

    integer    :: ierr
    procedure(potential_func), pointer :: potential


    if(.not. allocated(proj))then
      allocate(proj(1:m_found),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, proj allocation in Carnk-Nicolson failed!'
      endif
    endif

    if(.not. allocated(rh_vec))then
      allocate(rh_vec(l_indx:h_indx), stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, rh_vec allocation in Carnk-Nicolson failed!'
      endif
    endif

    if(.not. allocated(dcn_r))then
      allocate(dcn_r(l_indx:h_indx),dcn_l(l_indx:h_indx),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, dcn_r allocation failed!'
      endif
    endif

    if(.not. allocated(dcn_r_saved))then
      allocate(dcn_r_saved(l_indx:h_indx),dcn_l_saved(l_indx:h_indx),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, dcn_r allocation failed!'
      endif
    endif

    potential => select_potential_type(potential_type)

    dcn_r_saved(:) = 1.d0 - idt*( 1.d0/h**2 + potential(x(:)))
    dcn_l_saved(:) = 1.d0 + idt*( 1.d0/h**2 + potential(x(:)))

    if(.not. allocated(ecn_r_up))then
      allocate(ecn_r_up (l_indx:h_indx-1),ecn_l_up (l_indx:h_indx-1),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, ecn_r_up allocation failed!'
      endif
    endif

    if(.not. allocated(ecn_r_up_saved))then
      allocate(ecn_r_up_saved (l_indx:h_indx-1),ecn_l_up_saved (l_indx:h_indx-1) &
        ,stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, ecn_r_up allocation failed!'
      endif
    endif

    ecn_r_up_saved (:)=-idt*(-0.5d0/h**2 )
    ecn_l_up_saved (:)= idt*(-0.5d0/h**2 )

    if(.not. allocated(ecn_r_dwn))then
      allocate(ecn_r_dwn(l_indx+1:h_indx),ecn_l_dwn(l_indx+1:h_indx),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, ecn_r_dwn allocation failed!'
      endif
    endif


  end subroutine allocate_CN_variables

  !//////////////////////////////////////////////////////////////////////////////

  ! subroutine pulse_initializer(gauge,pulse_shape,A_pulse,E_pulse)
  !
  !   character(len=1)            :: gauge
  !   character(len=*)            :: pulse_shape
  !   type(E_smooth_pulse)        :: E_pulse
  !   type(A_smooth_pulse)        :: A_pulse
  !
  !   if(gauge == 'v')then
  !
  !     select case(pulse_shape)
  !
  !     case('smooth_pulse')
  !
  !       call A_pulse%initialize_velocity(E_0, omega, t_on)
  !
  !     case('square_pulse')
  !
  !       call A_pulse%velocity_gauge_field%initialize_velocity&
  !         (E_0, omega,t_on,phase)
  !
  !     end select
  !
  !   elseif (gauge =='l') then
  !
  !     select case(pulse_shape)
  !
  !     case('smooth_pulse')
  !
  !       call E_pulse%initialize_length(E_0, omega, t_on)
  !
  !     case('square_pulse')
  !
  !       call E_pulse%length_gauge_field%initialize_length(E_0, omega, t_on, phase)
  !
  !     end select
  !
  !   else
  !
  !     stop 'gauge has to be either velocity or length'
  !
  !   end if
  !
  !
  ! end subroutine

  !//////////////////////////////////////////////////////////////////////////////

  subroutine cn_driver_v(A_t)

    real(8)                     :: A_t
    integer                     :: i
    procedure(potential_func), pointer :: potential


    ! Filling out the Carnk-Nicolson matrices
    !update of diagonals & off-diagonals unfortunately the
    !zgtsv solver destroys the diagonals and off-diagonals
    !diagonals
    potential => select_potential_type(potential_type)
    dcn_r(:)=1.d0 - idt*( 1.d0/h**2 + potential(x(:)))
    dcn_l(:)=1.d0 + idt*( 1.d0/h**2 + potential(x(:)))
    
    !off-diagonals
    do i=l_indx,h_indx-1
      ecn_r_up (i)=-idt*(-0.5d0/h**2 + 0.5d0 * ii * A_t / h)
      ecn_l_up (i)= idt*(-0.5d0/h**2 + 0.5d0 * ii * A_t / h)
    end do
    do i=l_indx+1,h_indx
      ecn_r_dwn(i)=-idt*(-0.5d0/h**2 - 0.5d0 * ii * A_t / h)
      ecn_l_dwn(i)= idt*(-0.5d0/h**2 - 0.5d0 * ii * A_t / h)
    end do
    !applying Crank-Nicolson:
    ! building right-hand-side vector or B vector here
    rh_vec(l_indx) =  dcn_r(l_indx) * psi(l_indx)  + &
      ecn_r_up (l_indx) * psi(l_indx+1)
    do i = l_indx+1, h_indx-1
      rh_vec(i) =  dcn_r(i) * psi(i) + ecn_r_dwn(i) * psi(i-1) + &
        ecn_r_up (i) * psi(i+1)
    end do
    rh_vec(h_indx) =  dcn_r(h_indx) * psi(h_indx) +  &
      ecn_r_dwn(h_indx) * psi(h_indx-1)

    ! solve lh_mat * psi(new) = rh_mat * psi(old) = rh_vec
    ! by zgtsv(N, NRHS, DL, D, DU, B, LDB, INFO) that
    !solves complex A X = B system
    ! N is the dimension of A, NRHS is the number of right hand sides,
    ! i.e., the number of columns
    ! On entry, DL must contain the (n-1) subdiagonal elements of A.
    ! On entry, D must contain the diagonal elements of A
    ! On entry, DU must contain the (n-1) superdiagonal elements of A.
    ! On entry, the N-by-NRHS right hand side matrix B. On exit,
    !if INFO = 0, the N-by-NRHS solution matrix X.
    ! LDB is the leading dimension of the array B.
    !  LDB >= max(1,N) of the matrix B.
    call zgtsv(m_size, 1, ecn_l_dwn, dcn_l, ecn_l_up, rh_vec , m_size, info)
    if (info /= 0) then
      print*,'info=',info
      stop
    endif

    ! zgtsv puts the answer in rh_vec (vector B)

    psi = rh_vec


  end subroutine

  !//////////////////////////////////////////////////////////////////////////////

  subroutine Electron_Spectra_LNZ_length(pulse_shape)

    type(some_vec)              :: mat
    !type(E_smooth_pulse)        :: pulse
    !type(A_smooth_pulse)        :: A_pulse
    procedure(pulse_at_t_func),pointer :: pulse

    character(len=*)            :: pulse_shape
    character(len=1)            :: gauge

    complex(8)                  :: zdotc
    complex(8)                  :: projection
    complex(8),allocatable      :: eigenvectors(:,:)
    complex(8)                  :: psitest(m_size,4)
    complex(8)                  :: psi_conv(m_size)

    integer                     :: i, j, k, m, inc
    integer                     :: kk(1)
    integer                     :: pulse_switch
    integer                     :: tslength
    integer                     :: counter
    integer                     :: int_t_steps, previous_steps
    integer, allocatable        :: t_int(:)

    real(8), allocatable        :: time_series(:)
    real(8),dimension(:), allocatable        :: t_auxilary
    real(8)                     :: n_energy
    real(8)                     :: ddot
    real(8)                     :: A_t

    !length of time set
    integer                             :: tlength
    !length of probability set
    integer                             :: plength
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !time, incremented from 0 by dt upto t_intv
    real(8)                             :: t
    !lanczo specific dt (dt_prime) which can decrease below dt
    real(8)                             :: dt_p, dt_saved
    !electric field that is evolving with time
    real(8)                             :: E_t
    !initial time for timing the loop (cpu time)
    real(8)                             :: t_i
    !final time for timing the loop (cpu time)
    real(8)                             :: t_f
    !set containing the time increment with dt_p
    real(8), allocatable,dimension(:)   :: tdata
    !set containing the probability projections
    real(8), allocatable,dimension(:,:) :: pdata
    !probability of projection of the propagated wave onto
    !the ground (0) and neig number of excited states
    !, temporary holder, the final agregate data is in pdata
    real(8), dimension(0:m_found)          :: prob
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !initial tridiagonal matrix of the system
    type(tridiagonal_mat)               :: hamiltonian0
    !time-evolved tridiagonal matrix of the system
    type(tridiagonal_mat)               :: hamiltonian_t

    !linked list that records time, the start of the list
    type(cell),pointer                  :: inital_time
    !linked list that records time, the rest of the list
    type(cell),pointer                  :: time
    !linked list that records probability array, the start of the list
    type(cell_array),pointer            :: i_p
    !linked list that records probability array, the rest of the list
    type(cell_array),pointer            :: p
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !the ground state eigenvector of tdmat, that is evolved with time
    complex(8)                              :: psi_0(1:m_size)
    !the ground and neig number of excited states that are saved
    !      complex(8),allocatable,dimension(:,:)   :: vec0
    !      complex(8),dimension(0:m_found)         :: proj
    !i*dt of the exp (i*dt*D)
    complex(8)                              :: factor

    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !logical key that determines the performance of
    !full Gram_Schmidt procedure everytime
    logical                             :: GS
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    character(20)                       :: fmt
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    print '("***Lanczos with maximum iteration of",x,I3,x"***")',lancz_itnum
    print *, 'lanczos tolerance:',lanc_threshold

    print*, pulse_shape
    pulse => select_pulse_type(pulse_shape, 'length')
    !    *************************************************************************
    !     allocae and fill various variable pertinent to the method
    call initialize_tdmatrix(tdmat_alpha)
    allocate(rh_vec(1:m_size),q(m_size,lancz_itnum), qsesqv(1:m_size))
    !    *************************************************************************

    !        factor is double idt because idt already has been halved...
    factor = -2.d0*idt

    call convert_cycles_to_times(time_series,t_int)
    !check the time intervals given fit in the time of simulation
    !disregard times that are larger than simulation time
    call check_time_series(time_series,tslength)

    t_auxilary(:) = time_series (:) - dt
    !
    call fill_matrix(hamiltonian0)
    call fill_matrix(hamiltonian_t)

    call calculate_eigenvalues(hamiltonian0,mat)

    call find_spectrum_dimensions(mat,ndim)


    allocate(w(ndim))
    indxmax = ndim - 2
    allocate(pes(indxmax),peseven(indxmax),pesodd(indxmax))

    !finding quadruple-avereged eigenvalues weighted by omega
    call find_normalized_quadruple_energy(w,mat%eigenval)

    ! find all eigenvectors (ndim of them)
    print*, 'finding corresponding eigenvectors...'
    call find_some_eigenvectors(mat,ndim,eigenvectors)

    open(unit=30,file=datafilename)

    write(30, *) indxmax , tslength

    dt_p     = dt
    dt_saved = dt
    t        = 0.d0
    counter  = 1
    j        = 1
    print*, "initial number of steps: ",t_int(counter)


    previous_steps = 0
    print *,  '***Begin Time Loop***'
    print *,  'time after which the pulse is off:', t_on
    !               time the time loop
    call begin_timing
    do counter = 1, tslength !while (counter <= tslength)

      int_t_steps = t_int(counter) - previous_steps

      do !while(int_t_steps>=1) !(t <= t_auxilary(counter))

        do i = 1, int_t_steps
          !                    update pulse
          E_t = pulse(t+0.5d0*dt_p)

          !                     update diagonal in this case
          hamiltonian_t%diagonal(:)     = hamiltonian0%diagonal(:) &
            - x(:) * E_t

          !                     propagate the vector (psi_0) in time
          call time_dep_lanczos_tridiag2(hamiltonian_t,lancz_itnum,dt_p,factor,&
            psi,lanc_threshold)!,GS)

          !                     update factor
          factor = -ii * dt_p
          !                     update time
          t   = t + dt_p
        end do

        int_t_steps = int(abs(time_series(counter)-t)/dt_p)
        !print*, "steps to go:", int_t_steps, "@t:", t, "with dt:", dt_p
        if(int_t_steps<=1)exit

      end do

      !                    t = t - dt_p
      ! !                  now we slow down to 0.01
      !                    dt_p = 1.0d-03
      !                    factor = -ii * dt_p
      !
      !                    do while(t <= time_series(counter))
      !
      ! !                    update pulse
      !                       E_t = pulse%length_gauge_field%Efield(t+0.5d0*dt_p)
      !
      ! !                     update diagonal in this case
      !                       tdmat_t%diagonal(:)     = tdmat%diagonal(:)  - x(:) * E_t
      !
      ! !                     propagate the vector (psi_0) in time
      !                       call time_dep_lanczos_tridiag2(tdmat_t,lancz_itnum,dt_p,factor,psi_0,lanc_threshold)!,GS)
      !
      ! !                     update factor
      !                       factor = -ii * dt_p
      ! !                     update time
      !                       t   = t + dt_p
      !
      !
      !                    end do
      !
      print*,'counter',counter,'time:',time_series(counter),'t:',t!-dt_p
      pes  = 0.d0
      call project_ati(psi, eigenvectors, mat, ndim)

      do i =1, indxmax
        write(30, *) w(i), pes(i)
      end do

      previous_steps = t_int(counter)

    end do
    call stop_timing
    call print_timing

    print *,  '***End Time Loop***'
    print*, 'time in a.u.', t



  end subroutine

  !//////////////////////////////////////////////////////////////////////////////////

  subroutine time_dep_lanczos_tridiag2(tdmat_t,itnum,dt_p,factor,v,threshold,GS)
    integer                             :: i,j,k,m
    integer, intent(in)                 :: itnum
    integer, parameter                  :: sch_const = 1
    integer                             :: min_it
    integer                             :: mat_size
    ! !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    real(8), parameter                  :: sch_tol = 1.d-10
    real(8),intent(in)                  :: threshold
    real(8)                             :: convfac
    real(8)                             :: convfac_old
    real(8)                             :: error
    real(8)                             :: dt_p, dt_psaved
    real(8)                             :: factorial(1:itnum)
    real(8)                             :: alpha(itnum)
    real(8)                             :: beta (itnum)
    real(8)                             :: eigvec_k(itnum,itnum)
    real(8)                             :: eigval_k(itnum)
    real(8)                             :: eigvec_old(itnum,itnum)
    real(8)                             :: eigval_old(itnum)
    ! !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    type(tridiagonal_mat),intent(in)    :: tdmat_t
    type(all_vec)                       :: mat_lanczos
    ! !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ! !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !             complex(8),dimension (tdmat_t%mat_size,itnum)  :: q
    complex(8),dimension (   itnum)                :: qv
    complex(8),dimension (   itnum)                :: sqv
    complex(8),dimension (   itnum)                :: sqv_old
    complex(8),dimension (   itnum)                :: esqv
    complex(8),dimension (   itnum)                :: sesqv
    complex(8),dimension (   itnum)                :: sesqv_old
    ! !!            complex(8),dimension (tdmat_t%mat_size)        :: qsesqv
    ! !!            complex(8),dimension (tdmat_t%mat_size)        :: rh_vec
    complex(8),dimension (m_size)                  :: v
    complex(8)                                     :: factor
    complex(8)                                     :: newFactor
    complex(8)                                     :: zdotc
    ! !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logical                                        :: convg
    logical, optional                              :: GS
    !

    !              alpha    = 0.d0
    !              beta     = 0.d0
    !              eigval_k = 0.d0
    !            eigval_old = 0.d0
    !              eigvec_k = 0.d0
    !            eigvec_old = 0.d0
    !            qv        = z_zero
    q        = z_zero
    !           sqv        = z_zero
    !           sqv_old    = z_zero
    !          esqv        = z_zero
    sesqv        = z_zero
    sesqv_old    = z_zero
    !         qsesqv       = z_zero
    !         rh_vec       = z_zero
    convg     = .false.
    mat_size  = tdmat_t%mat_size
    !            tdmat_alpha = tdmat_t
    !
    !            ! First iteration*******************************************

    !              print*, size(v),mat_size
    !              print*,zdotc(mat_size,v,1,v,1)

    q(:,1)= v

    q(:,1) = q(:,1)/normz(v)

    !alpha(1) = q(:,1)* H_0 * q(:,1)

    alpha(1) = find_alpha(tdmat_t,q(:,1),m_size)
    !

    !Performing projection on incoming vector**********************
    sqv_old  (1) = zdotc(mat_size,q(:,1),1,v,1)
    sesqv_old(1) = exp(factor*alpha(1))* sqv_old(1)

    !
    ! Finding the q of second iteration*****************************
    !
    !
    tdmat_alpha%diagonal = tdmat_t%diagonal - alpha(1)

    call mat_vec_mul(tdmat_alpha,q(:,1),rh_vec)

    beta(1) = real(sqrt(zdotc(mat_size,rh_vec(:),1,rh_vec(:),1)))

    q(:,2)=rh_vec(:)/beta(1)

    do k= 2, itnum

      !                ! Find alpha(k):
      alpha(k) = find_alpha(tdmat_t,q(:,k),mat_size)

      !Finding the eigen values/vectors of the Lanczos
      !tridiagonal matrix
      call lanczos_diagonalizer(alpha(1:k),beta(1:k-1)&
        ,eigvec_k(1:k,1:k),eigval_k(1:k),k)

      ! Finding the projection of Lanczos tridiagonal
      ! matrix on the wave
      call proj_exp_q(q(:,1:k),v,eigvec_k(1:k,1:k),eigval_k(1:k), &
        mat_size,k,factor,sqv(1:k),sesqv(1:k))


      convfac = real(sqrt(zdotc(itnum,sesqv-sesqv_old,1,sesqv-sesqv_old,1)))

      if(convfac >= convfac_old) then

        !                ! Gram_Schmidt orthogonalizing of qs
        call zschmab(q(1,1),q(1,k),sch_tol,mat_size,sch_const,1,m)

        alpha(k) = find_alpha(tdmat_t,q(:,k),mat_size)
        !


        !Finding the eigen values/vectors of the Lanczos
        !tridiagonal matrix
        call lanczos_diagonalizer(alpha(1:k),beta(1:k-1)&
          ,eigvec_k(1:k,1:k),eigval_k(1:k),k)

        ! Finding the projection of Lanczos tridiagonal
        ! matrix on the wave
        call proj_exp_q(q(:,1:k),v,eigvec_k(1:k,1:k),eigval_k(1:k), &
          mat_size,k,factor,sqv(1:k),sesqv(1:k))


        convfac = real(sqrt(zdotc(itnum,sesqv-sesqv_old,1,sesqv-sesqv_old,1)))

      end if

      if(convfac<=threshold)then
        convg = .true.
        call mat_vec_mul(q,sesqv,qsesqv,mat_size,itnum)
        v = qsesqv
        exit
      elseif(k<itnum)then
        sesqv_old            = sesqv
        sqv_old              = sqv
        eigvec_old           = eigvec_k
        eigval_old           = eigval_k
        tdmat_alpha%diagonal = tdmat_t%diagonal - alpha(k)
        convfac_old          = convfac

        call mat_vec_mul(tdmat_alpha,q(:,k),rh_vec)

        rh_vec(:)   = rh_vec(:) - beta(k-1)* q(:,k-1)

        beta(k)  = real(sqrt(zdotc(mat_size,rh_vec(:),1,rh_vec(:),1)))

        q(:,k+1) = rh_vec(:)/beta(k)

      end if

    end do ! lanczos loop without Gram_Schmidt


    if(.not. convg) then
      ! !       If by this point convergence was not reached, therefore a time step reduction
      ! !       might be beneficial
      newFactor = factor/2

      ! !       reseting k to the last unconverged iteration
      i = 1
      j = 2
      dt_psaved=dt_p
      k = itnum
      ! !        we only need to try the last two iterations of lanczos with the new time.
      do while (.not. convg)

        esqv = z_zero

        esqv(1:k-1) = exp(newFactor*eigval_old(1:k-1))*sqv_old(1:k-1)

        call mat_vec_mul(eigvec_old,esqv(1:k-1),sesqv_old,k-1,k-1)

        esqv = z_zero

        esqv(1:k) = exp(newFactor*eigval_k(1:k))*sqv(1:k)

        call mat_vec_mul(eigvec_k,esqv(1:k),sesqv,k,k)

        convfac = real(sqrt(zdotc(itnum,sesqv-sesqv_old,1,sesqv-sesqv_old,1)))
        !print*, convfac

        if(convfac <= threshold) then
          ! ! projection to the original basis
          call mat_vec_mul(q,sesqv,qsesqv,mat_size,itnum)
          v = qsesqv
          dt_p = dt_psaved/j
          convg = .true.
          !print*, 'j:',j

        else

          if(i==10)stop 'reducing time-step size is not helping'
          newFactor = factor/j !newFactor/2

          i = i +1

          j = 2 **i

        end if

      end do

      !                      print*, "number of times halved:",i , 'dt_p:', dt_p
    end if


  end subroutine

  !//////////////////////////////////////////////////////////////////////////////////

  subroutine initialize_tdmatrix(tdmat)
    !*****************************************************************
    ! Based on known parameters h and m_size, fills the tridiagonal
    ! matrix. This particular case is filled with the diagonal
    ! and off-diagonal elements as described by Eberly for the length
    ! gauge of a 1dimensional particle in a box.
    !************************************************************************

    integer                    :: i
    type(tridiagonal_mat)      :: tdmat

    call fill_matrix(tdmat)

  end subroutine initialize_tdmatrix

  !//////////////////////////////////////////////////////////////////////////////////

  function find_alpha(tdmat, vec, ndim) result(alpha)
    ! !*************************************************************************
    ! !  Alpha as normaly described in Lanczos algorithms as the diagonal element
    !    of Lanczos tridiagonal.
    ! !*************************************************************************
    integer, intent(in)                     :: ndim
    type(tridiagonal_mat)                   :: tdmat
    complex(8), dimension(ndim), intent(in) :: vec
    complex(8), dimension(ndim)             :: rh_vec
    complex(8)                              :: zdotc
    real(8)                                 :: alpha

    call mat_vec_mul(tdmat,vec,rh_vec)

    !        alpha(k) = q(:,k)* H_0 * q(:,k)
    alpha = real(zdotc(ndim,vec,1,rh_vec,1))


  end function find_alpha

  !//////////////////////////////////////////////////////////////////////////////////

  function normz(vec) result(norm)
    !************************************************************************
    ! Normalizing a complex vec
    !************************************************************************
    complex(8)                            :: zdotc
    complex(8),intent(in), dimension(:)   :: vec
    real   (8)                            :: norm

    norm = real(sqrt(zdotc(size(vec),vec,1,vec,1)))

  end function normz
  ! !************************************************************************
  ! ! Diagonalizer for lanczos tridiagonal
  ! !************************************************************************
  subroutine lanczos_diagonalizer(alpha,beta,eigvec,eigval,ndim)

    type(all_vec)                               :: mat_lanczos
    real(8),dimension(ndim,ndim),intent(out)    :: eigvec
    real(8),dimension(ndim)     ,intent(out)    :: eigval
    real(8),dimension(ndim)     ,intent( in)    :: alpha
    real(8),dimension(ndim-1)   ,intent( in)    :: beta
    integer                                     :: ndim

    call mat_lanczos%mat_initialize(alpha,beta)
    ! !      print*,mat_all%mat_size
    call mat_lanczos%eigensystem_solve

    eigvec = mat_lanczos%eigenvec
    eigval = mat_lanczos%eigenval

  end subroutine lanczos_diagonalizer
  !************************************************************************
  ! ! projects exp onto the vector with appropriate q basis of lanczos
  !   a bit useless look at the next subroutine
  !************************************************************************
  subroutine proj_exp_vec(q,psi_0,eigvec,eigval,v,vq,ndim,k,factor)
    ! original matrix dimension
    integer   , intent(in)                :: ndim
    ! current iteration of lanczos, lanczos matrix dimension
    integer   , intent(in)                :: k
    ! imaginary i*dt factor of the exp
    complex(8), intent(in)                :: factor
    ! lanczos vectors dimensioned (ndim,k) at this point
    complex(8), intent(in),dimension(:,:) :: q
    ! overlap of q with the lanczos vector s(k,k)
    complex(8),allocatable,dimension(:,:) :: u
    ! overlap of exp of the diagonal of lanczos matrix and u
    complex(8),allocatable,dimension(:)   :: overlap
    ! final result vector, dimensioned ndim
    complex(8)            ,dimension(:)   :: v
    ! vector in the lanczos basis has dimension k
    complex(8)            ,dimension(:)   :: vq
    ! original vector being operated on from the left
    complex(8)            ,dimension(:)   :: psi_0
    ! mat soley used for matrix vector operations with generic functions
    ! eigenvector and eigenvalues of lanczos matrix dimensioned (k,k) & (k)
    real   (8), intent(in),dimension(:,:) :: eigvec
    real   (8), intent(in),dimension(:)   :: eigval

    allocate(u(ndim,k),overlap(k))


    ! !! u_(nxk) = Q_(nxk) * eigvec_(kxk)
    call cebcx(u,ndim,q,ndim,eigvec,k,ndim,k,k)

    ! !! overlap_(kx1) = u^t_(kxn) * psi_0_(nx1)

    call tmat_vec_mul(u,psi_0,overlap,ndim,k)

    ! !! scaling by exponential => overlap_(kx1)
    overlap(1:k)=exp(factor*eigval(1:k))*overlap(1:k)
    !
    ! !! v_(nx1) = u_(nxk) * overlap_(kx1)

    call mat_vec_mul(u,overlap,v,ndim,k)

    ! ! vq_(kx1) = Q^t_(kxn) * v_(nx1)
    call tmat_vec_mul(q,v,vq,ndim,k)

  end subroutine proj_exp_vec
  !**************************************************************************
  ! !  sesqv is the final vector in the lanczos basis; dimensioned k, the
  ! !  iteration number of the lanczos.
  ! !  This routine calclates sesqv and the the sqv(k) vector
  ! !  Here s is the (k,k) dimensioned modal matrix of lanczos
  ! !       q is the (ndim,k) matrix of lanczos vectors
  !**************************************************************************
  subroutine proj_exp_q(q,v,eigvec,eigval,ndim,k,factor,sqv,sesqv)
    integer   , intent(in)                  :: ndim
    integer   , intent(in)                  :: k
    complex(8), intent(in)                  :: factor
    complex(8), intent(in) ,dimension(:,:)  :: q
    complex(8), intent(in) ,dimension(ndim) :: v
    complex(8)             ,dimension(k)    :: qv
    complex(8), intent(out),dimension(k)    :: sqv
    complex(8)             ,dimension(k)    :: esqv
    complex(8), intent(out),dimension(k)    :: sesqv
    real   (8), intent(in),dimension(:,:)   :: eigvec
    real   (8), intent(in),dimension(:)     :: eigval

    !!             q^T_(kxn) * v(nx1) = qv (k,1)

    call tmat_vec_mul(q,v,qv,ndim,k)

    !!             s^T_(kxk) * qv_(kx1) = sqv_(kx1)
    call tmat_vec_mul(eigvec,qv,sqv,k,k)

    !!            exp(i dt eigenval)* I_(kxk) * sqv_(kx1)
    esqv(1:k) = exp(factor*eigval(1:k))*sqv(1:k)

    !!            S_(kxk) * esqv(kx1) = sesqv_(kx1)
    call mat_vec_mul(eigvec, esqv, sesqv, k,k)
  end subroutine proj_exp_q

  !***********************************************************************************************

  subroutine Electron_Spectra_GLNZ_length(pulse_shape,bandnum)
    use banded_matrices

    use lanczos_sym_banded, only : time_dep_lanczos_banded_sym
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    character(len=*)                   :: pulse_shape

    type(banded_sym_mat)               :: mat, mat_t

    integer                            :: i, j, m, inc, k
    integer                            :: int_t_steps, previous_steps
    integer                            :: tslength
    integer                            :: counter
    integer                            :: bandnum
    integer,dimension(:), allocatable  :: t_int


    real(8),dimension(:), allocatable  :: time_series
    real(8),dimension(:), allocatable  :: t_auxilary
    real(8),dimension(:), allocatable  :: eigenval
    real(8)                            :: t              !   !time, incremented from 0 by dt upto t_intv
    real(8)                            :: dt_p, dt_saved !   !lanczo specific dt (dt_prime) which can decrease below dt
    real(8)                            :: E_t
    real(8)                            :: test

    complex(8)                         :: factor
    complex(8), allocatable            :: psi_0(:)  ! the ground state eigenvector of tdmat, that is evolved with time
    complex(8), allocatable            :: eigenvectors(:,:)
    complex(8)                         :: zdotc
    complex(8)                         :: projection

    procedure(pulse_at_t_func),pointer :: pulse
    !    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


    print '("***Lanczos with maximum iteration of",x,I3,x"***")',lancz_itnum
    print *, 'lanczos tolerance:',lanc_threshold
    print*, pulse_shape

    call convert_cycles_to_times(time_series,t_int)
    !check the time intervals given fit in the time of simulation
    !disregard times that are larger than simulation time
    call check_time_series(time_series,tslength)
    !tslength = size(time_series)

    open(unit=33,file=datafilename)

    !*************************************************************************
    !     factor is double idt because idt already has been halved...
    factor = -2.d0*idt
    print*,'times for Electron Spectra:'
    !    adjust the times of recording to fit within total time of simulation

    allocate(t_auxilary(tslength))

    !
    print*, 'data calculated for', tslength, 'intervals:'
    print*, time_series(1:tslength)
    !
    t_auxilary(:) = time_series (1:tslength) - dt
    !
    !******Find Some Eigenvalues***************************
    !******and limited number of eigenvectors**************
    print*, 'Finding all Eigenvalues'
    !******* initialize a banded symetric matrix **************
    call mat%make_banded_matrix_on_grid(x,h,bandnum,soft_core_eps)
    call mat_t%make_banded_matrix_on_grid(x,h,bandnum)
    call mat%eigenvalues_only(eigenval)
    ! *******************************************************************
    ! *******************************************************************
    indxmin = 1
    test = 0.d0

    do k = 2, m_size - 2
      test  = 0.25*(sum(eigenval(k-1:k+2))) / omega
      if(test >= 6.d0) exit
    end do

    indxmax = k + 2

    ndim = indxmax - 1
    print*, "ndim of spectrum:"   , ndim

    print*, "indxmin:", indxmin, eigenval(indxmin)
    print*, "indxmax:", indxmax, eigenval(indxmax)

    ! *******finding quadruple-avereged eigenvalues weighted by omega ***
    allocate(w(ndim))
    do k = 2, indxmax
      w(k-1)  = 0.25*(sum(eigenval(k-1:k+2))) / omega
    end do

    write(33, *) ndim -2, tslength


    allocate(pes(ndim-2),peseven(ndim-2),pesodd(ndim-2))
    !***********************************************************************
    ! find all eigenvectors between
    !indxmin - 2 (=1) and indxmax - 1 (ndim of them)
    print*, 'finding corresponding eigenvectors...'
    call mat%eigensystem(ndim, psi_0, eigenvectors)
    print*, 'done'



    dt_p     = dt
    dt_saved = dt
    t        = 0.d0
    counter  = 1
    j        = 1
    previous_steps = 0
    print*, "initial number of steps: ",t_int(counter)
    pulse => select_pulse_type(pulse_shape, 'length')

    print *,  '***Begin Time Loop***'
    print *,  'time after which the pulse is off:', t_on
    ! time the time loop
    call begin_timing

    do counter = 1, tslength

      int_t_steps = t_int(counter) - previous_steps
      do
        do i = 1, int_t_steps
          !     update pulse
          E_t = pulse(t+0.5d0*dt_p)

          !     update diagonal in this case
          mat_t%diagonal(:) = mat%diagonal(:) - x(:) * E_t

          !**********************propagate the vector (psi_0) in time ************************************
          call time_dep_lanczos_banded_sym (      &
            mat_t,m_size,lancz_itnum,dt_p,factor,psi_0,lanc_threshold)
          !***********************************************************************************************
          t   = t + dt_p
        end do

        int_t_steps = int(abs(time_series(counter)-t)/dt_p)
        print*, "steps to go:", int_t_steps, "@t:", t, "with dt:", dt_p
        if(int_t_steps<1)exit

      end do

      print*,'counter',counter,'time:',time_series(counter),'t:',t!-dt_p
      previous_steps = t_int(counter)

      pes  = 0.d0
      do k = 2, ndim - 1
        i = k - 1

        projection = zdotc(m_size,eigenvectors(:,k),1,psi_0,1)
        peseven(i) = real(projection*conjg(projection))
        peseven(i) = peseven(i) / (eigenval(k+1)-eigenval(k-1))

        projection = zdotc(m_size,eigenvectors(:,k+1),1,psi_0,1)
        pesodd(i) =  real (projection*conjg(projection))

        pesodd(i) = pesodd(i)/(eigenval(k+2)-eigenval(k))

        pes(i)    = peseven(i) + pesodd(i)

      end do


      do i =1, ndim-2
        write(33, *) w(i), pes(i)
      end do


    end do
    call stop_timing
    print *,  '***End Time Loop***'

    call print_timing
    print*, 'time in a.u.', t

  end subroutine Electron_Spectra_GLNZ_length

  !***********************************************************************************************
  subroutine check_time_series(time_series,tslength)
    integer :: tslength
    real(8) :: time_series(:)

    tslength = size(time_series)

    do while (time_series(tslength)> t_intv)
      tslength = tslength - 1
    end do
    if(tslength<=0) stop "error encountered. The requested amount of cycles "//&
      "don't fit in the given time interval"
    print*, 'electron spectrum calculated for', tslength, 'intervals:'
    print*, 'at times: ',time_series(1:tslength)
  end subroutine

  !************************************************************

  subroutine calculate_eigenvalues(this,mat)
    type(tridiagonal_mat)       :: this
    type(some_vec)              :: mat
    integer                     :: m
    !*********************************************************************

    !
    !!******Find All Eigenvalues*************
    !print*,  m_found, 'Eigenvalues'

    call mat%mat_initialize(this%diagonal,this%offdiagonal           &
      ,m_size,'A',m_size,'E',1,m_size)

    call dstebz( mat%range, mat%order, mat%mat_size, mat%vl, mat%vu, &
      mat%il, mat%iu, mat%abstol, mat%diagonal, mat%offdiagonal,     &
      m, mat%nsplit, mat%w, mat%iblock, mat%isplit, mat%work,        &
      mat%iwork,mat%info )

    if (mat%info /= 0) then
      print*, 'Matrix Eigenvalue Info =', mat%info
      stop
    else
      mat%eigenval= mat%w
      !print*, mat%eigenval
    end if
    print*, 'done'

  end subroutine

  !************************************************************

  subroutine find_some_eigenvectors(mat,ndim,eigenvectors)
    type(some_vec)              :: mat
    integer                     :: i, inc
    integer                     :: ndim
    complex(8),allocatable      :: eigenvectors(:,:)

    allocate(eigenvectors(l_indx:h_indx,ndim))
    mat%m=ndim
    call dstein( mat%mat_size, mat%diagonal, mat%offdiagonal, mat%m, mat%w(1:ndim),        &
      mat%iblock, mat%isplit, mat%z, mat%ldz,mat%work, mat%iwork,          &
      mat%ifail, mat%info )
    !  !   psi time_independant of ground state, a little conversion between arrays
    !  !   psi (l_indx:h_indx)
    if(.not. allocated(psi))then
      allocate(psi(l_indx:h_indx))
    end if
    do i= 1, m_size
      psi(l_indx-1+i) = z_one * mat%z(i,1)
    end do
    do inc = 1, ndim
      do i= 1, m_size
        eigenvectors(l_indx-1+i,inc) = z_one * mat%z(i,inc)
      end do
    end do
    print*, 'done'

    !print*, psi(1), psi((m_size-1)/2), psi(m_size)
  end subroutine

  !*************************************************************

  subroutine find_spectrum_dimensions(mat, ndim)
    type(some_vec)              :: mat
    real(8)                     :: w
    integer                     :: i, k, indxmin
    integer                     :: ndim


    w = 0.d0

    do k = 2, m_size - 4

      w  = 0.25*(sum(mat%eigenval(k-1:k+2))) / omega

      if(w >= 6.d0) exit

    end do

    ndim = k + 1
    print*, "ndim of spectrum:"   , ndim

  end subroutine
  !*************************************************************

  subroutine find_normalized_quadruple_energy(w, eigenval)
    real(8)  :: w(:), eigenval(:)
    integer  :: wsz, i

    wsz = size(w)

    do i = 2, wsz + 1
      w(i-1) = 0.25d0 * sum(eigenval(i-1:i+2))/omega
    end do

  end subroutine

end module
