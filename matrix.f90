module matrix
  use parameters, only : h, pi, ii, neig, m_found, calc_matrix, &
                         potential_type 
  use grid
  use eigensystem
  use timing
  use potential
  implicit none
  private

  public  psi0 , psi,          &
    eigenval, eigenvec,        &
    !subroutines
  eig_H0, fill_matrix, print_matrix, cprint_matrix

  character(len=*), parameter :: MAT_SAVE_FILE = "matrix.dat"
  character(len=*), parameter :: ENERGIES_SAVE_FILE = "energies.dat"
  !**********************************************************************
  ! inumarators
  integer, private           :: i, j, k, m  
  !*************************************************************************    
  ! matrix diagonalization variables
  !*************************************************************************
  character         :: compz
  integer           :: ldz, info
  integer           :: il, iu      

  real*8, dimension(:), allocatable   :: eigenval
  real*8, dimension(:), allocatable   :: rhs 
  real*8, dimension(:,:), allocatable :: eigenvec
  !       !***********************************************************************
  ! ground and number m_found excited/continum states saved
  complex*16, allocatable     :: psi0(:,:)
  ! ground stat of matrix used for propagation
  complex*16, allocatable     :: psi(:) 
  

contains

  subroutine fill_matrix(this)
    type(tridiagonal_mat) :: this
    integer               :: i
    procedure(potential_func), pointer  :: potential
    !**********************************************************************************
    ! filling out the diagonals and off-diagonal elements of the Hamiltonian (H(t=0))
    !**********************************************************************************
    !         diagonals


    if(.not. (allocated(this%diagonal)))then
      allocate(this%diagonal(1:n),this%offdiagonal(1:n-1))
    end if

    potential => select_potential_type(potential_type)
    

    this%diagonal(:)= (1.d0 / h**2) + potential(x(:))
        !- (1.d0 / sqrt(x(i) * x(i) + epsilion * &
        !  epsilion))
    do i=1,n-1
      this%offdiagonal(i)=-0.5d0/h**2
    end do
    

    this%mat_size = n

  end subroutine          

  !! Calculates or retrives the desired eigenvalues and eigenvectors of the
  !! given tridiagonal_mat. If retrival is desired it must be specified in
  !! the config file, after previously calculating it. Only a single matrix
  !! can be calculated, and it does not check if the potential was changed.
  !!
  !! The matrix is optionally refilled with the default initial potential 
  !! if the no_fill_matrix parameter is NOT passed to this subroutine. 
  subroutine eig_H0 (this, no_fill_matrix)                                
    !    Find some or all eigenvalues and eigenvectors of the time-independant Hamiltonian
    !**************************************************************************************
    type(tridiagonal_mat) :: this
    type(all_vec)         :: mat_all
    type(some_vec)        :: mat_some
    integer               :: ierr, io
    logical, optional     :: no_fill_matrix
    logical               :: file_exists

    print *, '# Eigenvectors found = ', m_found
    print *, '# excited Eigenvectors to be saved/plotted= ', neig
    print *,'calc_matrix?',calc_matrix


    if(.not. allocated(psi0)) then
      allocate(psi0(m_size,m_found),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, psi0 allocation failed!'
      endif
    endif 

    if(.not. allocated(psi)) then
      allocate(psi(m_size),stat = ierr)
      if ( ierr /= 0 ) then
        write(*,*) 'Oops, psi allocation failed!'
      endif      
    endif
    !**********if calc_matrix logical key is true start calculation from scratch**************      
    if (calc_matrix) then
      !*****************************************************************************************            
      !**********************************************************************************
      ! filling out the diagonals and off-diagonal elements of the Hamiltonian (H(t=0))
      !**********************************************************************************
      if (.not. present(no_fill_matrix)) then
        print*, "Refilling the matrix"
        call fill_matrix(this)
      endif

      !**********************************************************	
      !     Diagonalizing the matrix
      !**********************************************************    
      print *,  '***Begin call to Eigensystem Solver***'

      call begin_timing
      if(m_found == m_size)then
        !     !     find all matrix eigenvectors
        print*, 'All matrix eigenvectors and eigenvales are calculated ...'
        compz='I'
        ldz= max(1,m_size)
        ! 	  
        ! 	  !!Find all eigenvalues and eigenvectors     
        call mat_all%mat_initialize(this%diagonal,this%offdiagonal,ldz,compz)
        call mat_all%eigensystem_solve      
        ! 	  
        !write eigenvalues to file

        allocate(eigenvec(m_size, m_size), eigenval(m_size))
        eigenval = mat_all%eigenval
        eigenvec = mat_all%eigenvec

      else
        !*******************************************************************************
        !find some matrix eigenvectors, but all eigenvalues
        !parameters for initialize : d,e,ldz,dir1,m_find,dir2,il,iu
        ! options : dir1 = 'A' for all eigenvalues, il and iu not necessary
        !           dir1 = 'I' for some --> il = first, iu = last
        !           dir2 = 'E'("Entire matrix"); 'B': ("By Block")
        !*******************************************************************************
        print*, 'Some matrix eigenvectors, all eigenvalues are calculated...'
        call mat_some%mat_initialize(this%diagonal,this%offdiagonal&
          ,m_size,'A',m_found,'E',1,m_found)
        call mat_some%eigensystem_solve 
        allocate(eigenvec(m_size,m_found), eigenval(m_size),stat = ierr)

        if ( ierr /= 0 ) then
          write(*,*) 'Oops, allocation of eigenvec in matrix.f90 failed!'
        endif              
        eigenvec = mat_some%eigenvec
        eigenval = mat_some%eigenval
      end if    

      call stop_timing
      call printEigenValues

      ! INIT PSI and PSI0
      do i = 1, m_size
        psi(i) = cmplx(1.d0, 0.d0) * eigenvec(i, 1)
      end do


      do i = 1, m_found
        do j = 1, m_size
          psi0(j, i) = cmplx(1.d0, 0.d0) * eigenvec(j, i)
        end do
      end do


      print *,  '***End call to Eigensystem Solver***'
      call print_timing

      call saveMatrix 
    else
      inquire(file='matrix.dat', exist=file_exists)
      if(file_exists)then
        print*, "Reading existing eigensystem data"
        call loadMatrix
        call printEigenValues
        print*, "Finished reading existing eigensystem data"
      else
         print*, "file with saved eigenvectors does not exist"
         print*, "put T for recalc_matrix in input"
         stop             
      endif  
    end if

    call print_psi

  end subroutine eig_H0
  !************************************************************************************

  !-------------------------------------------------------------------------
  ! DESCRIPTION:
  !> Saves the psi0 matrix, from which all the information can be restored.
  !> Saves some essential parameters needed to ensure the sanity of the program.
  !-------------------------------------------------------------------------
  subroutine saveMatrix
    open(unit=17, file=MAT_SAVE_FILE, form="unformatted", access="sequential")
    write(17) m_size
    write(17) m_found
    write(17) shape(psi0)
    write(17) psi0
    close(17)

    open(unit=3, file=ENERGIES_SAVE_FILE,form="unformatted",access="sequential")
    write(3) eigenval
    close(3) 
  end subroutine saveMatrix

  !-------------------------------------------------------------------------
  ! DESCRIPTION:
  !> loads the matrix from the MAT_SAVE_FILE, while performing various
  !> sanity checks.
  !> Then restores the values derived from psi0.
  !-------------------------------------------------------------------------
  subroutine loadMatrix
    integer :: mat_size, l_found, mat_shape(2), cur_shape(2)

    open(unit=17, file=MAT_SAVE_FILE, form="unformatted", access="sequential")

    ! Various sanity checks before loading to make sure the saved matrix is
    ! still valid
    read(17) mat_size
    if (mat_size /= m_size) then
      stop "Saved Matrix is Of Wrong Size"
    end if
    read(17) l_found
    if (l_found /= m_found) then
      stop "Saved matrix does not have requested number of eigenvectors"
    end if
    read(17) mat_shape
    cur_shape = shape(psi0)
    if (mat_shape(1) /= cur_shape(1) .OR. mat_shape(2) /= mat_shape(2)) then
      stop "Incorrect Matrix Shape"
    end if

    read(17) psi0

    ! reload other parameters from loaded matrix
    psi(:) = psi0(:,1)

    allocate(eigenvec(m_size, m_found), eigenval(m_size))
    eigenvec(:,:) = real(psi0(:,:))

    ! Reload the energies save file
    open(unit=3,file=ENERGIES_SAVE_FILE,form="unformatted",access="sequential")
    read(3) eigenval

    close(3)
    close(17)
  end subroutine loadMatrix

  !-------------------------------------------------------------------------
  ! DESCRIPTION:
  !> Prints out the highest and lowest eigenvectors
  !-------------------------------------------------------------------------
  subroutine printEigenValues
    integer bound
    print*, m_size, 'eigenvalues', size(eigenval)

    bound = min(9, m_size)

    print*, 'lowest eigenvalues: ', eigenval(1:bound)
    if (m_size > bound) then
      print*, 'highest eigenvalues:', eigenval(m_size - 5: m_size)
    end if
    print*, m_found, 'eigenvectors', size(eigenvec, 1), size(eigenvec, 2)

  end subroutine printEigenValues


  subroutine print_psi
    !write(*,*) "lowest values in psi"
    !write(*,*) psi(1:10)
    !write(*,*) "highest values in psi"
    !write(*,*) psi(size(psi) - 10: size(psi))

  end subroutine print_psi

  subroutine print_matrix(matrix)
    integer :: i
    real(8), dimension(:,:), intent(in) :: matrix
    character(len=128) :: fmt
    integer  :: mat_size_row, mat_size_clm

    !get the size of rows
    mat_size_row = size(matrix,1)
    !print*, mat_size_row
    !sieze the size of the columns:
    mat_size_clm = size(matrix,2)
    !print*, mat_size_clm
    write(fmt,'("(",I0,"Es15.7)")') mat_size_clm
    !print*, fmt
    do i = 1, mat_size_row
      write(*,fmt) matrix(i,:)
    end do   

  end subroutine print_matrix

  subroutine cprint_matrix(matrix)
    integer :: i
    complex(8), dimension(:,:), intent(in) :: matrix
    character(len=128) :: fmt
    integer  :: mat_size_row, mat_size_clm

    !get the size of rows
    mat_size_row = size(matrix,1)
    !print*, mat_size_row
    !sieze the size of the columns:
    mat_size_clm = size(matrix,2)
    !print*, mat_size_clm
    ! write(fmt,'(I0,"(Es15.7),",a1,",",I0,"(Es15.7)")') &
    !   mat_size_clm,"i",mat_size_clm
    ! print*, fmt

    do i = 1, mat_size_row
      write(*,'(100G15.5)') (matrix(i,j), j=1,mat_size_clm)
    end do   

    !    1002 FORMAT(F0.0,"+i",F0.0)
  end subroutine cprint_matrix
end module
