program main
  use parameters
  use grid
  use matrix
  use eigensystem
  use crank_nicolson
  use sb_crank_nicolson
  use lanczos
  use lanczos_sym_banded
  use split_operator
  use chebychev
  use electron_spectra
  implicit none

  !***********************************
  type(tridiagonal_mat) :: mat
  !***********************************

  !reading inputs
  call parameter_reader

  !setting up the spatial grid
  !l --> length of one side of the box (a.u.)
  !h --> grid size (a.u.)
  !call spatial_grid_fill_sym_z(l, h)
   call spatial_grid_fill(l,h,0.d0) 
  
  print*, "potential type:", trim(potential_type) 
  print*, "method-name:", trim(method_name)
  !************************************************************
  ! Crank-Nicolson combinations
  !************************************************************
  !selet different propagation methods
  select case(trim(method_name))

  ! crank-nicolson in length gauge
case('cn_l')

  call fill_matrix(mat)
  call eig_H0(mat)
  call CN_length(trim(pulse_name))

  ! crank-nicolson in velocity gauge
case('cn_v')

  call fill_matrix(mat)
  call eig_H0(mat)
  call CN_velocity(trim(pulse_name))

  ! crank-nicolson in length + split operator H0 and V(x) (2nd order)
case('cn_l_split')

  call fill_matrix(mat)
  call eig_H0(mat)
  call CN_length_split(trim(pulse_name))

  ! crank-nicolson in length gauge for symmetric banded matrices
case('sb_cn_l')

  call sb_cn_l(trim(pulse_name), m_size, x, h, band_num_sym_mat, soft_core_eps)

  !**************************************************************
  !   Lanczos methods
  !**************************************************************
  ! lanczos in length gauge (tridiagional matrices only)
case('lanczos')

  call fill_matrix(mat)
  call eig_H0(mat)
  call lanczos_exp(trim(pulse_name))

  ! lanczos in length gauge + splitting (tridiagonal matrix only)
case('lanczos_split_exp')

  call fill_matrix(mat)
  call eig_H0(mat)
  call lanczos_split_exp(trim(pulse_name))

  ! lanczos in length gauge with symmetric banded matrices
case('sb_lanczos')
  call sb_lanczos_exp(trim(pulse_name),band_num_sym_mat,neig)

  !**************************************************************
  ! involves no splitting, very expensive full expontiation
  ! at every time step
  !*************************************************************
case('full_exp_propagator')

  m_found = m_size
  call eig_H0(mat)
  call full_exp_propagator(pulse_name)
  !**************************************************************
  ! Splitting and exponentiation
  !**************************************************************
  ! splitting H_0 and V(x) where V(x) is diagonal
case('split_propagator')

  m_found = m_size
  call eig_H0(mat)
  call split_propagator(trim(pulse_name))

  ! splitting H_0 and V(x) to the 4th order splitting
case('split_propagator_4')

  m_found = m_size
  call eig_H0(mat)
  call split_propagator_4(trim(pulse_name))

  ! this method splits the matrix into smaller 2x2 matrices
  ! then does the regular splitting
case('even_odd_split_propagator')
  call fill_matrix(mat)
  m_found = m_size
  call eig_H0(mat)
  print*, size(mat%diagonal)
  call even_oddsplit_propagator(mat%diagonal,mat%offdiagonal)
  !*************************************************************
  ! chebychev propagator length gauge
  !*************************************************************
case('cheby')
  call prop_chebychev(trim(pulse_name))

case('spectrum_glncz','spectrum_lncz','spectrum_cnl', &
    'spectrum_sb_cnl','spectrum_cnv')
  !******************** Electron spectra **************************
  if  (trim(method_name)=='spectrum_cnv')then
    call  Electron_Spectra_CN_velocity(trim(pulse_name))
  elseif  (trim(method_name)=='spectrum_cnl')then
    call Electron_Spectra_CN_length(trim(pulse_name))
  elseif (trim(method_name)=='spectrum_sb_cnl')then
    call Electron_Spectra_sb_cn_l  (trim(pulse_name),bandnum=band_num_sym_mat)
  elseif (trim(method_name)=='spectrum_lncz')then
    call Electron_Spectra_LNZ_length(trim(pulse_name))
  elseif (trim(method_name)=='spectrum_glncz')then
    call  Electron_Spectra_GLNZ_length(trim(pulse_name),bandnum=band_num_sym_mat)
  endif


case default
  write(*,*) "name is not in the list!"
  write(*,*) "make sure you have added the method_name to the above list."


end select

end program main
