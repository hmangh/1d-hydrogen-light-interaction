module lanczos
  use parameters
  use grid  , only: x, m_size, l_indx, h_indx
  use matrix, only: eigenvec, eigenval,psi0 , psi, fill_matrix
  use eigensystem
  use generalUtility
  use functions2
  use matmult
  use timing
  implicit none
  private
  public lanczos_exp,lanczos_split_exp,&
    lanczos_split_exp4

  !-----------------------------------------------------------------
  !>    type(tridiagonal_mat)               :: tdmat_alpha
  !>    this tridiagonal matrix is used to find the q_kth lanczos
  !>    vector associated to alpha_k diagonal element of the
  !>    lanczos matrix
  !-----------------------------------------------------------------
  type(tridiagonal_mat)               :: tdmat_alpha
  integer                             :: last_vec
  real(8)                             :: max_overlap
  real(8)                             :: halfdt
contains
  !>*****************************************************************
  !> based on known parameters h and m_size, fills the tridiagonal
  !> matrix. This particular case is filled with the diagonal
  !> and off-diagonal elements as described by Eberly for the length
  !> gauge of a 1dimensional particle in a box.
  !>************************************************************************
  subroutine initialize_tdmatrix(tdmat)
    type(tridiagonal_mat)      :: tdmat

    tdmat%mat_size = m_size
    call fill_matrix(tdmat)
  end subroutine initialize_tdmatrix
  !************************************************************************
  !> Normalizing a complex vec
  !************************************************************************
  function normz(vec) result(norm)
    complex(8)                            :: zdotc
    complex(8),intent(in), dimension(:)   :: vec
    real   (8)                            :: norm

    norm = real(sqrt(dot_product(vec,vec)))!real(sqrt(zdotc(size(vec),vec,1,vec,1)))

  end function normz

  !*************************************************************************
  !>   Alpha as normaly described in Lanczos algorithms as the diagonal element
  !>   of Lanczos tridiagonal.
  !*************************************************************************
  function find_alpha(tdmat, vec, ndim) result(alpha)
    integer, intent(in)                     :: ndim
    type(tridiagonal_mat)                   :: tdmat
    complex(8), dimension(ndim), intent(in) :: vec
    complex(8), dimension(ndim)             :: rh_vec
    complex(8), external                    :: zdotc
    complex(8)                              :: zalpha
    real(8)                                 :: alpha

    call mat_vec_mul(tdmat,vec,rh_vec)

    !        alpha(k) = q(:,k)* H_0 * q(:,k)
    !print*, ndim
    !print*, vec(1000), size(vec)
    !print*, rh_vec(1000), size(rh_vec)
    !zalpha= dot_product(vec,rh_vec)!zdotc(ndim,vec,1,rh_vec,1)
    !print*, zalpha
    alpha = real(dot_product(vec,rh_vec))!real(zdotc(ndim,vec,1,rh_vec,1))


  end function find_alpha
  !************************************************************************
  !> Diagonalizer for lanczos tridiagonal
  !************************************************************************
  subroutine lanczos_diagonalizer(alpha,beta,eigvec,eigval,ndim)

    type(all_vec)                               :: mat_lanczos
    real(8),dimension(ndim,ndim),intent(out)    :: eigvec
    real(8),dimension(ndim)     ,intent(out)    :: eigval
    real(8),dimension(ndim)     ,intent( in)    :: alpha
    real(8),dimension(ndim-1)   ,intent( in)    :: beta
    integer                                     :: ndim

    call mat_lanczos%mat_initialize(alpha,beta)
    ! !      print*,mat_all%mat_size
    call mat_lanczos%eigensystem_solve

    eigvec = mat_lanczos%eigenvec
    eigval = mat_lanczos%eigenval

  end subroutine lanczos_diagonalizer
  !************************************************************************
  !>  sesqv is the final vector in the lanczos basis; dimensioned k, the
  !>  iteration number of the lanczos.
  !>  This routine calclates sesqv and the the sqv(k) vector
  !>  Here s is the (k,k) dimensioned modal matrix of lanczos
  !>       q is the (ndim,k) matrix of lanczos vectors
  !**************************************************************************
  subroutine proj_exp_q(q,v,eigvec,eigval,ndim,k,factor,sqv,sesqv)
    integer   , intent(in)                  :: ndim
    integer   , intent(in)                  :: k
    complex(8), intent(in)                  :: factor
    complex(8), intent(in) ,dimension(:,:)  :: q
    complex(8), intent(in) ,dimension(ndim) :: v
    complex(8)             ,dimension(k)    :: qv
    complex(8), intent(out),dimension(k)    :: sqv
    complex(8)             ,dimension(k)    :: esqv
    complex(8), intent(out),dimension(k)    :: sesqv
    real   (8), intent(in),dimension(:,:)   :: eigvec
    real   (8), intent(in),dimension(:)     :: eigval

    !!             q^T_(kxn) * v(nx1) = qv (k,1)

    call tmat_vec_mul(q,v,qv,ndim,k)

    !!             s^T_(kxk) * qv_(kx1) = sqv_(kx1)
    call tmat_vec_mul(eigvec,qv,sqv,k,k)

    !!            exp(i dt eigenval)* I_(kxk) * sqv_(kx1)
    esqv(1:k) = exp(factor*eigval(1:k))*sqv(1:k)

    !!            S_(kxk) * esqv(kx1) = sesqv_(kx1)
    call mat_vec_mul(eigvec, esqv, sesqv, k,k)

  end subroutine proj_exp_q
  !**************************************************************************
  !>    Lanczos iteration scheme for a tridiagonal matrix
  !>    This version tries to reduce the dt in the exponential if convergence
  !>    is not reached at the end of the given number of iterations.
  !**************************************************************************
  subroutine time_dep_lanczos_tridiag2(tdmat_t,itnum,dt_p,factor,v,threshold)
    integer                             :: i,j,k,m
    integer, intent(in)                 :: itnum
    integer, parameter                  :: sch_const = 1
    integer                             :: mat_size
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    real(8), parameter                  :: sch_tol = 1.d-10
    real(8),intent(in)                  :: threshold
    real(8)                             :: convfac
    real(8)                             :: convfac_old
    real(8)                             :: dt_p
    real(8)                             :: alpha(itnum)
    real(8)                             :: beta (itnum)
    real(8)                             :: eigvec_k(itnum,itnum)
    real(8)                             :: eigval_k(itnum)
    real(8)                             :: eigvec_old(itnum,itnum)
    real(8)                             :: eigval_old(itnum)
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    type(tridiagonal_mat),intent(in)    :: tdmat_t
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    complex(8),dimension (tdmat_t%mat_size,itnum)  :: q
    complex(8),dimension (   itnum)                :: qv
    complex(8),dimension (   itnum)                :: sqv
    complex(8),dimension (   itnum)                :: sqv_old
    complex(8),dimension (   itnum)                :: esqv
    complex(8),dimension (   itnum)                :: sesqv
    complex(8),dimension (   itnum)                :: sesqv_old
    complex(8),dimension (tdmat_t%mat_size)        :: qsesqv
    complex(8),dimension (tdmat_t%mat_size)        :: rh_vec
    complex(8),dimension (:)                       :: v
    complex(8)                                     :: factor
    complex(8)                                     :: newFactor
    complex*16                                     :: zdotc
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logical                                        :: convg

    !******************
    !   diagnostics
    !******************
    !max_overlap = 0.d0
    !last_vec = itnum
    !******************

    alpha    = 0.d0
    beta     = 0.d0
    eigval_k = 0.d0
    eigval_old = 0.d0
    eigvec_k = 0.d0
    eigvec_old = 0.d0
    qv        = z_zero
    q        = z_zero
    sqv        = z_zero
    sqv_old    = z_zero
    esqv        = z_zero
    sesqv        = z_zero
    sesqv_old    = z_zero
    convg     = .false.
    mat_size  = tdmat_t%mat_size

    !
    !! First iteration*******************************************

    q(:,1) = v/normz(v)

    !alpha(1) = q(:,1)* H_0 * q(:,1)
    !print*, q(1000,1)
    alpha(1) = find_alpha(tdmat_t,q(:,1),m_size)
    !

    !Performing projection on incoming vector**********************
    sqv_old  (1) = dot_product(q(:,1),v)!zdotc(mat_size,q(:,1),1,v,1)
    sesqv_old(1) = exp(factor*alpha(1))* sqv_old(1)

    !
    ! Finding the q of second iteration*****************************
    !

    tdmat_alpha%diagonal = tdmat_t%diagonal - alpha(1)

    call mat_vec_mul(tdmat_alpha,q(:,1),rh_vec)

    beta(1) = real(sqrt(dot_product(rh_vec(:),rh_vec(:))))!zdotc(mat_size,rh_vec(:),1,rh_vec(:),1)))

    q(:,2)=rh_vec(:)/beta(1)

    ! print*,"just gram-schmidt the 1st to 2nd (they could be too close to each other)"
    call zschmab(q(1,1),q(1,2),sch_tol,mat_size,sch_const,1,m)
    !
    !! All other lanczos iterations***********************************
    !

    do k= 2, itnum

      !!****Gram_Schmidt orthogonalizing of qs
      !   if (present(GS))then
      !       if (GS ==.true.)the
      !          call zschmab(q(1,1),q(1,k),sch_tol,mat_size,k-1,1,m)
      !       end if
      !   end if

      ! Find alpha(k):
      alpha(k) = find_alpha(tdmat_t,q(:,k),mat_size)

      !Finding the eigen values/vectors of the Lanczos
      !tridiagonal matrix
      call lanczos_diagonalizer(alpha(1:k),beta(1:k-1)&
        ,eigvec_k(1:k,1:k),eigval_k(1:k),k)

      ! Finding the projection of Lanczos tridiagonal
      ! matrix on the wave
      call proj_exp_q(q(:,1:k),v,eigvec_k(1:k,1:k),eigval_k(1:k), &
        mat_size,k,factor,sqv(1:k),sesqv(1:k))

      convfac = real(sqrt(dot_product(sesqv-sesqv_old,sesqv-sesqv_old)))!zdotc(itnum,sesqv-sesqv_old,1,sesqv-sesqv_old,1)))
      
      if(convfac<=threshold)then
        convg = .true.
        call mat_vec_mul(q,sesqv,qsesqv,mat_size,itnum)
        v = qsesqv
        exit
      elseif(k<itnum)then
        sesqv_old            = sesqv
        sqv_old              = sqv
        eigvec_old           = eigvec_k
        eigval_old           = eigval_k
        tdmat_alpha%diagonal = tdmat_t%diagonal - alpha(k)
        convfac_old          = convfac

        call mat_vec_mul(tdmat_alpha,q(:,k),rh_vec)

        rh_vec(:)   = rh_vec(:) - beta(k-1)* q(:,k-1)

        beta(k)  = real(sqrt(dot_product(rh_vec(:),rh_vec(:))))!zdotc(mat_size,rh_vec(:),1,rh_vec(:),1)))

        q(:,k+1) = rh_vec(:)/beta(k)

      end if

    end do ! lanczos loop

    newFactor = factor/2
    ! !       reseting k to the last unconverged iteration
    i = 1 ; j =2
    k = itnum

    ! !        we only need to try the last two iterations of lanczos with the new time.
    do while (.not. convg)

      esqv = z_zero

      esqv(1:k-1) = exp(newFactor*eigval_old(1:k-1))*sqv_old(1:k-1)

      call mat_vec_mul(eigvec_old,esqv(1:k-1),sesqv_old,k-1,k-1)

      esqv = z_zero

      esqv(1:k) = exp(newFactor*eigval_k(1:k))*sqv(1:k)

      call mat_vec_mul(eigvec_k,esqv(1:k),sesqv,k,k)

      convfac = real(sqrt(dot_product(sesqv-sesqv_old,sesqv-sesqv_old))) 
      !                print*, convfac

      if(convfac <= threshold) then
        ! ! projection to the original basis
        call mat_vec_mul(q,sesqv,qsesqv,mat_size,itnum)
        v = qsesqv
        !print*,"divided by:",j
        dt_p = dt_p/j
        factor = - ii * dt_p
        convg = .true.

      else

        if(i==20)stop 'reducing time-step size is not helping'
        newFactor = newFactor/2

        i = i +1
        j = 2 **i

      end if

    end do
  end subroutine

  !*************************************************************************
  !   Lanczos driver, lancz_itnum and lanc_threshold are parameters read
  !   from input file (look at parameters.f90) The option GS is the
  !   Gram_Schmidt operation on the lanczos vectors (q here.)
  !*************************************************************************
  subroutine lanczos_exp(pulse_shape)
    use list
    integer                             :: i,j,k
    !length of time set
    integer                             :: tlength
    !length of probability set
    integer                             :: plength
    !iteration for which data is recorded
    integer                             :: rec_it
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !time, incremented from 0 by dt upto t_intv
    real(8)                             :: t
    !lanczo specific dt (dt_prime) which can decrease below dt
    real(8)                             :: dt_p, dt_saved
    !electric field that is evolving with time
    real(8)                             :: E_t
    !set containing the time increment with dt_p
    real(8), allocatable,dimension(:)   :: tdata
    !set containing the probability projections
    real(8), allocatable,dimension(:,:) :: pdata
    !probability of projection of the propagated wave onto
    !the ground (0) and neig number of excited states
    !, temporary holder, the final agregate data is in pdata
    real(8), allocatable                :: prob(:)
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !initial tridiagonal matrix of the system
    type(tridiagonal_mat)               :: tdmat
    !time-evolved tridiagonal matrix of the system
    type(tridiagonal_mat)               :: tdmat_t
    !determines the properties of the pulse
    procedure(pulse_at_t_func), pointer :: pulse
    !linked list that records time, the start of the list
    type(cell),pointer                  :: inital_time
    !linked list that records time, the rest of the list
    type(cell),pointer                  :: time
    !linked list that records probability array, the start of the list
    type(cell_array),pointer            :: i_p
    !linked list that records probability array, the rest of the list
    type(cell_array),pointer            :: p
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !>projections of propagated wave on the m_found states
    complex(8), allocatable             :: proj (:)
    !i*dt of the exp (i*dt*D)
    complex(8)                          :: factor
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    character(len=*)                    :: pulse_shape
    character(len=64)                   :: fmt
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    print*, '("***Lanczos with maximum iteration of",x,I3,x"***")',lancz_itnum
    print*, 'lanczos tolerance:',lanc_threshold
    open(unit=33,file=datafilename)

    write(fmt,'("(f10.2,",I0,"es15.3)")') m_found
    !print*, fmt
    
    !open(unit=34, file="iteration_vec.out")

    allocate(prob(1:m_found), proj(1:m_found))
    call initialize_tdmatrix(tdmat)
    call initialize_tdmatrix(tdmat_t)
    call initialize_tdmatrix(tdmat_alpha)

    !!factor is double idt because idt already has been halved...
    factor = -2.d0*idt

    !Start of the time loop
    call init_fill_list(inital_time, time, 0.d0)
    prob = 0.d0
    prob(1) = 1.d0
    call init_fill_array_list(i_p, p, prob)

    pulse => select_pulse_type(pulse_shape, 'length')
    dt_p = dt ; t = 0.d0; dt_saved = dt_p; j=0; rec_it=1

    print *,  '***Begin Time Loop***'

    call begin_timing
    do while (t <= t_intv)
      ! update pulse
      E_t = pulse(t + 0.5d0 * dt_p)

      ! Length-Gauge
      tdmat_t%diagonal(:)  = tdmat%diagonal(:)  - x(:) * E_t

      call time_dep_lanczos_tridiag2(tdmat_t,lancz_itnum,dt_p,factor,&
        psi,lanc_threshold)

      ! **************diagnostics*****************************************
      !  if (dt_saved > dt_p )then
      !   print*, "dt=", dt_p, "@ t=", t,"fac=",factor
      rec_it = int(dt/dt_p)
      !   end if
      ! ******************************************************************
      !  print*, dt_p
      j = j + 1

      if (j == rec_it) then


        do i= 1, neig

          proj(i)  =  sum(psi(:)*psi0(:,i))
          prob(i)  =  real(proj(i)*conjg(proj(i)))

        end do

        !              warning this part is storing the probability arrays in memory
        !              the linked list approach is taken here

        call fill_list(time, t)
        call fill_array_list(p,prob)

        j = 0

      end if

      t = t + dt_p
      !            !****diagnostics***************
      !               dt_saved = min(dt_p,dt_saved)
      !write(34,*) t,last_vec, max_overlap
      !            !******************************
      !               dt_p = dt; factor= -ii * dt

    end do ! end of the time loop

    call stop_timing()
    print *,  '***End Time Loop***'

    print*,'last (smallest) time step used:',dt_p
    print*,'last factor:',factor

    call print_timing

    call end_list(time)
    call end_array_list(p)

    call read_list(inital_time, time, tdata, tlength)
    call read_array_list(i_p, p, pdata, plength)

    !first memebr of the list is an artificial zero to start the list
    ! print*, m_found
    ! print*, plength, tlength
    !print*, shape(pdata)
    !print*, shape(tdata)
    ! print*, pdata(:,2), pdata(:,plength)
    ! print*, tdata(2), tdata(plength)
    !do i = 2,!plength5 !plength5 !plength5 !plength5 !plength 
    !  write(*,fmt)tdata(i), pdata(:,i)
    !end do
    do i = 2, plength
      !do j = 1, neig+1
      write(33,fmt)tdata(i),pdata(:,i)
      !end do
    end do
    close(33)
    !          do i = 2, tlength
    !             write(34,*)tdata(i)
    !          end do


  end subroutine lanczos_exp

  !****************************************************************************
  !>   Lanczos driver, lancz_itnum and lanc_threshold are parameters read
  !>   from input file (look at parameters.f90) The option GS is the
  !>   Gram_Schmidt operation on the lanczos vectors (q here.)
  !*************************************************************************
  subroutine lanczos_split_exp(pulse_shape)
    use list
    integer                             :: i,k
    !>length of time set
    integer                             :: tlength
    !>length of probability set
    integer                             :: plength
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !>time, incremented from 0 by dt upto t_intv
    real(8)                             :: t
    !>lanczo specific dt (dt_prime) which can decrease below dt
    real(8)                             :: dt_p
    !>electric field that is evolving with time
    real(8)                             :: E_t
    !>set containing the time increment with dt_p
    real(8), allocatable,dimension(:)   :: tdata
    !>set containing the probability projections
    real(8), allocatable,dimension(:,:) :: pdata
    !>probability of projection of the propagated wave onto
    !>the ground (0) and neig number of excited states
    !>, temporary holder, the final agregate data is in pdata
    real(8), allocatable                :: prob(:)
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !>initial tridiagonal matrix of the system
    type(tridiagonal_mat)               :: tdmat
    !>determines the properties of the pulse
    procedure(pulse_at_t_func), pointer :: pulse
    !>linked list that records time, the start of the list
    type(cell),pointer                  :: inital_time
    !>linked list that records time, the rest of the list
    type(cell),pointer                  :: time
    !>linked list that records probability array, the start of the list
    type(cell_array),pointer            :: i_p
    !>linked list that records probability array, the rest of the list
    type(cell_array),pointer            :: p
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    complex(8)                          :: V_t (1:m_size)

    !>projections of propagated wave on the m_found states
    complex(8), allocatable             :: proj (:)
    !>i*dt of the exp (i*dt*D)
    complex(8)                          :: factor
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    character(len=*)                    :: pulse_shape
    character(20)                       :: fmt
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    print '("***Lanczos with maximum iteration of",x,I3,x"***")',lancz_itnum
    print *, 'lanczos tolerance:',lanc_threshold

    open(unit=33,file=datafilename)
    allocate(prob(neig), proj(neig))
    call initialize_tdmatrix(tdmat)
    call initialize_tdmatrix(tdmat_alpha)

    ! initializations*****************************
    dt_p = dt
    t = 0.d0
    halfdt = 0.5d0 * dt
    factor= -ii*dt_p
    call init_fill_list(inital_time, time, 0.d0)
    prob = 0.d0
    prob(1) = 1.d0

    call init_fill_array_list(i_p, p, prob)
    !**********************************************
    print *,  '***Begin Time Loop***'
    call begin_timing
    pulse => select_pulse_type(pulse_shape, 'length')
    do while (t<= t_intv)

      call fill_list(time, t)

      !!changed
      E_t = pulse(t + halfdt)
      V_t = exp(idt * x(:) * E_t)
      psi(:)  = V_t(:) * psi(:)

      ! Lanczos
      do i = 1, int(dt/dt_p)
        call time_dep_lanczos_tridiag2(tdmat,lancz_itnum,dt_p,factor,psi,lanc_threshold)
      end do

      psi(:)  = V_t(:) * psi(:)

      t = t + dt
      !       E_t = pulse(t + 0.5d0*dt_p)
      !       ! Length-Gauge
      !       psi_0(:)     = exp(0.5d0*ii*dt_p * x(:) * E_t) * psi_0(:)
      !
      !       ! Lanczos
      !
      !       call time_dep_lanczos_tridiag2(tdmat,lancz_itnum,dt_p,factor,psi_0,lanc_threshold)
      !
      !       psi_0(:)     = exp(0.5d0*ii*dt_p * x(:) * E_t) * psi_0(:)

      !      t = t + dt_p

      do i= 1, neig
        proj(i)  =  sum(psi(:)*psi0(:,i))
        prob(i)  =  real(proj(i)*conjg(proj(i)))
      end do
      ! This part is storing the probability arrays in memory
      ! the linked list approach is taken here
      call fill_array_list(p,prob)


      !dt_p = dt ; factor = -ii*dt_p

    end do ! end of the time loop
    call stop_timing
    print *,  '***End Time Loop***'

    call print_timing
    print*,'last (smallest) time step used:',dt_p
    print*,'last factor:',factor

    call end_list(time)
    call end_array_list(p)

    call read_list(inital_time, time, tdata, tlength)
    call read_array_list(i_p, p, pdata, plength)

    !first memebr of the list is an artificial zero to start the list

    write(fmt,'("(f10.2,",I0,"es15.3)")')neig

    do i = 2, plength

      write(33,fmt)tdata(i),pdata(:,i)

    end do


  end subroutine lanczos_split_exp

  !*************************************************************************
  !>   Lanczos driver + spliting (4th order), lancz_itnum and lanc_threshold are parameters read
  !>   from input file (look at parameters.f90) The option GS is the
  !>   Gram_Schmidt operation on the lanczos vectors (q here.)
  !*************************************************************************
  subroutine lanczos_split_exp4(pulse_shape)
    use list
    integer                             :: i,j,k
    !length of time set
    integer                             :: tlength
    !length of probability set
    integer                             :: plength
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !time, incremented from 0 by dt upto t_intv
    real(8)                             :: t
    !lanczo specific dt (dt_prime) which can decrease below dt
    real(8)                             :: dt_p
    !electric field that is evolving with time
    real(8)                             :: E_t
    !set containing the time increment with dt_p
    real(8), allocatable,dimension(:)   :: tdata
    !set containing the probability projections
    real(8), allocatable,dimension(:,:) :: pdata
    !probability of projection of the propagated wave onto
    !the ground (0) and neig number of excited states
    !, temporary holder, the final agregate data is in pdata
    real(8), dimension(1:m_found)       :: prob
    real(8), dimension(5)               :: t_var
    !>coefficient of the fourth order
    real(8), parameter                  :: s = 1.d0/(4.d0 - 4.d0**(1.d0/3.d0))
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !initial tridiagonal matrix of the system
    type(tridiagonal_mat)               :: tdmat
    !determines the properties of the pulse
    procedure(pulse_at_t_func), pointer :: pulse
    !linked list that records time, the start of the list
    type(cell),pointer                  :: inital_time
    !linked list that records time, the rest of the list
    type(cell),pointer                  :: time
    !linked list that records probability array, the start of the list
    type(cell_array),pointer            :: i_p
    !linked list that records probability array, the rest of the list
    type(cell_array),pointer            :: p
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !>the ground state eigenvector of tdmat, that is evolved with time
    complex(8)                          :: psi_0(1:m_size)
    !>the m_found number of wavefunctions that are saved
    complex(8)                          :: vec0 (1:m_size,1:neig)
    !>projections of propagated wave on the m_found states
    complex(8)                          :: proj (1:neig)
    !i*dt of the exp (i*dt*D)
    complex(8)                          :: factor
    !
    complex(8)                          :: V_t(1:m_size), V_t_m(1:m_size)
    !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    character(len=*)                    :: pulse_shape
    character(20)                       :: fmt
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    print '("***Lanczos with maximum iteration of",x,I3,x"***")',lancz_itnum
    print *, 'lanczos tolerance:',lanc_threshold

    open(unit=33,file=datafilename)

    print*, 's: ', s
    t_var = [s * 0.5d0 ,  s * 1.5d0, 0.5d0, 1.d0-1.5d0 * s, 1.d0 - s]

    do i = 1, 5
      print*, "t_var(",i,"):",t_var(i)
    end do

    call initialize_tdmatrix(tdmat)
    call initialize_tdmatrix(tdmat_alpha)

    ! Setting up the projection of propagated wave onto the ground
    ! state and a few excited states, a conversion from psi(-n:n)
    ! to vec(1:m_size) is taking place, not to worry!
    do i= 1, m_size
      psi_0(i) = psi(l_indx-1+i)
      do k=1,neig
        vec0(i,k) = psi0(l_indx-1+i,l_indx+k-1)
      end do
    end do

    dt_p = dt
    t = 0.d0
    factor= -ii*dt_p*s
    !Start of the time loop
    call init_fill_list(inital_time, time, 0.d0)
    prob = 0.d0
    prob(1) = 1.d0
    call init_fill_array_list(i_p, p, prob)
    print *,  '***Begin Time Loop***'

    call begin_timing

    pulse => select_pulse_type(pulse_shape, 'length')

    do while (t<= t_intv)

      call fill_list(time, t)

      do i  = 1, 2

        E_t = pulse(t + t_var(i) * dt)
        V_t = exp(0.5d0*s*ii*dt * x(:) * E_t)

        !> Length-Gauge
        psi_0(:) = V_t * psi_0(:)

        ! Lanczos
        !!******* some initializations******************************************************
        do j = 1, int(dt / dt_p)
          call time_dep_lanczos_tridiag2(tdmat,lancz_itnum,dt_p,factor,psi_0,lanc_threshold)
          factor = -s * ii * dt_p
        end do
        !***********************************************************************************
        psi_0(:) = V_t * psi_0(:)

      end do

      factor = - (1.d0-4.d0*s) * ii * dt_p

      E_t = pulse(t + t_var(3) * dt)
      V_t = exp((0.5d0-2.d0*s)*ii*dt * x(:) * E_t)

      psi_0(:) = V_t * psi_0(:)


      do j = 1, int(dt / dt_p)
        call time_dep_lanczos_tridiag2(tdmat,lancz_itnum,dt_p,factor,psi_0,lanc_threshold)
      end do

      psi_0(:) = V_t * psi_0(:)

      factor = - s * ii * dt_p


      do i  = 4, 5

        E_t = pulse(t + t_var(i) * dt)
        V_t = exp(0.5d0*s*ii*dt * x(:) * E_t)

        psi_0(:) = V_t * psi_0(:)


        do j = 1, int(dt/dt_p)
          call time_dep_lanczos_tridiag2(tdmat,lancz_itnum,dt_p,factor,psi_0,lanc_threshold)
          factor = - s * ii * dt_p
        end do

        psi_0(:) = V_t * psi_0(:)

      end do


      do i= 1, neig
        proj(i)  =  sum(psi_0(:)*vec0(:,i))
        prob(i)  =  real(proj(i)*conjg(proj(i)))
      end do

      !This part is storing the probability arrays in memory
      !the linked list approach is taken here
      call fill_array_list(p,prob)

      t = t + dt


    end do ! end of the time loop
    call stop_timing

    print *,  '***End Time Loop***'

    call print_timing
    print*,'last (smallest) time step used:',dt_p
    print*,'last factor:',factor

    call end_list(time)
    call end_array_list(p)

    call read_list(inital_time, time, tdata, tlength)
    call read_array_list(i_p, p, pdata, plength)

    !first memebr of the list is an artificial zero to start the list

    write(fmt,'("(f10.2,",I0,"es15.3)")')m_found

    do i = 2, plength
      write(33,fmt)tdata(i),pdata(:,i)
    end do

  end subroutine lanczos_split_exp4


end module
